<?php
	$RequiredRole = 1;
	
	require 'modulos/session-login.php';
	
	require 'modulos/connection-db.php';

    require_once("lib/raelgc/view/Template.php");
    use raelgc\view\Template;

    $tpl = new Template("templates/configuracoes.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("TOPBAR", "templates/topbar.html");

		// Adicionando mais um arquivo HTML
    $tpl->addFile("FOOTER", "templates/footer.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("SIDEBAR", "templates/sidebar.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("SCRIPTS", "templates/scripts.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("ESTILOS", "templates/estilos.html");
	
	//Dados Usuário
	$email_login = $_SESSION['email_login'];
	
	if($_SESSION['RoleUser'] == '1'){
		$tpl->block("BLOCO_CONFIGURACOES");	
	}
	
	$estadosBrasileiros = array(
	'AC'=>'Acre',
	'AL'=>'Alagoas',
	'AP'=>'Amapá',
	'AM'=>'Amazonas',
	'BA'=>'Bahia',
	'CE'=>'Ceará',
	'DF'=>'Distrito Federal',
	'ES'=>'Espírito Santo',
	'GO'=>'Goiás',
	'MA'=>'Maranhão',
	'MT'=>'Mato Grosso',
	'MS'=>'Mato Grosso do Sul',
	'MG'=>'Minas Gerais',
	'PA'=>'Pará',
	'PB'=>'Paraíba',
	'PR'=>'Paraná',
	'PE'=>'Pernambuco',
	'PI'=>'Piauí',
	'RJ'=>'Rio de Janeiro',
	'RN'=>'Rio Grande do Norte',
	'RS'=>'Rio Grande do Sul',
	'RO'=>'Rondônia',
	'RR'=>'Roraima',
	'SC'=>'Santa Catarina',
	'SP'=>'São Paulo',
	'SE'=>'Sergipe',
	'TO'=>'Tocantins'
	);
	
	$query_usuario = $pdo->query("SELECT id, nome, email, celular, status, token, sobrenome, data_criacao, company_id, role, admin FROM cadastro_usuarios WHERE email = '$email_login'")->fetchAll();
 		
		foreach($query_usuario as $linha){
			$company_id = $linha['company_id'];
		}
	$query_corretora = $pdo->query("SELECT id, nome, endereco, numero, bairro, cep, cidade, estado, complemento, telefone, status, token, logo, data_cadastro FROM cadastro_corretoras WHERE id = '$company_id'")->fetchAll();
 		
		foreach($query_corretora as $linha){
			$estado_atual = $linha['estado'];
			$data = $linha['data_cadastro'];
			$tpl->TOKEN = $linha['token'];		
			$tpl->NOME = $linha['nome'];
			$tpl->TELEFONE = $linha['telefone'];
			$tpl->ENDERECO = $linha['endereco'];
			$tpl->NUMERO = $linha['numero'];
			$tpl->BAIRRO = $linha['bairro'];
			$tpl->CEP = $linha['cep'];
			$tpl->CIDADE = $linha['cidade'];
			$tpl->ESTADO = $linha['estado'];
			$tpl->COMPLEMENTO_EXP = $linha['complemento'];
			if($linha['complemento'] != ""){
			$tpl->COMPLEMENTO = " - ".$linha['complemento'];
			}else{
			$tpl->COMPLEMENTO = "";	
			}
			
			if($linha['logo'] != ""){
			$tpl->IMAGEM_DESTACADA = $linha['logo'];
			}else{
			$tpl->IMAGEM_DESTACADA = "avatar-corretora.png";
			}
		}
		
		foreach($pdo->query("SELECT DATE_FORMAT('$data', '%d/%m/%Y') as Calculated_Date FROM cadastro_usuarios WHERE email = '$email_login'") as $row) {
			$tpl->DATA = $row['Calculated_Date'];	
		}
	
	$count_usuario = $pdo->query("SELECT id FROM cadastro_usuarios WHERE company_id = '$company_id'")->fetchAll();
	$count = count($count_usuario);
	
	$tpl->TOTAL_USUARIOS = $count;
	
	$tpl->TOKEN_CORRETORA = $_SESSION['TokenCorretora'];
	
	foreach($estadosBrasileiros as $value => $text){

		$tpl->SIGLA = $value;
		$tpl->ESTADO = $text;
	
	// Vendo se a opção atual deve ter o atributo "selected"
		if($estado_atual == $value) $tpl->SELECTED = "selected";

		// Caso esta não seja a opção atual, limpamos o valor da variável SELECTED
		else $tpl->clear("SELECTED");

		$tpl->block("BLOCO_ESTADO");	

	}
	$token_corretora = $_SESSION['TokenCorretora'];
	$query_company = $pdo->query("SELECT background, logo, nome FROM cadastro_corretoras WHERE token = '$token_corretora'")->fetchAll();
 		
		foreach($query_company as $ln){
			$nome = $ln['nome'];
			$background = $ln['background'];
			$logo = $ln['logo'];
		}
		
	$tpl->BACKGROUND = $background;
	if($logo != ""){
		$tpl->LOGO_CORRETORA = '<img src="'.ROOT.'/admin/imagens/'.$logo.'" class="img-responsive" alt="'.$nome.'" />';
	}else{
		$tpl->LOGO_CORRETORA = '<img src="'.ROOT.'/src/imagens/logo.png" class="img-responsive" alt="Simulador Group Saúde" />';	
	}
			
	$token_corretora = $_SESSION['TokenCorretora'];
	$query_company = $pdo->query("SELECT background, logo, nome FROM cadastro_corretoras WHERE token = '$token_corretora'")->fetchAll();
 		
		foreach($query_company as $ln){
			$nome = $ln['nome'];
			$background = $ln['background'];
			$logo = $ln['logo'];
		}
		
	$tpl->BACKGROUND = $background;
	if($logo != ""){
		$tpl->LOGO_CORRETORA = '<img src="'.ROOT.'/admin/imagens/'.$logo.'" class="img-responsive" alt="'.$nome.'" />';
	}else{
		$tpl->LOGO_CORRETORA = '<img src="'.ROOT.'/src/imagens/logo.png" class="img-responsive" alt="Simulador Group Saúde" />';	
	}
	
	$tpl->NOME_USUARIO = $_SESSION['NameUser'];
	$tpl->ROOT = ROOT;
    $tpl->show();

?>