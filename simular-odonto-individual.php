<?php

	require 'modulos/session-login.php';

	require 'modulos/connection-db.php';

    require_once("lib/raelgc/view/Template.php");
    use raelgc\view\Template;

    $tpl = new Template("templates/simular-odonto.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("TOPBAR", "templates/topbar.html");

		// Adicionando mais um arquivo HTML
    $tpl->addFile("FOOTER", "templates/footer.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("SIDEBAR", "templates/sidebar.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("SCRIPTS", "templates/scripts.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("ESTILOS", "templates/estilos.html");
	
	if(isset($_GET['estado'])){
		$estado = $_GET['estado'];
				
		$tpl->addFile("FILTROS", "templates/filtros-odonto.html");
		
		$estadosBrasileiros = array(
		'AC'=>'Acre','AL'=>'Alagoas','AP'=>'Amapá','AM'=>'Amazonas','BA'=>'Bahia','CE'=>'Ceará','DF'=>'Distrito Federal','ES'=>'Espírito Santo','GO'=>'Goiás','MA'=>'Maranhão','MT'=>'Mato Grosso','MS'=>'Mato Grosso do Sul','MG'=>'Minas Gerais','PA'=>'Pará','PB'=>'Paraíba','PR'=>'Paraná','PE'=>'Pernambuco','PI'=>'Piauí','RJ'=>'Rio de Janeiro','RN'=>'Rio Grande do Norte','RS'=>'Rio Grande do Sul','RO'=>'Rondônia','RR'=>'Roraima','SC'=>'Santa Catarina','SP'=>'São Paulo','SE'=>'Sergipe','TO'=>'Tocantins'
		);

		if(array_key_exists($estado, $estadosBrasileiros))
		{
		$tpl->SIGLA = $estado;
		$tpl->ESTADO = $estadosBrasileiros[$estado];
		}
		
		$tpl->PERFIL = "2";	
		
		$query_cidade = $pdo->query("SELECT Nome, Codigo FROM cadastro_municipios WHERE Uf = '$estado' ORDER BY Nome ASC")->fetchAll();
			foreach($query_cidade as $cidade){
				$tpl->CIDADES_FILTRO = $cidade['Nome'];
				
				$tpl->block("BLOCO_CIDADES_FILTRO");
			}
				
		$query_planos = $pdo->query("SELECT token FROM cadastro_planos_odonto WHERE status = '1' AND perfil = '2' ORDER BY id DESC")->fetchAll();
		
		foreach($query_planos as $rw){
			
			$token_plano = $rw['token'];
			
		$query_perfil = $pdo->query("SELECT token_seguradora FROM assoc_planos_odonto_seguradoras WHERE token_plano = '$token_plano' ORDER BY id DESC")->fetchAll();
		
		$token_seguradora = array();	
		foreach($query_perfil as $row){
			
			$token = $row['token_seguradora'];
				 if ( in_array($row['token_seguradora'], $token_seguradora) ) {
					continue;
				}
				$token_seguradora[] = $row['token_seguradora'];
				
				$token = $row['token_seguradora'];
	
				$query_seguradoras = $pdo->query("SELECT id, nome, logo, status, slug, token FROM cadastro_seguradoras_odonto WHERE status = '1' AND estado = '$estado' AND token = '$token' ORDER BY id DESC")->fetchAll();
				
				foreach($query_seguradoras as $linha){
					$tpl->NOME = $linha['nome'];
					$tpl->SLUG = $linha['slug'];
					$tpl->IMAGEM_DESTACADA = $linha['logo'];
					$tpl->TOKEN_SEGURADORA = $linha['token'];
					
					$tpl->block("BLOCO_SEGURADORAS");
					
				}
				
				
		}
		}

				
		
	$tpl->VIDAS_ODONTO = '<input type="hidden" class="form-control vidas" id="quantidade" name="quantidade" value="1" />';
	
		
	}else{
		
		// Adicionando mais um arquivo HTML
		$tpl->addFile("MAPA", "templates/mapa.html");
	}
	

	
	//Dados Usuário
	$email_login = $_SESSION['email_login'];
	
	if($_SESSION['RoleUser'] == '1'){
		$tpl->block("BLOCO_CONFIGURACOES");	
	}
			
	$token_corretora = $_SESSION['TokenCorretora'];
	$query_company = $pdo->query("SELECT background, logo, nome FROM cadastro_corretoras WHERE token = '$token_corretora'")->fetchAll();
 		
		foreach($query_company as $ln){
			$nome = $ln['nome'];
			$background = $ln['background'];
			$logo = $ln['logo'];
		}
		
	$tpl->BACKGROUND = $background;
	if($logo != ""){
		$tpl->LOGO_CORRETORA = '<img src="'.ROOT.'/admin/imagens/'.$logo.'" class="img-responsive" alt="'.$nome.'" />';
	}else{
		$tpl->LOGO_CORRETORA = '<img src="'.ROOT.'/src/imagens/logo.png" class="img-responsive" alt="Simulador Group Saúde" />';	
	}
	
	$tpl->MENU9 = "active";							
	$tpl->URL = "simular-odonto-individual";
	$tpl->PAGINA = "Odonto - Individual";
	$tpl->NOME_USUARIO = $_SESSION['NameUser'];
	$tpl->ROOT = ROOT;
    $tpl->show();

?>