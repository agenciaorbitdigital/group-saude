<?php
	
	require 'modulos/session-login.php';

	require 'modulos/connection-db.php';

    require_once("lib/raelgc/view/Template.php");
    use raelgc\view\Template;

    $tpl = new Template("templates/tabela-resumida-odonto.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("SCRIPTS", "templates/scripts.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("ESTILOS", "templates/estilos.html");
	
	$token = $_GET['seguradora'];
	
	$tpl->PERFIL = $_GET['perfil'];


	$estado = $_GET['estado'];
	$estadosBrasileiros = array(
		'AC'=>'Acre','AL'=>'Alagoas','AP'=>'Amapá','AM'=>'Amazonas','BA'=>'Bahia','CE'=>'Ceará','DF'=>'Distrito Federal','ES'=>'Espírito Santo','GO'=>'Goiás','MA'=>'Maranhão','MT'=>'Mato Grosso','MS'=>'Mato Grosso do Sul','MG'=>'Minas Gerais','PA'=>'Pará','PB'=>'Paraíba','PR'=>'Paraná','PE'=>'Pernambuco','PI'=>'Piauí','RJ'=>'Rio de Janeiro','RN'=>'Rio Grande do Norte','RS'=>'Rio Grande do Sul','RO'=>'Rondônia','RR'=>'Roraima','SC'=>'Santa Catarina','SP'=>'São Paulo','SE'=>'Sergipe','TO'=>'Tocantins'
		);

		if(array_key_exists($estado, $estadosBrasileiros))
		{
		$tpl->SIGLA = $estado;
		$tpl->ESTADO = $estadosBrasileiros[$estado];
		}
		
	$query_seguradora = $pdo->query("SELECT id, nome, logo, token FROM cadastro_seguradoras_odonto WHERE token = '$token'")->fetchAll();
				
		foreach($query_seguradora as $linha){
			$tpl->SEGURADORA = $linha['nome'];
			$tpl->IMAGEM_DESTACADA = $linha['logo'];			
		}
	
	$query_planos = $pdo->query("SELECT id, nome, token FROM cadastro_planos_odonto WHERE token_seguradora = '$token'")->fetchAll();
				
		foreach($query_planos as $linha){
			$tpl->PLANOS = "<th>".$linha['nome']."</th>";
			
			$tpl->block("BLOCO_THEAD");
			$tpl->block("BLOCO_THEAD_PRECOS");				
		}
				
	if(isset($_GET['preco'])){
		$preco = $_GET['preco'];
		
		$tpl->TD_VALOR = "<td>Valor</td>";
		
		$query_preco = $pdo->query("SELECT preco FROM cadastro_planos_odonto WHERE token_seguradora = '$token'")->fetchAll();
				
		foreach($query_preco as $linha){
			$tpl->PRECOS = "<th>R$ ".$linha['preco']."</th>";	
			
			$tpl->block("BLOCO_PRECO");		
		}
	}

	if(isset($_GET['fundo_reserva'])){
		$fundo_reserva = $_GET['fundo_reserva'];
		
		$tpl->TD_FUNDO = "<td>Fundo de Reserva</td>";
		
		$query_fundo_reserva = $pdo->query("SELECT fundo_reserva FROM cadastro_planos_odonto WHERE token_seguradora = '$token'")->fetchAll();
				
		foreach($query_fundo_reserva as $linha){
			if($linha['fundo_reserva'] == "1"){
				$tpl->FUNDOS = "<th>Com Fundo de Reserva</th>";		
			}
			if($linha['fundo_reserva'] == "2"){
				$tpl->FUNDOS = "<th>Sem Fundo de Reserva</th>";	
			}
			
			
			$tpl->block("BLOCO_FUNDO");		
		}
	}
	if(isset($_GET['reembolso'])){
		$reembolso = $_GET['reembolso'];
		
		$tpl->TD_REEMBOLSO = "<td>Rembolso</td>";
		
		$query_reembolso = $pdo->query("SELECT reembolso FROM cadastro_planos_odonto WHERE token_seguradora = '$token'")->fetchAll();
				
		foreach($query_reembolso as $linha){
			$tpl->REEMBOLSOS = "<th>".$linha['reembolso']."</th>";	
			
			$tpl->block("BLOCO_REEMBOLSO");		
		}
	}
	
	if(isset($_GET['taxa'])){
		$taxa = $_GET['taxa'];
		
		$query_taxa = $pdo->query("SELECT taxa_inscricao FROM cadastro_seguradoras_odonto WHERE token = '$token'")->fetchAll();
				
		foreach($query_taxa as $linha){
			if($linha['taxa_inscricao'] != ""){
				$tpl->TAXA = "<h2>Taxa de Inscrição: ".$linha['taxa_inscricao']."</h2>";
			}else{
				$tpl->TAXA = "<h2>Taxa de Inscrição: Não Informado</h2>";	
			}
		}
	}
	
	if(isset($_GET['atuacao'])){
		$atuacao = $_GET['atuacao'];
		
		$tpl->TD_ABRANGENCIA = "<td>Abrangência</td>";
		
		$query_abrangencia = $pdo->query("SELECT atuacao FROM cadastro_planos_odonto WHERE token_seguradora = '$token'")->fetchAll();
				
		foreach($query_abrangencia as $linha){
			$tpl->ABRANGENCIAS = "<th>".$linha['atuacao']."</th>";	
			
			$tpl->block("BLOCO_ABRANGENCIA");		
		}
	}
	if(isset($_GET['coparticipacao'])){
		$coparticipacao = $_GET['coparticipacao'];	
		
		$tpl->TD_COPARTICIPACAO = "<td>Coparticipação</td>";
		
		$query_coparticipacao = $pdo->query("SELECT coparticipacao FROM cadastro_planos_odonto WHERE token_seguradora = '$token'")->fetchAll();
				
		foreach($query_coparticipacao as $linha){
			$tpl->COPARTICIPACOES = "<th>".$linha['coparticipacao']."</th>";	
			
			$tpl->block("BLOCO_COPARTICIPACAO");		
		}
	}
		
	if(isset($_GET['carencia'])){
		$carencia = $_GET['carencia'];
		
		$tpl->TD_CARENCIA = "<td>Carência</td>";
		
		$query_id_carencia = $pdo->query("SELECT carencia FROM cadastro_planos_odonto WHERE token_seguradora = '$token'")->fetchAll();
				
		foreach($query_id_carencia as $linha){
			$id_carencia = $linha['carencia'];
			
			$query_carencia = $pdo->query("SELECT id FROM cadastro_carencias WHERE id = '$id_carencia'")->fetchAll();
				
			foreach($query_carencia as $row){
			
				$tpl->CARENCIAS = $row['id'];	
				
				$tpl->block("BLOCO_CARENCIA");		
			}	
		}
	}
	
	
	if(isset($_GET['rede_credenciada'])){
		$rede_credenciada = $_GET['rede_credenciada'];
		
		$query_token_plano = $pdo->query("SELECT token, nome FROM cadastro_planos_odonto WHERE token_seguradora = '$token'")->fetchAll();
				
		foreach($query_token_plano as $linha){
			$id_plano = $linha['token'];
			$tpl->NOME_PLANO = $linha['nome'];
			
			$query_rede_credenciada = $pdo->query("SELECT id_rede_credenciada FROM assoc_planos_odonto_rede_credenciada WHERE token_plano = '$id_plano'")->fetchAll();
				
			foreach($query_rede_credenciada as $ln){
				$id_rede_credenciada = $ln['id_rede_credenciada'];
				
				$query_unidades = $pdo->query("SELECT id, nome FROM cadastro_rede_credenciada WHERE id = '$id_rede_credenciada'")->fetchAll();
				
				foreach($query_unidades as $row){
					
					$tpl->UNIDADES = $row['nome'];					
								
				}	
				
				$tpl->block("BLOCO_UNIDADE");	
			}
			
			$query_procedimento = $pdo->query("SELECT id_procedimento FROM assoc_procedimentos_planos_odonto WHERE token_plano = '$id_plano'")->fetchAll();
				
			foreach($query_procedimento as $ln){
				$id_procedimento = $ln['id_procedimento'];
				
				$query_procedimentos = $pdo->query("SELECT id, nome FROM cadastro_procedimentos WHERE id = '$id_procedimento'")->fetchAll();
				
				foreach($query_procedimentos as $row){
					
					$tpl->PROCEDIMENTO = $row['nome'];					
								
				}	
				
				$tpl->block("BLOCO_PROCEDIMENTO");	
			
			}
			$count_procedimento = count($query_procedimento);
			$count_unidades = count($query_rede_credenciada);
			
			if($count_procedimento == 0){
				$tpl->SEM_PROCEDIMENTOS = "Nenhum Procedimento Cadastrado!";
			}else{
				$tpl->SEM_PROCEDIMENTOS = "";
			}
			if($count_unidades == 0){
				$tpl->SEM_UNIDADES = "Nenhum Unidade Cadastrada!";
			}else{
				$tpl->SEM_UNIDADES = "";
			}
			
			$tpl->block("BLOCO_PROCEDIMENTOS");
			$tpl->block("BLOCO_REDE_CREDENCIADA");			
		}
		
		
	}
	
	if(isset($_GET['informacoes'])){
		$informacoes = $_GET['informacoes'];
		
		$query_informacoes = $pdo->query("SELECT informacoes_adicionais, documentos, nome FROM cadastro_planos_odonto WHERE token_seguradora = '$token'")->fetchAll();
			
		foreach($query_informacoes as $row){
			
			$tpl->NAME_PLANO = $row['nome'];
			
			if($row['informacoes_adicionais'] OR $row['documentos'] != ""){
				
			}else{

				$tpl->SEM_INFORMACOES = "Nenhuma informação adicional encontrada!";
			}
			
			$tpl->INFORMACOES = $row['informacoes_adicionais'];	
			$tpl->DOCUMENTOS = $row['documentos'];	
				
			$tpl->block("BLOCO_INFORMACOES");		
		}
			
			if($row['informacoes_adicionais'] != ""){
				$tpl->TIT_INFORMACOES = "<h4>Informações Adicionais</h4>";	
			}else{
				$tpl->TIT_INFORMACOES = "";	
			}
			if($row['documentos'] != ""){
				$tpl->TIT_DOCUMENTOS = "<h4>Documentação Necessária</h4>";	
			}else{
				$tpl->TIT_DOCUMENTOS = "";	
			}
	}
	
	$tpl->REFERENCIA = ucfirst(strftime("%B/ %Y"));
			
	$token_corretora = $_SESSION['TokenCorretora'];
	$query_company = $pdo->query("SELECT background, logo, nome, endereco, cidade, estado, bairro, cep, numero, complemento FROM cadastro_corretoras WHERE token = '$token_corretora'")->fetchAll();
 		
		foreach($query_company as $ln){
			$nome = $ln['nome'];
			$background = $ln['background'];
			$logo = $ln['logo'];
			$endereco = $ln['endereco'];
			$num = $ln['numero'];
			$bairro = $ln['bairro'];
			$cidade = $ln['cidade'];
			$estado = $ln['estado'];
			$cep = $ln['cep'];
			$complemento = $ln['complemento'];
		}
	$tpl->ENDERECO = $endereco.", ".$num." - ".$complemento." - ".$bairro."<br />".$cidade."/ ".$estado." - CEP: ".$cep;
	$tpl->CORRETORA = $nome;
	
	$email_login = $_SESSION['email_login'];
	
	$query_id = $pdo->query("SELECT celular FROM cadastro_usuarios WHERE email = '$email_login'")->fetchAll();
		foreach($query_id as $ln){
			$telefone = $ln['celular'];
		}
	$tpl->TELEFONE = $telefone;
	$tpl->EMAIL = $email_login;
	
	if($logo != ""){
		$tpl->LOGO_CORRETORA = '<img src="'.ROOT.'/admin/imagens/'.$logo.'" class="img-responsive" alt="'.$nome.'" />';
	}else{
		$tpl->LOGO_CORRETORA = '<img src="'.ROOT.'/src/imagens/logo.png" class="img-responsive" alt="Simulador Group Saúde" />';	
	}
	
	$tpl->NOME_USUARIO = $_SESSION['NameUser'];
	$tpl->ROOT = ROOT;
    $tpl->show();

?>