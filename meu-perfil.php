<?php

	require 'modulos/session-login.php';

	require 'modulos/connection-db.php';

    require_once("lib/raelgc/view/Template.php");
    use raelgc\view\Template;

    $tpl = new Template("templates/meu-perfil.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("TOPBAR", "templates/topbar.html");

		// Adicionando mais um arquivo HTML
    $tpl->addFile("FOOTER", "templates/footer.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("SIDEBAR", "templates/sidebar.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("SCRIPTS", "templates/scripts.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("ESTILOS", "templates/estilos.html");
	
	//Dados Usuário
	$email_login = $_SESSION['email_login'];
	
	if($_SESSION['RoleUser'] == '1'){
		$tpl->block("BLOCO_CONFIGURACOES");	
	}
	
	$query_usuario = $pdo->query("SELECT id, nome, email, celular, status, token, sobrenome, data_criacao, company_id, role, admin, foto_perfil FROM cadastro_usuarios WHERE email = '$email_login'")->fetchAll();
 		
		foreach($query_usuario as $linha){
			$data = $linha['data_criacao'];
			$company_id = $linha['company_id'];
			$tpl->TOKEN = $linha['token'];		
			$tpl->NOME = $linha['nome'];
			$tpl->SOBRENOME = $linha['sobrenome'];
			$tpl->EMAIL = $linha['email'];
			$tpl->CELULAR = $linha['celular'];
			$tpl->FOTO_PERFIL = $linha['foto_perfil'];
			
				
			if($linha['role'] == 1){
				$tpl->ROLE = "Administrador";
			}else{
				$tpl->ROLE = "Usuário";	
			}

		}
		foreach($pdo->query("SELECT DATE_FORMAT('$data', '%d/%m/%Y') as Calculated_Date FROM cadastro_usuarios WHERE email = '$email_login'") as $row) {
			$tpl->DATA = $row['Calculated_Date'];	
		}
	
	$query_corretora = $pdo->query("SELECT id, nome FROM cadastro_corretoras WHERE id = '$company_id'")->fetchAll();
 		
		foreach($query_corretora as $row){
			$tpl->CORRETORA = $row['nome'];	
		}
			
	$token_corretora = $_SESSION['TokenCorretora'];
	$query_company = $pdo->query("SELECT background, logo, nome FROM cadastro_corretoras WHERE token = '$token_corretora'")->fetchAll();
 		
		foreach($query_company as $ln){
			$nome = $ln['nome'];
			$background = $ln['background'];
			$logo = $ln['logo'];
		}
		
	$tpl->BACKGROUND = $background;
	if($logo != ""){
		$tpl->LOGO_CORRETORA = '<img src="'.ROOT.'/admin/imagens/'.$logo.'" class="img-responsive" alt="'.$nome.'" />';
	}else{
		$tpl->LOGO_CORRETORA = '<img src="'.ROOT.'/src/imagens/logo.png" class="img-responsive" alt="Simulador Group Saúde" />';	
	}
	
	$tpl->NOME_USUARIO = $_SESSION['NameUser'];
	$tpl->ROOT = ROOT;
    $tpl->show();

?>