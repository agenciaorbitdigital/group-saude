﻿<?php require 'modulos/auto-login.php'; ?>
<?php
if(!isset ($_GET['token'])){
	echo "<script>window.location.href = 'login'</script>";
}
if(!isset ($_GET['key'])){
	echo "<script>window.location.href = 'login'</script>";
}
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<!-- Inclusão do Bootstrap -->
<link rel="stylesheet" href="src/bootstrap/css/bootstrap.css" />
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="src/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="src/css/AdminLTE.min.css">
 <!-- Tema Azul -->
  <link rel="stylesheet" href="src/css/skins/skin-blue.min.css">
  <style type="text/css">
  body,td,th {
	font-family: "Source Sans Pro", "Helvetica Neue", Helvetica, Arial, sans-serif;
}
  </style>


  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
<title>Altere Sua Senha</title>
</head>

<body class="hold-transition login-page" style="overflow:hidden;">
<div class="login-box">
  <div class="login-logo">
    <a href="/"><img src="src/imagens/logo.png" alt="Group Saúde" /></a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">Digite sua nova senha</p>

    <form method="post" action="modulos/nova-senha.php">
      <input type="hidden" name="key" value="<?php echo $_GET['key']; ?>" />
      <input type="hidden" name="token" value="<?php echo $_GET['token']; ?>" />
      <div class="form-group has-feedback">
        <input type="password" class="form-control" name="senha" id="senha" required />
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" name="confirma_senha" id="confirma_senha" required />
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-12">
          <button type="submit" class="btn btn-success btn-block btn-flat">Alterar Senha <i class="fa fa-lock"></i></button>
        </div>
        <!-- /.col -->
      </div>
    </form>
	<div id="return_login"></div><!-- /#return_login -->
    <div id="load"></div><!--- /#load -->
    <a href="login">Fazer Login</a><br>
  </div>
  <!-- /.login-box-body -->
  <div class="lockscreen-footer text-center">
    Copyright &copy; 2017 <b><a href="#" class="text-black">Orbit Digital</a></b><br>
    Todos os Direitos Reservados
  </div>
</div>
<!-- /.login-box -->
<!-- INCLUSÃO DO JQUERY -->
<script type="text/javascript" src="src/js/jquery.js"></script> 
<!-- INCLUSÃO DO JQUERY.VALIDATE -->
<script type="text/javascript" src="src/js/jquery.validate.js"></script>
<!-- INCLUSÃO FUNÇOES JAVASCRIPT -->
<script type="text/javascript" src="src/js/scripts.js"></script>
</body>
</html>