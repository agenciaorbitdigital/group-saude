<?php
	$RequiredRole = 1;
	
	require 'modulos/session-login.php';

	require 'modulos/connection-db.php';

    require_once("lib/raelgc/view/Template.php");
    use raelgc\view\Template;

    $tpl = new Template("templates/editar-usuario.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("TOPBAR", "templates/topbar.html");

		// Adicionando mais um arquivo HTML
    $tpl->addFile("FOOTER", "templates/footer.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("SIDEBAR", "templates/sidebar.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("SCRIPTS", "templates/scripts.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("ESTILOS", "templates/estilos.html");
	
	//Dados Usuário
	$id = $_GET['id'];
	
	if($_SESSION['RoleUser'] == '1'){
		$tpl->block("BLOCO_CONFIGURACOES");	
	}
	
	$query_usuario = $pdo->query("SELECT id, nome, email, celular, status, token, sobrenome, role, cargo FROM cadastro_usuarios WHERE id = '$id'")->fetchAll();
 		
		foreach($query_usuario as $linha){
			$tpl->TOKEN = $linha['token'];		
			$tpl->NOME = $linha['nome'];
			$tpl->SOBRENOME = $linha['sobrenome'];
			$tpl->EMAIL = $linha['email'];
			$tpl->CELULAR = $linha['celular'];
			$tpl->CARGO = $linha['cargo'];
			
			if($linha['role'] == 1){
				$tpl->ROLE1 = "selected";
				$tpl->ROLE2 = "";
			}else{
				$tpl->ROLE1 = "";
				$tpl->ROLE2 = "selected";	
			}
				if($linha['status'] == 1){
				$tpl->STATUS_CHECK = "checked";
			}else{	
				$tpl->STATUS_CHECK = "";
			}

		}
		
	$token_corretora = $_SESSION['TokenCorretora'];
	$query_company = $pdo->query("SELECT background, logo, nome FROM cadastro_corretoras WHERE token = '$token_corretora'")->fetchAll();
 		
		foreach($query_company as $ln){
			$nome = $ln['nome'];
			$background = $ln['background'];
			$logo = $ln['logo'];
		}
		
	$tpl->BACKGROUND = $background;
	if($logo != ""){
		$tpl->LOGO_CORRETORA = '<img src="'.ROOT.'/admin/imagens/'.$logo.'" class="img-responsive" alt="'.$nome.'" />';
	}else{
		$tpl->LOGO_CORRETORA = '<img src="'.ROOT.'/src/imagens/logo.png" class="img-responsive" alt="Simulador Group Saúde" />';	
	}
	
	$tpl->NOME_USUARIO = $_SESSION['NameUser'];
	$tpl->ROOT = ROOT;
    $tpl->show();

?>