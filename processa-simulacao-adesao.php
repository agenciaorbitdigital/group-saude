<?php error_reporting(E_ALL & ~E_NOTICE & ~E_WARNING);
	
	require 'modulos/session-login.php';

	require 'modulos/connection-db.php';

    require_once("lib/raelgc/view/Template.php");
    use raelgc\view\Template;

    $tpl = new Template("templates/processa-simulacao-adesao.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("TOPBAR", "templates/topbar.html");

		// Adicionando mais um arquivo HTML
    $tpl->addFile("FOOTER", "templates/footer.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("SIDEBAR", "templates/sidebar.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("SCRIPTS", "templates/scripts.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("ESTILOS", "templates/estilos.html");
	
	if(!isset($_POST['seguradora'])){	
		echo "<script>alert('Não existem seguradoras cadastradas!');window.history.go(-1);</script>";	
	}else{
		$token = $_POST['seguradora'];
	
	$perfil = $_POST['perfil'];
	if($perfil == "4"){
		$tpl->PERFIL = "Pessoa Física";
	}if($perfil == "3"){
		$tpl->PERFIL = "PME";
	}
	
	$estado = $_POST['estado'];
	$estadosBrasileiros = array(
		'AC'=>'Acre','AL'=>'Alagoas','AP'=>'Amapá','AM'=>'Amazonas','BA'=>'Bahia','CE'=>'Ceará','DF'=>'Distrito Federal','ES'=>'Espírito Santo','GO'=>'Goiás','MA'=>'Maranhão','MT'=>'Mato Grosso','MS'=>'Mato Grosso do Sul','MG'=>'Minas Gerais','PA'=>'Pará','PB'=>'Paraíba','PR'=>'Paraná','PE'=>'Pernambuco','PI'=>'Piauí','RJ'=>'Rio de Janeiro','RN'=>'Rio Grande do Norte','RS'=>'Rio Grande do Sul','RO'=>'Rondônia','RR'=>'Roraima','SC'=>'Santa Catarina','SP'=>'São Paulo','SE'=>'Sergipe','TO'=>'Tocantins'
		);

		if(array_key_exists($estado, $estadosBrasileiros))
		{
		$tpl->SIGLA = $estado;
		$tpl->ESTADO = $estadosBrasileiros[$estado];
		}
		
	$query_seguradora = $pdo->query("SELECT id, nome, logo, token FROM cadastro_planos_adesao WHERE token = '$token'")->fetchAll();
				
		foreach($query_seguradora as $linha){
			$tpl->SEGURADORA = $linha['nome'];
			$tpl->IMAGEM_DESTACADA = $linha['logo'];			
		}
		
		$query_planos = $pdo->query("SELECT id, nome, token FROM cadastro_planos_adesao WHERE token = '$token'")->fetchAll();
				
		foreach($query_planos as $linha){
			$tpl->PLANOS = '<th>'.$linha['nome'].'</th>';
			
			$tpl->PLANOS2 = "<th>".$linha['nome']."</th>";
			
			$tpl->block("BLOCO_THEAD");	
			$tpl->block("BLOCO_THEAD_PRECOS");				
		}
		
	if(($_POST['idade1'] == "") AND ($_POST['idade2'] == "") AND ($_POST['idade3'] == "") AND ($_POST['idade4'] == "") AND ($_POST['idade5'] == "") AND ($_POST['idade6'] == "") AND ($_POST['idade7'] == "") AND ($_POST['idade8'] == "") AND ($_POST['idade9'] == "") AND ($_POST['idade10'] == "") ){
		
		$tpl->NENHUM_PRECO = "<p>Você não informou a quantidade de vidas!</p>";	
		
	}else{
				
	if(isset($_POST['idade1']) && ($_POST['idade1']) != ""){
		$preco1 = $_POST['idade1'];
		 
		 $tpl->QTD1 = "<th>".$preco1."</th>";
		$tpl->TD_VALOR1 = "<td>0 a 18 anos</td>";
		
		
		$query_preco1 = $pdo->query("SELECT 0_18 FROM cadastro_planos_adesao WHERE token = '$token'")->fetchAll();

		foreach($query_preco1 as $linha){
			
			if($linha['0_18'] == ""){
			$tpl->PRECO1 = "<th>Preço não informado</th>";
			}else{
			$tpl->PRECO1 = "<th>R$ ".$linha['0_18']."</th>";
			}
			
			$tpl->block("BLOCO_PRECOS1");		
		}
		
	}

	if(isset($_POST['idade2']) && ($_POST['idade2']) != ""){
		$preco2 = $_POST['idade2'];
		 
		 $tpl->QTD2 = "<th>".$preco2."</th>";
		$tpl->TD_VALOR2 = "<td>19 a 23 anos</td>";
		
		
		$query_preco2 = $pdo->query("SELECT 19_23 FROM cadastro_planos_adesao WHERE token = '$token'")->fetchAll();

		foreach($query_preco2 as $linha){
			
			if($linha['19_23'] == ""){
			$tpl->PRECO2 = "<th>Preço não informado</th>";
			}else{
			$tpl->PRECO2 = "<th>R$ ".$linha['19_23']."</th>";
			}
			
			$tpl->block("BLOCO_PRECOS2");		
		}
		
	}
	
	if(isset($_POST['idade3']) && ($_POST['idade3']) != ""){
		$preco3 = $_POST['idade3'];
		 
		 $tpl->QTD3 = "<th>".$preco3."</th>";
		$tpl->TD_VALOR3 = "<td>24 a 28 anos</td>";
		
		
		$query_preco3 = $pdo->query("SELECT 24_28 FROM cadastro_planos_adesao WHERE token = '$token'")->fetchAll();

		foreach($query_preco3 as $linha){
			
			if($linha['24_28'] == ""){
			$tpl->PRECO3 = "<th>Preço não informado</th>";
			}else{
			$tpl->PRECO3 = "<th>R$ ".$linha['24_28']."</th>";
			}
			
			$tpl->block("BLOCO_PRECOS3");		
		}
		
	}
	
	if(isset($_POST['idade4']) && ($_POST['idade4']) != ""){
		$preco4 = $_POST['idade4'];
		 
		 $tpl->QTD4 = "<th>".$preco4."</th>";
		$tpl->TD_VALOR4 = "<td>29 a 33 anos</td>";
		
		
		$query_preco4 = $pdo->query("SELECT 29_33 FROM cadastro_planos_adesao WHERE token = '$token'")->fetchAll();

		foreach($query_preco4 as $linha){
			
			if($linha['29_33'] == ""){
			$tpl->PRECO4 = "<th>Preço não informado</th>";
			}else{
			$tpl->PRECO4 = "<th>R$ ".$linha['29_33']."</th>";
			}
			
			$tpl->block("BLOCO_PRECOS4");		
		}
		
	}
	
	if(isset($_POST['idade5']) && ($_POST['idade5']) != ""){
		$preco5 = $_POST['idade5'];
		
		$tpl->QTD5 = "<th>".$preco5."</th>"; 
		$tpl->TD_VALOR5 = "<td>34 a 38 anos</td>";
		
		
		$query_preco5 = $pdo->query("SELECT 34_38 FROM cadastro_planos_adesao WHERE token = '$token'")->fetchAll();

		foreach($query_preco5 as $linha){
			
			if($linha['34_38'] == ""){
			$tpl->PRECO5 = "<th>Preço não informado</th>";
			}else{
			$tpl->PRECO5 = "<th>R$ ".$linha['34_38']."</th>";
			}
			
			$tpl->block("BLOCO_PRECOS5");		
		}
		
	}
	
	if(isset($_POST['idade6']) && ($_POST['idade6']) != ""){
		$preco6 = $_POST['idade6'];
		 
		 $tpl->QTD6 = "<th>".$preco6."</th>";
		$tpl->TD_VALOR6 = "<td>39 a 43 anos</td>";
		
		
		$query_preco6 = $pdo->query("SELECT 39_43 FROM cadastro_planos_adesao WHERE token = '$token'")->fetchAll();

		foreach($query_preco6 as $linha){
			
			if($linha['39_43'] == ""){
			$tpl->PRECO6 = "<th>Preço não informado</th>";
			}else{
			$tpl->PRECO6 = "<th>R$ ".$linha['39_43']."</th>";
			}
			
			$tpl->block("BLOCO_PRECOS6");		
		}
		
	}
	
	if(isset($_POST['idade7']) && ($_POST['idade7']) != ""){
		$preco7 = $_POST['idade7'];
		 
		 $tpl->QTD7 = "<th>".$preco7."</th>";
		$tpl->TD_VALOR7 = "<td>44 a 48 anos</td>";
		
		
		$query_preco7 = $pdo->query("SELECT 44_48 FROM cadastro_planos_adesao WHERE token = '$token'")->fetchAll();
		$count_preco7 = count($query_preco7);

		foreach($query_preco7 as $linha){
			
			if($linha['44_48'] == ""){
			$tpl->PRECO7 = "<th>Preço não informado</th>";
			}else{
			$tpl->PRECO7 = "<th>R$ ".$linha['44_48']."</th>";
			}
			
			$tpl->block("BLOCO_PRECOS7");		
		}
		
	}
	
	if(isset($_POST['idade8']) && ($_POST['idade8']) != ""){
		$preco8 = $_POST['idade8'];
		 
		 $tpl->QTD8 = "<th>".$preco8."</th>";
		$tpl->TD_VALOR8 = "<td>49 a 53 anos</td>";
		
		
		$query_preco8 = $pdo->query("SELECT 49_53 FROM cadastro_planos_adesao WHERE token = '$token'")->fetchAll();

		foreach($query_preco8 as $linha){
			
			if($linha['49_53'] == ""){
			$tpl->PRECO8 = "<th>Preço não informado</th>";
			}else{
			$tpl->PRECO8 = "<th>R$ ".$linha['49_53']."</th>";
			}
			
			$tpl->block("BLOCO_PRECOS8");		
		}
		
	}
	
	if(isset($_POST['idade9']) && ($_POST['idade9']) != ""){
		$preco9 = $_POST['idade9'];
		
		 $tpl->QTD9 = "<th>".$preco9."</th>";
		$tpl->TD_VALOR9 = "<td>54 a 58 anos</td>";
		
		
		$query_preco9 = $pdo->query("SELECT 54_58 FROM cadastro_planos_adesao WHERE token = '$token'")->fetchAll();

		foreach($query_preco9 as $linha){
			
			if($linha['54_58'] == ""){
			$tpl->PRECO9 = "<th>Preço não informado</th>";
			}else{
			$tpl->PRECO9 = "<th>R$ ".$linha['54_58']."</th>";
			}
			
			$tpl->block("BLOCO_PRECOS9");		
		}
		
	}

	if(isset($_POST['idade10']) && ($_POST['idade10']) != ""){
		$preco10 = $_POST['idade10'];
		 
		 $tpl->QTD10 = "<th>".$preco10."</th>";
		$tpl->TD_VALOR10 = "<td>Acima de 59 anos</td>";
		
		
		$query_preco10 = $pdo->query("SELECT acima_59 FROM cadastro_planos_adesao WHERE token = '$token'")->fetchAll();


		foreach($query_preco10 as $linha){
			
			if($linha['acima_59'] == ""){
			$tpl->PRECO10 = "<th>Preço não informado</th>";
			}else{
			$tpl->PRECO10 = "<th>R$ ".$linha['acima_59']."</th>";
			}
			
			$tpl->block("BLOCO_PRECOS10");		
		}
		
	}
	}
	if(isset($_POST['acomodacao'])){
		$acomodacao = $_POST['acomodacao'];
		
		$tpl->TD_ACOMODACAO = "<td>Acomodação</td>";
		
		$query_acomodacao = $pdo->query("SELECT acomodacao FROM cadastro_planos_adesao WHERE token = '$token'")->fetchAll();
				
		foreach($query_acomodacao as $linha){
			if($linha['acomodacao'] == "1"){
				$tpl->ACOMODACAO = "<th>Quarto coletivo ou Enfermaria</th>";		
			}
			if($linha['acomodacao'] == "2"){
				$tpl->ACOMODACAO = "<th>Quarto privativo ou Apartamento</th>";	
			}
			
			
			$tpl->block("BLOCO_ACOMODACAO");		
		}
	}
	if(isset($_POST['reembolso'])){
		$reembolso = $_POST['reembolso'];
		
		$tpl->TD_REEMBOLSO = "<td>Rembolso</td>";
		
		$query_reembolso = $pdo->query("SELECT reembolso FROM cadastro_planos_adesao WHERE token = '$token'")->fetchAll();
				
		foreach($query_reembolso as $linha){
			$tpl->REEMBOLSOS = "<th>".$linha['reembolso']."</th>";	
			
			$tpl->block("BLOCO_REEMBOLSO");		
		}
	}
	
	if(isset($_POST['taxa'])){
		$taxa = $_POST['taxa'];
		
		$query_taxa = $pdo->query("SELECT taxa_inscricao FROM cadastro_planos_adesao WHERE token = '$token'")->fetchAll();
				
		foreach($query_taxa as $linha){
			if($linha['taxa_inscricao'] != ""){
				$tpl->TAXA = "<h2>Taxa de Inscrição: ".$linha['taxa_inscricao']."</h2>";
			}else{
				$tpl->TAXA = "<h2>Taxa de Inscrição: Não Informado</h2>";	
			}
		}
	}
	
	if(isset($_POST['atuacao'])){
		$atuacao = $_POST['atuacao'];
		
		$tpl->TD_ABRANGENCIA = "<td>Abrangência</td>";
		
		$query_abrangencia = $pdo->query("SELECT atuacao FROM cadastro_planos_adesao WHERE token = '$token'")->fetchAll();
				
		foreach($query_abrangencia as $linha){
			$tpl->ABRANGENCIAS = "<th>".$linha['atuacao']."</th>";	
			
			$tpl->block("BLOCO_ABRANGENCIA");		
		}
	}
	if(isset($_POST['coparticipacao'])){
		$coparticipacao = $_POST['coparticipacao'];	
		
		$tpl->TD_COPARTICIPACAO = "<td>Coparticipação</td>";
		
		$query_coparticipacao = $pdo->query("SELECT coparticipacao FROM cadastro_planos_adesao WHERE token = '$token'")->fetchAll();
				
		foreach($query_coparticipacao as $linha){
			$tpl->COPARTICIPACOES = "<th>".$linha['coparticipacao']."</th>";	
			
			$tpl->block("BLOCO_COPARTICIPACAO");		
		}
	}
		
	if(isset($_POST['carencia'])){
		$carencia = $_POST['carencia'];
		
		$tpl->TD_CARENCIA = "<td>Carência</td>";
		
		$query_id_carencia = $pdo->query("SELECT carencia FROM cadastro_planos_adesao WHERE token = '$token'")->fetchAll();
				
		foreach($query_id_carencia as $linha){
			$id_carencia = $linha['carencia'];
			
			$query_carencia = $pdo->query("SELECT slug FROM cadastro_carencias WHERE id = '$id_carencia'")->fetchAll();
				
			foreach($query_carencia as $row){
			
				$tpl->CARENCIAS = $row['slug'];	
				
				$tpl->block("BLOCO_CARENCIA");		
			}	
		}
	}
	
	
	if(isset($_POST['rede_credenciada'])){
		$rede_credenciada = $_POST['rede_credenciada'];
		
		$query_token_plano = $pdo->query("SELECT token, nome FROM cadastro_planos_adesao WHERE token = '$token'")->fetchAll();
				
		foreach($query_token_plano as $linha){
			$id_plano = $linha['token'];
			$tpl->NOME_PLANO = $linha['nome'];
			
			$query_rede_credenciada = $pdo->query("SELECT id_rede_credenciada FROM assoc_planos_adesao_rede_credenciada WHERE token_plano = '$id_plano'")->fetchAll();
			
			
			foreach($query_rede_credenciada as $ln){
				$id_rede_credenciada = $ln['id_rede_credenciada'];
				
				$query_cidades = $pdo->query("SELECT cidade FROM cadastro_rede_credenciada WHERE id = '$id_rede_credenciada'")->fetchAll();

				
				foreach($query_cidades as $rw){
					$array[] = $rw['cidade'];
				}	
			}
			
			$array=array_unique($array);
			foreach($array as $cidade){
				$tpl->CIDADES_UNIDADES = $cidade;
				$query_rede_credenciada = $pdo->query("SELECT id_rede_credenciada FROM assoc_planos_adesao_rede_credenciada WHERE token_plano = '$id_plano'")->fetchAll();
			
			
			foreach($query_rede_credenciada as $ln){
				$id_rede_credenciada = $ln['id_rede_credenciada'];
				$query_unidades = $pdo->query("SELECT  id, nome, token, a_ambulatorio, h_hospital, m_maternidade, pa_pronto_atendimento, ps_pronto_socorro FROM cadastro_rede_credenciada WHERE cidade = '$cidade' AND id = '$id_rede_credenciada'")->fetchAll();
			
				foreach($query_unidades as $row){
					$id_unidade = $row['id'];
					$tpl->ID_UNIDADE = $row['id'];
					$token_rede_credenciada = $row['token'];
					$nome = $row['nome'];	

					if($row['a_ambulatorio'] == 1){
					$ambulatorio = "A / ";
					}else{
						$ambulatorio = "";
					}
					if($row['h_hospital'] == 1){
					$hospital = "H / ";
					}else{
						$hospital = "";
					}
					if($row['m_maternidade'] == 1){
					$maternidade = "M / ";
					}else{
						$maternidade = "";
					}
					if($row['pa_pronto_atendimento'] == 1){
					$p_atendimento = "PA / ";
					}else{
						$p_atendimento = "";
					}
					if($row['ps_pronto_socorro'] == 1){
					$p_socorro = "PS ";
					}else{
						$p_socorro = "";
					}
					
					if($row['a_ambulatorio'] OR $row['h_hospital'] OR $row['m_maternidade'] OR $row['pa_pronto_atendimento'] OR $row['ps_pronto_socorro'] == 1){
					$tpl->UNIDADES = $nome ." - " .$ambulatorio . $hospital . $maternidade .$p_atendimento . $p_socorro;
					}else{
						$tpl->UNIDADES = $nome;	
					}
					
					if(isset($_POST['especialidades'])){				
					$query_id_especialidade = $pdo->query("SELECT id_especialidade FROM assoc_rede_credenciada_especialidades WHERE token_rede_credenciada = '$token_rede_credenciada'")->fetchAll();
				
						foreach($query_id_especialidade as $ln){
							$id_especialidade = $ln['id_especialidade'];
							
							$query_especialidades = $pdo->query("SELECT id, nome FROM cadastro_especialidades WHERE id = '$id_especialidade'")->fetchAll();
							
							foreach($query_especialidades as $row){
								
								$tpl->ESPECIALIDADES = $row['nome'];					
											
							}	
							
			
							$tpl->block("BLOCO_ESPECIALIDADE");
						}
					}
					
					$tpl->block("BLOCO_UNIDADE");
				}
			
			}
			$tpl->block("BLOCO_REGIOES_UNIDADES");	
			}
			
			$count_unidades = count($query_rede_credenciada);
			

			if($count_unidades == 0){
				$tpl->SEM_UNIDADES = "Nenhuma unidade cadastrada!";
			}else{
				$tpl->SEM_UNIDADES = "";
			}
			
			$tpl->block("BLOCO_REDE_CREDENCIADA");			
		}
		
		
	}
	
	//INFORMAÇOES	
	if(isset($_POST['informacoes'])){
		$informacoes = $_POST['informacoes'];
		
		$tpl->PRINT_INFORMACOES = "1";
		
		$query_id_informacoes = $pdo->query("SELECT id_informacoes, nome FROM cadastro_planos_adesao WHERE token = '$token_assoc_plano'")->fetchAll();
		
		
		foreach($query_id_informacoes as $rw){
			
			$id_informacoes = $rw['id_informacoes'];
			
			$query_informacoes = $pdo->query("SELECT * FROM cadastro_informacoes_adicionais WHERE id = '$id_informacoes' AND status = '1'")->fetchAll();
		
		
			foreach($query_informacoes as $row){
			
					$tpl->NAME_PLANO = $rw['nome'];
				
				if($row['informacoes_adicionais'] OR $row['documentacao'] != ""){

				}else{

					$tpl->SEM_INFORMACOES = "Nenhuma informação adicional encontrada!";
				}

				$tpl->INFORMACOES = $row['informacoes_adicionais'];	
				$tpl->DOCUMENTOS = $row['documentacao'];	

				$tpl->block("BLOCO_INFORMACOES");
			}
		}
			
			
	}
 //INFORMAÇOES
	}
	$tpl->REFERENCIA = ucfirst(strftime("%B/ %Y"));
			
	$token_corretora = $_SESSION['TokenCorretora'];
	$query_company = $pdo->query("SELECT background, logo, nome FROM cadastro_corretoras WHERE token = '$token_corretora'")->fetchAll();
 		
		foreach($query_company as $ln){
			$nome = $ln['nome'];
			$background = $ln['background'];
			$logo = $ln['logo'];
		}
		
	$tpl->BACKGROUND = $background;
	if($logo != ""){
		$tpl->LOGO_CORRETORA = '<img src="'.ROOT.'/admin/imagens/'.$logo.'" class="img-responsive" alt="'.$nome.'" />';
	}else{
		$tpl->LOGO_CORRETORA = '<img src="'.ROOT.'/src/imagens/logo.png" class="img-responsive" alt="Simulador Group Saúde" />';	
	}
	
	$tpl->MENU12 = "active";	
	$tpl->NOME_USUARIO = $_SESSION['NameUser'];
	$tpl->ROOT = ROOT;
    $tpl->show();

?>