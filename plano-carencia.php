<?php
require 'modulos/connection-db.php';

$carencia = $_GET['carencia'];
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Plano de Carência</title>
<style type="text/css">
body{margin:0; padding:0;}
.tg  {border-collapse:collapse;border-spacing:0;border-color:#bbb; width:100%;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:8px 20px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;border-color:#bbb;color:#594F4F;background-color:#E0FFEB;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:8px 20px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;border-color:#bbb;color:#493F3F;background-color:#9DE0AD;}
.tg .tg-pw5p{font-weight:bold;background-color:#9aff99;text-align:center;vertical-align:top}
.tg .tg-baqh{text-align:center;vertical-align:top}
.tg .tg-u8fl{font-weight:bold;background-color:#9aff99;vertical-align:top}
.tg .tg-yw4l{vertical-align:top}
.grey{background:#CFCFCF !important; color:#D90000 !important;}
</style>
</head>
<body>
<?php
$query_carencia = $pdo->query("SELECT id, informacoes, nome, token FROM cadastro_carencias WHERE slug ='$carencia' AND status = '1'")->fetchAll();

	foreach($query_carencia as $linha){
		$token_carencia = $linha['token'];
		$nome = $linha['nome'];
		$informacoes = $linha['informacoes'];
?>
<table class="tg">
  <tr>
    <td class="tg-u8fl">Carência <?php echo $nome; ?></td>
  </tr>
<?php		
		$query_eventos = $pdo->query("SELECT id, evento, token_carencia FROM cadastro_eventos WHERE token_carencia ='$token_carencia'")->fetchAll();
		
			foreach($query_eventos as $ln){
				$evento = $ln['evento'];
				
?>
  <tr>
    <td class="tg-yw4l grey" colspan="4"><?php echo $evento; ?></td>
  </tr>
<?php
			}		
?>
  <tr>
    <td class="tg-yw4l grey" colspan="4"><?php echo $informacoes; ?></td>
  </tr>
</table>

<?php
	}
?>

    
  
</body>
</html>