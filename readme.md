# Simulador Group Saúde #

Versão 1.0

Alterações:

V. 1.0.1 - Correção nos scripts de processamento do simulador - 23/02/2018
V. 1.0.2 - Ajustes nos filtros do simulador - 23/02/2018
V. 1.0.3 - Front End do módulo de conversão e cálculo de idade - 23/02/2018
V. 1.0.4 - Ajustes na exibiçãos dos planos de carência do simulador - 23/02/2018
V. 1.0.5 - Alteração no layout dos filtros do simulador - 23/02/2018
V. 1.1.0 - Implantação do sistema de cálculo de faixas etárias - 23/02/2018
V. 1.1.1 - Implantação dos cálculos de faixa etária - 23/02/2018
V. 1.2.0 - Implantação do sistema de cadastro de reembolsos - 28/02/2018
V. 1.2.1 - Retirada dos campos min_vidas, max_vidas e titulares do plano por adesão - 02/03/2018

## REQUISITOS DO SISTEMA

PHP 5.6+
Apache 2
MySQL 5+
128MB RAM
1GB Armazenamento

PHP mod_rewrite

Permissões de pastas e arquivos 755


## INSTALAÇÃO 

### Banco de Dados 
Altere o arquivo /modulos/connection-db.php com as informações do banco de dados do seu  servidor

```
//PDO
$pdo = new PDO("mysql:host=localhost;dbname=NOME_DO_BANCO", 'NOME_DO_USUARIO', 'SENHA_DO_BANCO');
```

### SMTP
Altere o arquivo /emails/mail-config.php com as informações de SMTP do seu servidor

```
$mail->Host = 'smtp.XXXXXXX.com.br'; // Endereço do servidor SMTP (Autenticação, utilize o host smtp.seudomínio.com.br)
$mail->SMTPAuth   = true;  // Usar autenticação SMTP (obrigatório para smtp.seudomínio.com.br)
$mail->Port       = 587; //  Usar 587 porta SMTP
$mail->Username = 'XXXXXX@XXXXXXXX.com.br'; // Usuário do servidor SMTP (endereço de email)
$mail->Password = 'XXXXXXXX'; // Senha do servidor SMTP (senha do email usado)
//Define o remetente
// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=    
$mail->SetFrom('XXXXXX@XXXXXXXX.com.br', 'Recuperação de Senha - Simulador Group Saúde'); //Seu e-mail
$mail->AddReplyTo('XXXXXX@XXXXXXXX.com.br', 'Group Saúde'); //Seu e-mail
$mail->Subject = 'Recupere sua Senha';//Assunto do e-mail
```