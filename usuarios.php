<?php
	$RequiredRole = 1;
	
	require 'modulos/session-login.php';
	
	require 'modulos/connection-db.php';

    require_once("lib/raelgc/view/Template.php");
    use raelgc\view\Template;

    $tpl = new Template("templates/usuarios.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("TOPBAR", "templates/topbar.html");

		// Adicionando mais um arquivo HTML
    $tpl->addFile("FOOTER", "templates/footer.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("SIDEBAR", "templates/sidebar.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("SCRIPTS", "templates/scripts.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("ESTILOS", "templates/estilos.html");
	
	//Dados Usuário
	$email_login = $_SESSION['email_login'];
	
	if($_SESSION['RoleUser'] == '1'){
		$tpl->block("BLOCO_CONFIGURACOES");	
	}
	
	$query_usuario = $pdo->query("SELECT id, nome, email, celular, status, token, sobrenome, data_criacao, company_id, role, admin, cargo FROM cadastro_usuarios")->fetchAll();
 		
		foreach($query_usuario as $linha){
			$id_user = $linha['id'];
			$data = $linha['data_criacao'];
			$company_id = $linha['company_id'];
			$tpl->TOKEN_CADASTRO = $linha['token'];		
			$tpl->NOME_CADASTRO = $linha['nome'];
			$tpl->SOBRENOME_CADASTRO = $linha['sobrenome'];
			$tpl->EMAIL_CADASTRO = $linha['email'];
			$tpl->CELULAR_CADASTRO = $linha['celular'];
			$tpl->ID_CADASTRO = $linha['id'];
			$tpl->CARGO = $linha['cargo'];
			
			if($linha['role'] == 1){
				$tpl->STATUS_CLASS = "active";
				$tpl->STATUS_EXP = "Ativo";
			}else{
				$tpl->STATUS_CLASS = "disabled";
				$tpl->STATUS_EXP = "Inativo";					
			}
			
			$query_corretora = $pdo->query("SELECT id, nome FROM cadastro_corretoras WHERE id = '$company_id'")->fetchAll();
 		
			foreach($query_corretora as $row){
				$tpl->CORRETORA_CADASTRO = $row['nome'];	
			}
					
			foreach($pdo->query("SELECT DATE_FORMAT('$data', '%d/%m/%Y') as Calculated_Date FROM cadastro_usuarios WHERE id = '$id_user'") as $row) {
				$tpl->DATA_CADASTRO = $row['Calculated_Date'];	
			}
		
			$tpl->block("BLOCO_LISTAGEM");	
			
		}
		
	$tpl->TOKEN_NEW = md5(uniqid(rand(), true));
	
	$tpl->TOKEN_CORRETORA = $_SESSION['TokenCorretora'];	
	
	$token_corretora = $_SESSION['TokenCorretora'];
	$query_company = $pdo->query("SELECT background, logo, nome FROM cadastro_corretoras WHERE token = '$token_corretora'")->fetchAll();
 		
		foreach($query_company as $ln){
			$nome = $ln['nome'];
			$background = $ln['background'];
			$logo = $ln['logo'];
		}
		
	$tpl->BACKGROUND = $background;
	if($logo != ""){
		$tpl->LOGO_CORRETORA = '<img src="'.ROOT.'/admin/imagens/'.$logo.'" class="img-responsive" alt="'.$nome.'" />';
	}else{
		$tpl->LOGO_CORRETORA = '<img src="'.ROOT.'/src/imagens/logo.png" class="img-responsive" alt="Simulador Group Saúde" />';	
	}
	
	$tpl->NOME_USUARIO = $_SESSION['NameUser'];
	$tpl->ROOT = ROOT;
    $tpl->show();

?>