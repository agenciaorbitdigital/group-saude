<?php

	require 'modulos/session-login.php';

	require 'modulos/connection-db.php';

    require_once("lib/raelgc/view/Template.php");
    use raelgc\view\Template;

    $tpl = new Template("templates/simular-saude.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("TOPBAR", "templates/topbar.html");

		// Adicionando mais um arquivo HTML
    $tpl->addFile("FOOTER", "templates/footer.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("SIDEBAR", "templates/sidebar.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("SCRIPTS", "templates/scripts.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("ESTILOS", "templates/estilos.html");
	
	if(isset($_GET['estado'])){
		$estado = $_GET['estado'];
				
		$tpl->addFile("FILTROS", "templates/filtros-saude.html");
		
		$estadosBrasileiros = array(
		'AC'=>'Acre','AL'=>'Alagoas','AP'=>'Amapá','AM'=>'Amazonas','BA'=>'Bahia','CE'=>'Ceará','DF'=>'Distrito Federal','ES'=>'Espírito Santo','GO'=>'Goiás','MA'=>'Maranhão','MT'=>'Mato Grosso','MS'=>'Mato Grosso do Sul','MG'=>'Minas Gerais','PA'=>'Pará','PB'=>'Paraíba','PR'=>'Paraná','PE'=>'Pernambuco','PI'=>'Piauí','RJ'=>'Rio de Janeiro','RN'=>'Rio Grande do Norte','RS'=>'Rio Grande do Sul','RO'=>'Rondônia','RR'=>'Roraima','SC'=>'Santa Catarina','SP'=>'São Paulo','SE'=>'Sergipe','TO'=>'Tocantins'
		);

		if(array_key_exists($estado, $estadosBrasileiros))
		{
		$tpl->SIGLA = $estado;
		$tpl->ESTADO = $estadosBrasileiros[$estado];
		}
		
		$query_cidade = $pdo->query("SELECT Nome, Codigo FROM cadastro_municipios WHERE Uf = '$estado' ORDER BY Nome ASC")->fetchAll();
			foreach($query_cidade as $cidade){
				$tpl->CIDADES_FILTRO = $cidade['Nome'];
				
				$tpl->block("BLOCO_CIDADES_FILTRO");
			}
			
		$tpl->PERFIL = "1";	
				
		$query_planos = $pdo->query("SELECT token FROM cadastro_planos WHERE status = '1' AND perfil = '1' ORDER BY id DESC")->fetchAll();
		
		foreach($query_planos as $rw){
			
			$token_plano = $rw['token'];
			
		$query_perfil = $pdo->query("SELECT token_seguradora FROM assoc_planos_saude_seguradoras WHERE token_plano = '$token_plano' ORDER BY id DESC")->fetchAll();
		
		$token_seguradora = array();	
		foreach($query_perfil as $row){
			
			$token = $row['token_seguradora'];
				 if ( in_array($row['token_seguradora'], $token_seguradora) ) {
					continue;
				}
				$token_seguradora[] = $row['token_seguradora'];
				
				$token = $row['token_seguradora'];
	
				$query_seguradoras = $pdo->query("SELECT id, nome, logo, status, slug, token FROM cadastro_seguradoras WHERE status = '1' AND estado = '$estado' AND token = '$token' ORDER BY id DESC")->fetchAll();
				
				foreach($query_seguradoras as $linha){
					$tpl->NOME = $linha['nome'];
					$tpl->SLUG = $linha['slug'];
					$tpl->IMAGEM_DESTACADA = $linha['logo'];
					$tpl->TOKEN_SEGURADORA = $linha['token'];
					
					$tpl->block("BLOCO_SEGURADORAS");
					
				}
				
				
		}
		}

	$tpl->VIDAS = '<div class="vidas">
<h2>Quantidade de Vidas</h2>
<div class="row">

    <div class="form-group col-md-2">
        <label for="0_18">0 - 18 anos</label>
        <input type="number" min="0" class="form-control idade" id="0_18" name="idade1" />
    </div>
    <div class="form-group col-md-2">
        <label for="19_23">19 - 23 anos</label>
        <input type="number" min="0" class="form-control idade" id="19_23" name="idade2" />
    </div>
    <div class="form-group col-md-2">
        <label for="24_28">24 -28 anos</label>
        <input type="number" min="0" class="form-control idade" id="24_28" name="idade3" />
    </div>
    <div class="form-group col-md-2">
        <label for="29_33">29 - 33 anos</label>
        <input type="number" min="0" class="form-control idade" id="29_33" name="idade4" />
    </div>
    <div class="form-group col-md-2">
        <label for="34_38">34 - 38 anos</label>
        <input type="number" min="0" class="form-control idade" id="34_38" name="idade5" />
    </div>
    <div class="clearfix">
    
    </div>
    <div class="form-group col-md-2">
        <label for="39_43">39 - 43 anos</label>
        <input type="number" min="0" class="form-control idade" id="39_43" name="idade6" />
    </div>
    <div class="form-group col-md-2">
        <label for="44_48">44 - 48 anos</label>
        <input type="number" min="0" class="form-control idade" id="44_48" name="idade7" />
    </div>
    <div class="form-group col-md-2">
        <label for="49_53">49 - 53 anos</label>
        <input type="number" min="0" class="form-control idade" id="49_53" name="idade8" />
    </div>
    <div class="form-group col-md-2">
        <label for="54_58">54 - 58 anos</label>
        <input type="number" min="0" class="form-control idade" id="54_58" name="idade9" />
    </div>
    <div class="form-group col-md-2">
        <label for="mais_59">Acima de 59 anos</label>
        <input type="number" min="0" class="form-control idade" id="mais_59" name="idade10" />
    </div>
    <div class="col-md-2">
        <p id="total"></p>
    </div>
    <div class="clearfix"></div><br />
     <div class="col-md-12"><button type="button" class="btn btn-default add_field_button pull-right">Adicionar Excessão</button> </div>
     <div class="input_fields_wrap"></div>
</div>
</div>';
		
	$tpl->FILTRO_TITULARES = '<div class="titulares"><h2>Quantidade de Titulares</h2>
						  <div class="row">
						  	<div class="form-group col-md-2">
								<input type="number" class="form-control" id="quantidade_titulares" name="quantidade_titulares" />
							</div></div></div>';		
		
		
	}else{
		
		// Adicionando mais um arquivo HTML
		$tpl->addFile("MAPA", "templates/mapa.html");
	}
	

	
	//Dados Usuário
	$email_login = $_SESSION['email_login'];
	
	if($_SESSION['RoleUser'] == '1'){
		$tpl->block("BLOCO_CONFIGURACOES");	
	}
			
	$token_corretora = $_SESSION['TokenCorretora'];
	$query_company = $pdo->query("SELECT background, logo, nome FROM cadastro_corretoras WHERE token = '$token_corretora'")->fetchAll();
 		
		foreach($query_company as $ln){
			$nome = $ln['nome'];
			$background = $ln['background'];
			$logo = $ln['logo'];
		}
		
	$tpl->BACKGROUND = $background;
	if($logo != ""){
		$tpl->LOGO_CORRETORA = '<img src="'.ROOT.'/admin/imagens/'.$logo.'" class="img-responsive" alt="'.$nome.'" />';
	}else{
		$tpl->LOGO_CORRETORA = '<img src="'.ROOT.'/src/imagens/logo.png" class="img-responsive" alt="Simulador Group Saúde" />';	
	}
	
	$tpl->MENU3 = "active";							
	$tpl->URL = "simular-saude-familiar";
	$tpl->PAGINA = "Saúde - Familiar";
	$tpl->NOME_USUARIO = $_SESSION['NameUser'];
	$tpl->ROOT = ROOT;
    $tpl->show();

?>