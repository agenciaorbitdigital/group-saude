<?php

	require 'modulos/session-login.php';

	require 'modulos/connection-db.php';

    require_once("lib/raelgc/view/Template.php");
    use raelgc\view\Template;

    $tpl = new Template("templates/resumida-adesao.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("TOPBAR", "templates/topbar.html");

		// Adicionando mais um arquivo HTML
    $tpl->addFile("FOOTER", "templates/footer.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("SIDEBAR", "templates/sidebar.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("SCRIPTS", "templates/scripts.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("ESTILOS", "templates/estilos.html");
	
	if(isset($_GET['estado'])){
		$estado = $_GET['estado'];
				
		$tpl->addFile("FILTROS", "templates/filtros-resumida-adesao.html");
		
		$estadosBrasileiros = array(
		'AC'=>'Acre','AL'=>'Alagoas','AP'=>'Amapá','AM'=>'Amazonas','BA'=>'Bahia','CE'=>'Ceará','DF'=>'Distrito Federal','ES'=>'Espírito Santo','GO'=>'Goiás','MA'=>'Maranhão','MT'=>'Mato Grosso','MS'=>'Mato Grosso do Sul','MG'=>'Minas Gerais','PA'=>'Pará','PB'=>'Paraíba','PR'=>'Paraná','PE'=>'Pernambuco','PI'=>'Piauí','RJ'=>'Rio de Janeiro','RN'=>'Rio Grande do Norte','RS'=>'Rio Grande do Sul','RO'=>'Rondônia','RR'=>'Roraima','SC'=>'Santa Catarina','SP'=>'São Paulo','SE'=>'Sergipe','TO'=>'Tocantins'
		);

		if(array_key_exists($estado, $estadosBrasileiros))
		{
		$tpl->SIGLA = $estado;
		$tpl->ESTADO = $estadosBrasileiros[$estado];
		}
		
		$tpl->PERFIL = "4";	
			
		$query_admin = $pdo->query("SELECT id, nome FROM cadastro_administradoras WHERE status = '1' ORDER BY id DESC")->fetchAll();
		
	
		foreach($query_admin as $linha){
			
			$tpl->ADMINISTRADORA = $linha['nome'];
			$tpl->ID_ADMINISTRADORA = $linha['id'];
			
			$tpl->block("BLOCO_ADMINISTRADORAS");		
				
		}
		
		$query_emprego = $pdo->query("SELECT id, nome FROM cadastro_empregos WHERE status = '1' ORDER BY id DESC")->fetchAll();
		
	
		foreach($query_emprego as $linha){
			
			$tpl->EMPREGO = $linha['nome'];
			$tpl->ID_EMPREGO = $linha['id'];
			
			$tpl->block("BLOCO_EMPREGOS");		
				
		}
		
		$query_entidade = $pdo->query("SELECT id, nome FROM cadastro_entidades WHERE status = '1' ORDER BY id DESC")->fetchAll();
		
	
		foreach($query_entidade as $linha){
			
			$tpl->ENTIDADE = $linha['nome'];
			$tpl->ID_ENTIDADE = $linha['id'];
			
			$tpl->block("BLOCO_ENTIDADES");		
				
		}

	}else{
		
		// Adicionando mais um arquivo HTML
		$tpl->addFile("MAPA", "templates/mapa.html");
	}
	

	
	//Dados Usuário
	$email_login = $_SESSION['email_login'];
	
	if($_SESSION['RoleUser'] == '1'){
		$tpl->block("BLOCO_CONFIGURACOES");	
	}
			
	$token_corretora = $_SESSION['TokenCorretora'];
	$query_company = $pdo->query("SELECT background, logo, nome FROM cadastro_corretoras WHERE token = '$token_corretora'")->fetchAll();
 		
		foreach($query_company as $ln){
			$nome = $ln['nome'];
			$background = $ln['background'];
			$logo = $ln['logo'];
		}
		
	$tpl->BACKGROUND = $background;
	if($logo != ""){
		$tpl->LOGO_CORRETORA = '<img src="'.ROOT.'/admin/imagens/'.$logo.'" class="img-responsive" alt="'.$nome.'" />';
	}else{
		$tpl->LOGO_CORRETORA = '<img src="'.ROOT.'/src/imagens/logo.png" class="img-responsive" alt="Simulador Group Saúde" />';	
	}
	
	$tpl->MENU11 = "active";	
	$tpl->URL = "resumida-adesao-pf";
	$tpl->PAGINA = "Adesão - Pessoa Física";
	$tpl->NOME_USUARIO = $_SESSION['NameUser'];
	$tpl->ROOT = ROOT;
    $tpl->show();

?>