<?php
	
	require 'modulos/session-login.php';

	require 'modulos/connection-db.php';

    require_once("lib/raelgc/view/Template.php");
    use raelgc\view\Template;

    $tpl = new Template("templates/processa-resumida-saude.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("TOPBAR", "templates/topbar.html");

		// Adicionando mais um arquivo HTML
    $tpl->addFile("FOOTER", "templates/footer.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("SIDEBAR", "templates/sidebar.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("SCRIPTS", "templates/scripts.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("ESTILOS", "templates/estilos.html");
	
	if(!isset($_POST['seguradora'])){	
		echo "<script>alert('Não existem seguradoras cadastradas!');window.history.go(-1);</script>";	
	}else{
		
		$token = $_POST['seguradora'];
		
		$tpl->TOKEN_SEGURADORA = $token;
			
	$perfil = $_POST['perfil'];
	if($perfil == "4"){
		$tpl->PERFIL = "Pessoa Física";
	}if($perfil == "3"){
		$tpl->PERFIL = "PME";
	}
	
	$estado = $_POST['estado'];
	$estadosBrasileiros = array(
		'AC'=>'Acre','AL'=>'Alagoas','AP'=>'Amapá','AM'=>'Amazonas','BA'=>'Bahia','CE'=>'Ceará','DF'=>'Distrito Federal','ES'=>'Espírito Santo','GO'=>'Goiás','MA'=>'Maranhão','MT'=>'Mato Grosso','MS'=>'Mato Grosso do Sul','MG'=>'Minas Gerais','PA'=>'Pará','PB'=>'Paraíba','PR'=>'Paraná','PE'=>'Pernambuco','PI'=>'Piauí','RJ'=>'Rio de Janeiro','RN'=>'Rio Grande do Norte','RS'=>'Rio Grande do Sul','RO'=>'Rondônia','RR'=>'Roraima','SC'=>'Santa Catarina','SP'=>'São Paulo','SE'=>'Sergipe','TO'=>'Tocantins'
		);

		if(array_key_exists($estado, $estadosBrasileiros))
		{
		$tpl->SIGLA = $estado;
		$tpl->ESTADO = $estadosBrasileiros[$estado];
		}
	
	
	
	$query_seguradora = $pdo->query("SELECT id, nome, logo, token FROM cadastro_seguradoras WHERE token = '$token'")->fetchAll();
				
		foreach($query_seguradora as $linha){
			$tpl->SEGURADORA = $linha['nome'];
			$tpl->IMAGEM_DESTACADA = $linha['logo'];			
		}
	
	$query_assoc_seguradora = $pdo->query("SELECT token_plano FROM assoc_planos_saude_seguradoras WHERE token_seguradora = '$token' ORDER BY id DESC")->fetchAll();
				
	foreach($query_assoc_seguradora as $ln){
		$token_assoc_plano = $ln['token_plano'];	
		
	$query_planos = $pdo->query("SELECT id, nome, token FROM cadastro_planos WHERE token = '$token_assoc_plano'")->fetchAll();
				
		foreach($query_planos as $linha){
			$tpl->PLANOS = "<th>".$linha['nome']."</th>";
			
			$tpl->block("BLOCO_THEAD");	
			$tpl->block("BLOCO_THEAD_PRECOS");				
		}
	

	if(isset($_POST['preco'])){
			
		
		$preco = $_POST['preco'];
		 $tpl->PRINT_PRECO = "1";
		 
		$tpl->TD_VALOR1 = "<td>0 a 18 anos</td>";
		$tpl->TD_VALOR2 = "<td>19 a 23 anos</td>";
		$tpl->TD_VALOR3 = "<td>24 a 28 anos</td>";
		$tpl->TD_VALOR4 = "<td>29 a 33 anos</td>";
		$tpl->TD_VALOR5 = "<td>34 a 38 anos</td>";
		$tpl->TD_VALOR6 = "<td>39 a 43 anos</td>";
		$tpl->TD_VALOR7 = "<td>44 a 48 anos</td>";
		$tpl->TD_VALOR8 = "<td>49 a 53 anos</td>";
		$tpl->TD_VALOR9 = "<td>54 a 58 anos</td>";
		$tpl->TD_VALOR10 = "<td>Acima de 59 anos</td>";
		
		$query_preco = $pdo->query("SELECT 0_18, 19_23, 24_28, 29_33, 34_38, 39_43, 44_48, 49_53, 54_58, acima_59 FROM cadastro_planos WHERE token = '$token_assoc_plano'")->fetchAll();
				
		foreach($query_preco as $linha){
			$preco1 = $linha['0_18'];
			$preco2 = $linha['19_23'];
			$preco3 = $linha['24_28'];
			$preco4 = $linha['29_33'];
			$preco5 = $linha['34_38'];
			$preco6 = $linha['39_43'];
			$preco7 = $linha['44_48'];
			$preco8 = $linha['49_53'];
			$preco9 = $linha['54_58'];
			$preco10 = $linha['acima_59'];
			
			if($preco1 !== ""){
				$tpl->PRECO1 = "<th>R$ ".$preco1."</th>";
			}else{
				$tpl->PRECO1 = "<th>Preço Não Informado</th>";
			}
			
			if($preco2 !== ""){
				$tpl->PRECO2 = "<th>R$ ".$preco2."</th>";
			}else{
				$tpl->PRECO2 = "<th>Preço Não Informado</th>";
			}
			
			if($preco3 !== ""){
				$tpl->PRECO3 = "<th>R$ ".$preco3."</th>";
			}else{
				$tpl->PRECO3 = "<th>Preço Não Informado</th>";
			}
			
			if($preco4 !== ""){
				$tpl->PRECO4 = "<th>R$ ".$preco4."</th>";
			}else{
				$tpl->PRECO4 = "<th>Preço Não Informado</th>";
			}
			
			if($preco5 !== ""){
				$tpl->PRECO5 = "<th>R$ ".$preco5."</th>";
			}else{
				$tpl->PRECO5 = "<th>Preço Não Informado</th>";
			}
			
			if($preco6 !== ""){
				$tpl->PRECO6 = "<th>R$ ".$preco6."</th>";
			}else{
				$tpl->PRECO6 = "<th>Preço Não Informado</th>";
			}
			
			if($preco7 !== ""){
				$tpl->PRECO7 = "<th>R$ ".$preco7."</th>";
			}else{
				$tpl->PRECO7 = "<th>Preço Não Informado</th>";
			}
			
			if($preco8 !== ""){
				$tpl->PRECO8 = "<th>R$ ".$preco8."</th>";
			}else{
				$tpl->PRECO8 = "<th>Preço Não Informado</th>";
			}
			
			if($preco9 !== ""){
				$tpl->PRECO9 = "<th>R$ ".$preco9."</th>";
			}else{
				$tpl->PRECO9 = "<th>Preço Não Informado</th>";
			}
			
			if($preco10 !== ""){
				$tpl->PRECO10 = "<th>R$ ".$preco10."</th>";
			}else{
				$tpl->PRECO10 = "<th>Preço Não Informado</th>";
			}

			
			$tpl->block("BLOCO_PRECOS");
			$tpl->block("BLOCO_PRECOS2");
			$tpl->block("BLOCO_PRECOS3");
			$tpl->block("BLOCO_PRECOS4");
			$tpl->block("BLOCO_PRECOS5");
			$tpl->block("BLOCO_PRECOS6");
			$tpl->block("BLOCO_PRECOS7");
			$tpl->block("BLOCO_PRECOS8");
			$tpl->block("BLOCO_PRECOS9");
			$tpl->block("BLOCO_PRECOS10");		
		}
		
		$query_token_planos = $pdo->query("SELECT token FROM cadastro_planos WHERE token = '$token_assoc_plano'")->fetchAll();
				
		foreach($query_token_planos as $linha){
			$token_plano = $linha['token'];	
			
			$query_preco_ex = $pdo->query("SELECT idade, valor FROM cadastro_excessao_idade WHERE token_plano = '$token_plano'")->fetchAll();
				
			foreach($query_preco_ex as $ln){
				
				$tpl->IDADE_EX = $ln['idade'];
				$tpl->PRECO_EX = $ln['valor'];
				
				$tpl->block("BLOCO_PRECOS_EX");	
			}
			
			
		}
		
	}else{
		
		
	}

	if(isset($_POST['acomodacao'])){
		$acomodacao = $_POST['acomodacao'];
		
		$tpl->PRINT_ACOMODACAO = "1";
		
		$tpl->TD_ACOMODACAO = "<td>Acomodação</td>";
		
		$query_acomodacao = $pdo->query("SELECT acomodacao FROM cadastro_planos WHERE token = '$token_assoc_plano'")->fetchAll();
				
		foreach($query_acomodacao as $linha){
			if($linha['acomodacao'] == "1"){
				$tpl->ACOMODACAO = "<th>Quarto coletivo ou Enfermaria</th>";		
			}
			if($linha['acomodacao'] == "2"){
				$tpl->ACOMODACAO = "<th>Quarto privativo ou Apartamento</th>";	
			}
			
			
			$tpl->block("BLOCO_ACOMODACAO");		
		}
	}
	if(isset($_POST['reembolso'])){
		$reembolso = $_POST['reembolso'];
		
		$tpl->PRINT_REEMBOLSO = "1";
		
		$tpl->TD_REEMBOLSO = "<td>Rembolso</td>";
		
		$query_reembolso = $pdo->query("SELECT reembolso FROM cadastro_planos WHERE token = '$token_assoc_plano'")->fetchAll();
				
		foreach($query_reembolso as $linha){
			$tpl->REEMBOLSOS = "<th>".$linha['reembolso']."</th>";	
			
			$tpl->block("BLOCO_REEMBOLSO");		
		}
	}
	
	if(isset($_POST['taxa'])){
		$taxa = $_POST['taxa'];
		
		$tpl->PRINT_TAXA = "1";
		
		$query_taxa = $pdo->query("SELECT taxa_inscricao FROM cadastro_seguradoras WHERE token = '$token'")->fetchAll();
				
		foreach($query_taxa as $linha){
			if($linha['taxa_inscricao'] != ""){
				$tpl->TAXA = "<h2>Taxa de Inscrição: ".$linha['taxa_inscricao']."</h2>";
			}else{
				$tpl->TAXA = "<h2>Taxa de Inscrição: Não Informado</h2>";	
			}
		}
	}
	
	if(isset($_POST['atuacao'])){
		$atuacao = $_POST['atuacao'];
		
		$tpl->PRINT_ABRANGENCIA = "1";
		
		$tpl->TD_ABRANGENCIA = "<td>Abrangência</td>";
		
		$query_abrangencia = $pdo->query("SELECT atuacao FROM cadastro_planos WHERE token = '$token_assoc_plano'")->fetchAll();
				
		foreach($query_abrangencia as $linha){
			$tpl->ABRANGENCIAS = "<th>".$linha['atuacao']."</th>";	
			
			$tpl->block("BLOCO_ABRANGENCIA");		
		}
	}
	if(isset($_POST['coparticipacao'])){
		$coparticipacao = $_POST['coparticipacao'];	
		
		$tpl->PRINT_COPARTICIPACAO = "1";
		
		$tpl->TD_COPARTICIPACAO = "<td>Coparticipação</td>";
		
		$query_coparticipacao = $pdo->query("SELECT coparticipacao FROM cadastro_planos WHERE token = '$token_assoc_plano'")->fetchAll();
				
		foreach($query_coparticipacao as $linha){
			$tpl->COPARTICIPACOES = "<th>".$linha['coparticipacao']."</th>";	
			
			$tpl->block("BLOCO_COPARTICIPACAO");		
		}
	}
		
	if(isset($_POST['carencia'])){
		$carencia = $_POST['carencia'];
		
		$tpl->PRINT_CARENCIA = "1";
		
		$tpl->TD_CARENCIA = "<td>Carência</td>";
		
		$query_id_carencia = $pdo->query("SELECT carencia FROM cadastro_planos WHERE token = '$token_assoc_plano'")->fetchAll();
				
		foreach($query_id_carencia as $linha){
			$id_carencia = $linha['carencia'];
			
			$query_carencia = $pdo->query("SELECT slug FROM cadastro_carencias WHERE id = '$id_carencia'")->fetchAll();
				
			foreach($query_carencia as $row){
			
				$tpl->CARENCIAS = $row['slug'];	
				
				$tpl->block("BLOCO_CARENCIA");		
			}	
		}
	}
	
	
	
	if(isset($_POST['rede_credenciada'])){
		
		
	}
		
//INFORMAÇOES	
	if(isset($_POST['informacoes'])){
		$informacoes = $_POST['informacoes'];
		
		$tpl->PRINT_INFORMACOES = "1";
		
		$query_id_informacoes = $pdo->query("SELECT id_informacoes, nome FROM cadastro_planos WHERE token = '$token_assoc_plano'")->fetchAll();
		
		
		foreach($query_id_informacoes as $rw){
			
			$id_informacoes = $rw['id_informacoes'];
			
			$query_informacoes = $pdo->query("SELECT * FROM cadastro_informacoes_adicionais WHERE id = '$id_informacoes' AND status = '1'")->fetchAll();
		
		
			foreach($query_informacoes as $row){
			
					$tpl->NAME_PLANO = $rw['nome'];

				if($row['informacoes_adicionais'] OR $row['documentacao'] != ""){
					
					$tpl->INFORMACOES = $row['informacoes_adicionais'];	
					$tpl->DOCUMENTOS = $row['documentacao'];	

					$tpl->block("BLOCO_INFORMACOES");
					
				}else{

					$tpl->SEM_INFORMACOES = "Nenhuma informação adicional encontrada!";
				}

				
			}
		}
			
			
	}
 //INFORMAÇOES
	}
	}
	$tpl->REFERENCIA = ucfirst(strftime("%B/ %Y"));
			
	$token_corretora = $_SESSION['TokenCorretora'];
	$query_company = $pdo->query("SELECT background, logo, nome FROM cadastro_corretoras WHERE token = '$token_corretora'")->fetchAll();
 		
		foreach($query_company as $ln){
			$nome = $ln['nome'];
			$background = $ln['background'];
			$logo = $ln['logo'];
		}
		
	$tpl->BACKGROUND = $background;
	if($logo != ""){
		$tpl->LOGO_CORRETORA = '<img src="'.ROOT.'/admin/imagens/'.$logo.'" class="img-responsive" alt="'.$nome.'" />';
	}else{
		$tpl->LOGO_CORRETORA = '<img src="'.ROOT.'/src/imagens/logo.png" class="img-responsive" alt="Simulador Group Saúde" />';	
	}
	
	$tpl->MENU2 = "active";	
	$tpl->NOME_USUARIO = $_SESSION['NameUser'];
	$tpl->ROOT = ROOT;
    $tpl->show();

?>