<?php
	
	require 'modulos/session-login.php';

	require 'modulos/connection-db.php';

    require_once("lib/raelgc/view/Template.php");
    use raelgc\view\Template;

    $tpl = new Template("templates/tabela-resumida-saude.html");
	
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("SCRIPTS", "templates/scripts.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("ESTILOS", "templates/estilos.html");
	
	$token = $_GET['seguradora'];
			
	$tpl->PERFIL = $_GET['perfil'];
	
	
	$estado = $_GET['estado'];
	$estadosBrasileiros = array(
		'AC'=>'Acre','AL'=>'Alagoas','AP'=>'Amapá','AM'=>'Amazonas','BA'=>'Bahia','CE'=>'Ceará','DF'=>'Distrito Federal','ES'=>'Espírito Santo','GO'=>'Goiás','MA'=>'Maranhão','MT'=>'Mato Grosso','MS'=>'Mato Grosso do Sul','MG'=>'Minas Gerais','PA'=>'Pará','PB'=>'Paraíba','PR'=>'Paraná','PE'=>'Pernambuco','PI'=>'Piauí','RJ'=>'Rio de Janeiro','RN'=>'Rio Grande do Norte','RS'=>'Rio Grande do Sul','RO'=>'Rondônia','RR'=>'Roraima','SC'=>'Santa Catarina','SP'=>'São Paulo','SE'=>'Sergipe','TO'=>'Tocantins'
		);

		if(array_key_exists($estado, $estadosBrasileiros))
		{
		$tpl->SIGLA = $estado;
		$tpl->ESTADO = $estadosBrasileiros[$estado];
		}
	
	
	
	$query_seguradora = $pdo->query("SELECT id, nome, logo, token FROM cadastro_seguradoras WHERE token = '$token'")->fetchAll();
				
		foreach($query_seguradora as $linha){
			$tpl->SEGURADORA = $linha['nome'];
			$tpl->IMAGEM_DESTACADA = $linha['logo'];			
		}
	
	$query_planos = $pdo->query("SELECT id, nome, token FROM cadastro_planos WHERE token_seguradora = '$token'")->fetchAll();
				
		foreach($query_planos as $linha){
			$tpl->PLANOS = "<th>".$linha['nome']."</th>";
			
			$tpl->block("BLOCO_THEAD");	
			$tpl->block("BLOCO_THEAD_PRECOS");				
		}
				
	if(isset($_GET['preco']) && ($_GET['preco'] == "1")){
		$preco = $_GET['preco'];
		 
		$tpl->TD_VALOR1 = "<td>0 a 18 anos</td>";
		$tpl->TD_VALOR2 = "<td>19 a 23 anos</td>";
		$tpl->TD_VALOR3 = "<td>24 a 28 anos</td>";
		$tpl->TD_VALOR4 = "<td>29 a 33 anos</td>";
		$tpl->TD_VALOR5 = "<td>34 a 38 anos</td>";
		$tpl->TD_VALOR6 = "<td>39 a 43 anos</td>";
		$tpl->TD_VALOR7 = "<td>44 a 48 anos</td>";
		$tpl->TD_VALOR8 = "<td>49 a 53 anos</td>";
		$tpl->TD_VALOR9 = "<td>54 a 58 anos</td>";
		$tpl->TD_VALOR10 = "<td>Acima de 59 anos</td>";
		
		$query_preco = $pdo->query("SELECT 0_18, 19_23, 24_28, 29_33, 34_38, 39_43, 44_48, 49_53, 54_58, acima_59 FROM cadastro_planos WHERE token_seguradora = '$token'")->fetchAll();
				
		foreach($query_preco as $linha){
			$tpl->PRECO1 = "<th>R$ ".$linha['0_18']."</th>";
			$tpl->PRECO2 = "<th>R$ ".$linha['19_23']."</th>";	
			$tpl->PRECO3 = "<th>R$ ".$linha['24_28']."</th>";	
			$tpl->PRECO4 = "<th>R$ ".$linha['29_33']."</th>";	
			$tpl->PRECO5 = "<th>R$ ".$linha['34_38']."</th>";	
			$tpl->PRECO6 = "<th>R$ ".$linha['39_43']."</th>";	
			$tpl->PRECO7 = "<th>R$ ".$linha['44_48']."</th>";	
			$tpl->PRECO8 = "<th>R$ ".$linha['49_53']."</th>";	
			$tpl->PRECO9 = "<th>R$ ".$linha['54_58']."</th>";	
			$tpl->PRECO10 = "<th>R$ ".$linha['acima_59']."</th>";	
			
			$tpl->block("BLOCO_PRECOS");
			$tpl->block("BLOCO_PRECOS2");
			$tpl->block("BLOCO_PRECOS3");
			$tpl->block("BLOCO_PRECOS4");
			$tpl->block("BLOCO_PRECOS5");
			$tpl->block("BLOCO_PRECOS6");
			$tpl->block("BLOCO_PRECOS7");
			$tpl->block("BLOCO_PRECOS8");
			$tpl->block("BLOCO_PRECOS9");
			$tpl->block("BLOCO_PRECOS10");		
		}
		
		$query_token_planos = $pdo->query("SELECT token FROM cadastro_planos WHERE token_seguradora = '$token'")->fetchAll();
				
		foreach($query_token_planos as $linha){
			$token_plano = $linha['token'];	
			
			$query_preco_ex = $pdo->query("SELECT idade, valor FROM cadastro_excessao_idade WHERE token_plano = '$token_plano'")->fetchAll();
				
			foreach($query_preco_ex as $ln){
				
				$tpl->IDADE_EX = $ln['idade'];
				$tpl->PRECO_EX = $ln['valor'];
				
				$tpl->block("BLOCO_PRECOS_EX");	
			}
		}
		
	}

	if(isset($_GET['acomodacao']) && ($_GET['acomodacao'] == "1")){
		$acomodacao = $_GET['acomodacao'];
		
		$tpl->TD_ACOMODACAO = "<td>Acomodação</td>";
		
		$query_acomodacao = $pdo->query("SELECT acomodacao FROM cadastro_planos WHERE token_seguradora = '$token'")->fetchAll();
				
		foreach($query_acomodacao as $linha){
			if($linha['acomodacao'] == "1"){
				$tpl->ACOMODACAO = "<th>Quarto coletivo ou Enfermaria</th>";		
			}
			if($linha['acomodacao'] == "2"){
				$tpl->ACOMODACAO = "<th>Quarto privativo ou Apartamento</th>";	
			}
			
			
			$tpl->block("BLOCO_ACOMODACAO");		
		}
	}
	if(isset($_GET['reembolso']) && ($_GET['reembolso'] == "1")){
		$reembolso = $_GET['reembolso'];
		
		$tpl->TD_REEMBOLSO = "<td>Rembolso</td>";
		
		$query_reembolso = $pdo->query("SELECT reembolso FROM cadastro_planos WHERE token_seguradora = '$token'")->fetchAll();
				
		foreach($query_reembolso as $linha){
			$tpl->REEMBOLSOS = "<th>".$linha['reembolso']."</th>";	
			
			$tpl->block("BLOCO_REEMBOLSO");		
		}
	}
	
	if(isset($_GET['taxa']) && ($_GET['taxa'] == "1")){
		$taxa = $_GET['taxa'];
		
		$query_taxa = $pdo->query("SELECT taxa_inscricao FROM cadastro_seguradoras WHERE token = '$token'")->fetchAll();
				
		foreach($query_taxa as $linha){
			if($linha['taxa_inscricao'] != ""){
				$tpl->TAXA = "<h2>Taxa de Inscrição: ".$linha['taxa_inscricao']."</h2>";
			}else{
				$tpl->TAXA = "<h2>Taxa de Inscrição: Não Informado</h2>";	
			}
		}
	}
	
	if(isset($_GET['atuacao']) && ($_GET['atuacao'] == "1")){
		$atuacao = $_GET['atuacao'];
		
		$tpl->TD_ABRANGENCIA = "<td>Abrangência</td>";
		
		$query_abrangencia = $pdo->query("SELECT atuacao FROM cadastro_planos WHERE token_seguradora = '$token'")->fetchAll();
				
		foreach($query_abrangencia as $linha){
			$tpl->ABRANGENCIAS = "<th>".$linha['atuacao']."</th>";	
			
			$tpl->block("BLOCO_ABRANGENCIA");		
		}
	}
	if(isset($_GET['coparticipacao']) && ($_GET['coparticipacao'] == "1")){
		$coparticipacao = $_GET['coparticipacao'];	
		
		$tpl->TD_COPARTICIPACAO = "<td>Coparticipação</td>";
		
		$query_coparticipacao = $pdo->query("SELECT coparticipacao FROM cadastro_planos WHERE token_seguradora = '$token'")->fetchAll();
				
		foreach($query_coparticipacao as $linha){
			$tpl->COPARTICIPACOES = "<th>".$linha['coparticipacao']."</th>";	
			
			$tpl->block("BLOCO_COPARTICIPACAO");		
		}
	}
		
	if(isset($_GET['carencia'])){
		$carencia = $_GET['carencia'];
		
		$tpl->TD_CARENCIA = "<td>Carência</td>";
		
		$query_id_carencia = $pdo->query("SELECT carencia FROM cadastro_planos WHERE token_seguradora = '$token'")->fetchAll();
				
		foreach($query_id_carencia as $linha){
			$id_carencia = $linha['carencia'];
			
			$query_carencia = $pdo->query("SELECT id FROM cadastro_carencias WHERE id = '$id_carencia'")->fetchAll();
				
			foreach($query_carencia as $row){
			
				$tpl->CARENCIAS = $row['id'];	
				
				$tpl->block("BLOCO_CARENCIA");		
			}	
		}
	}
	
	
	if(isset($_GET['rede_credenciada']) && ($_GET['rede_credenciada'] == "1")){
		$rede_credenciada = $_GET['rede_credenciada'];
		
		$query_token_plano = $pdo->query("SELECT token, nome FROM cadastro_planos WHERE token_seguradora = '$token'")->fetchAll();
				
		foreach($query_token_plano as $linha){
			$id_plano = $linha['token'];
			$tpl->NOME_PLANO = $linha['nome'];
			
			$query_rede_credenciada = $pdo->query("SELECT id_rede_credenciada FROM assoc_planos_rede_credenciada WHERE token_plano = '$id_plano'")->fetchAll();
				
			foreach($query_rede_credenciada as $ln){
				$id_rede_credenciada = $ln['id_rede_credenciada'];
				
				$query_unidades = $pdo->query("SELECT id, nome, token FROM cadastro_rede_credenciada WHERE id = '$id_rede_credenciada'")->fetchAll();
				
				foreach($query_unidades as $row){
					$token_rede_credenciada = $row['token'];
					$tpl->UNIDADES = $row['nome'];	
					
					if(isset($_GET['especialidades'])){				
					$query_id_especialidade = $pdo->query("SELECT id_especialidade FROM assoc_rede_credenciada_especialidades WHERE token_rede_credenciada = '$token_rede_credenciada'")->fetchAll();
				
						foreach($query_id_especialidade as $ln){
							$id_especialidade = $ln['id_especialidade'];
							
							$query_especialidades = $pdo->query("SELECT id, nome FROM cadastro_especialidades WHERE id = '$id_especialidade'")->fetchAll();
							
							foreach($query_especialidades as $row){
								
								$tpl->ESPECIALIDADES = $row['nome'];					
											
							}	
							
			
							$tpl->block("BLOCO_ESPECIALIDADE");
						}
					}
				}	
				
				$tpl->block("BLOCO_UNIDADE");	
			}
			
			
			
			$count_unidades = count($query_rede_credenciada);
			

			if($count_unidades == 0){
				$tpl->SEM_UNIDADES = "Nenhum Unidade Cadastrada!";
			}else{
				$tpl->SEM_UNIDADES = "";
			}
			
			$tpl->block("BLOCO_REDE_CREDENCIADA");			
		}
		
		
	}
	
	if(isset($_GET['informacoes']) && ($_GET['informacoes'] == "1")){
		$informacoes = $_GET['informacoes'];
		
		$query_informacoes = $pdo->query("SELECT informacoes_adicionais, documentos, nome FROM cadastro_planos WHERE token_seguradora = '$token'")->fetchAll();
			
		foreach($query_informacoes as $row){
			
			$tpl->NAME_PLANO = $row['nome'];
			
			if($row['informacoes_adicionais'] OR $row['documentos'] != ""){
				
			}else{

				$tpl->SEM_INFORMACOES = "Nenhuma informação adicional encontrada!";
			}
			
			$tpl->INFORMACOES = $row['informacoes_adicionais'];	
			$tpl->DOCUMENTOS = $row['documentos'];	
				
			$tpl->block("BLOCO_INFORMACOES");		
		}
			
			if($row['informacoes_adicionais'] != ""){
				$tpl->TIT_INFORMACOES = "<h4>Informações Adicionais</h4>";	
			}else{
				$tpl->TIT_INFORMACOES = "";	
			}
			if($row['documentos'] != ""){
				$tpl->TIT_DOCUMENTOS = "<h4>Documentação Necessária</h4>";	
			}else{
				$tpl->TIT_DOCUMENTOS = "";	
			}
	}

	$tpl->REFERENCIA = ucfirst(strftime("%B/ %Y"));
			
	$token_corretora = $_SESSION['TokenCorretora'];
	$query_company = $pdo->query("SELECT background, logo, nome, endereco, cidade, estado, bairro, cep, numero, complemento FROM cadastro_corretoras WHERE token = '$token_corretora'")->fetchAll();
 		
		foreach($query_company as $ln){
			$nome = $ln['nome'];
			$background = $ln['background'];
			$logo = $ln['logo'];
			$endereco = $ln['endereco'];
			$num = $ln['numero'];
			$bairro = $ln['bairro'];
			$cidade = $ln['cidade'];
			$estado = $ln['estado'];
			$cep = $ln['cep'];
			$complemento = $ln['complemento'];
		}
	$tpl->ENDERECO = $endereco.", ".$num." - ".$complemento." - ".$bairro."<br />".$cidade."/ ".$estado." - CEP: ".$cep;
	$tpl->CORRETORA = $nome;
	
	$email_login = $_SESSION['email_login'];
	
	$query_id = $pdo->query("SELECT celular FROM cadastro_usuarios WHERE email = '$email_login'")->fetchAll();
		foreach($query_id as $ln){
			$telefone = $ln['celular'];
		}
	$tpl->TELEFONE = $telefone;
	$tpl->EMAIL = $email_login;
	
	if($logo != ""){
		$tpl->LOGO_CORRETORA = '<img src="'.ROOT.'/admin/imagens/'.$logo.'" class="img-responsive" alt="'.$nome.'" />';
	}else{
		$tpl->LOGO_CORRETORA = '<img src="'.ROOT.'/src/imagens/logo.png" class="img-responsive" alt="Simulador Group Saúde" />';	
	}
	

	$tpl->NOME_USUARIO = $_SESSION['NameUser'];
	$tpl->ROOT = ROOT;
    $tpl->show();

?>