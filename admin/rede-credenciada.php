<?php

	require 'modulos/session-login.php';

	require '../modulos/connection-db.php';

    require_once("lib/raelgc/view/Template.php");
    use raelgc\view\Template;

    $tpl = new Template("templates/rede-credenciada.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("TOPBAR", "templates/topbar.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("SIDEBAR", "templates/sidebar.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("SCRIPTS", "templates/scripts.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("ESTILOS", "templates/estilos.html");
	
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("FOOTER", "templates/footer.html");
	
	
	//Dados Usuário
	$email_login = $_SESSION['email_login'];
	
	$query_usuario = $pdo->query("SELECT id, nome, data_criacao FROM cadastro_usuarios WHERE email = '$email_login'")->fetchAll();
 		
		foreach($query_usuario as $linha){
			$id_usuario = $linha['id'];
			$nome = $linha['nome'];
			$data_cadastro = $linha['data_criacao'];
			
			$_SESSION['UserName'] = $linha['nome'];
		}
		
	$query_hospitais = $pdo->query("SELECT id, nome, cidade, estado, status, slug, ps_pronto_socorro, psi_pronto_socorro_infantil, m_maternidade, h_hospital, pa_pronto_atendimento, a_ambulatorio FROM cadastro_rede_credenciada ORDER BY id DESC")->fetchAll();
			
			foreach($query_hospitais as $linha){
				$tpl->ID = $linha['id'];
				$nome = $linha['nome'];
				$tpl->CIDADE = $linha['cidade'];
				$tpl->UF = $linha['estado'];
				$tpl->SLUG = $linha['slug'];
				if($linha['status'] == 1){
					$tpl->STATUS = "Ativo";
				}if($linha['status'] == 0){
					$tpl->STATUS = "Inativo";
				}
				if($linha['status'] == 1){
					$tpl->STATUS = "Ativo";
				}if($linha['status'] == 0){
					$tpl->STATUS = "Inativo";
				}
				
				if($linha['a_ambulatorio'] == 1){
					$ambulatorio = "A / ";
					}else{
						$ambulatorio = "";
					}
					if($linha['h_hospital'] == 1){
					$hospital = "H / ";
					}else{
						$hospital = "";
					}
					if($linha['m_maternidade'] == 1){
					$maternidade = "M / ";
					}else{
						$maternidade = "";
					}
					if($linha['pa_pronto_atendimento'] == 1){
					$p_atendimento = "PA / ";
					}else{
						$p_atendimento = "";
					}
					if($linha['ps_pronto_socorro'] == 1){
					$p_socorro = "PS ";
					}else{
						$p_socorro = "";
					}
					
					if($linha['a_ambulatorio'] OR $linha['h_hospital'] OR $linha['m_maternidade'] OR $linha['pa_pronto_atendimento'] OR $linha['ps_pronto_socorro'] == 1){
					$tpl->NOME = $nome ." - " .$ambulatorio . $hospital . $maternidade .$p_atendimento . $p_socorro;
					}else{
						$tpl->NOME = $nome;	
					}
					
				$tpl->block("BLOCO_LISTAGEM");
			}
			
	$tpl->ROOT = ROOT;
		
    $tpl->DATA = date('Y');
	
	$tpl->NOME_USUARIO = $_SESSION['NameUser'];
	$tpl->MENU3 = "active";
	$tpl->MENU3_1 = "active";
    $tpl->show();

?>