<?php

	require 'modulos/session-login.php';

	require '../modulos/connection-db.php';

    require_once("lib/raelgc/view/Template.php");
    use raelgc\view\Template;

    $tpl = new Template("templates/add-plano-adesao.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("TOPBAR", "templates/topbar.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("SIDEBAR", "templates/sidebar.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("SCRIPTS", "templates/scripts.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("ESTILOS", "templates/estilos.html");
	
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("FOOTER", "templates/footer.html");
	
	
	//Dados Usuário
	$email_login = $_SESSION['email_login'];
	
	$query_usuario = $pdo->query("SELECT id, nome, data_criacao FROM cadastro_usuarios WHERE email = '$email_login'")->fetchAll();
 		
		foreach($query_usuario as $linha){
			$id_usuario = $linha['id'];
			$nome = $linha['nome'];
			$data_cadastro = $linha['data_criacao'];
			
			$_SESSION['UserName'] = $linha['nome'];
		}
		
	$query_hospital = $pdo->query("SELECT id, nome FROM cadastro_rede_credenciada ORDER BY id DESC")->fetchAll();
			
			foreach($query_hospital as $linha){
				$tpl->ID_HOSPITAL = $linha['id'];
				$tpl->HOSPITAL = $linha['nome'];
				
				
				$tpl->block("BLOCO_HOSPITAL");
			}	
			
	$query_carencia = $pdo->query("SELECT id, nome FROM cadastro_carencias ORDER BY id DESC")->fetchAll();
			
			foreach($query_carencia as $linha){
				$tpl->ID_CARENCIA = $linha['id'];
				$tpl->CARENCIA = $linha['nome'];
				
				
				$tpl->block("BLOCO_CARENCIAS");
			}
	$query_reembolso = $pdo->query("SELECT id, nome FROM cadastro_reembolsos WHERE status = '1' ORDER BY id DESC")->fetchAll();
			
			foreach($query_reembolso as $linha){
				$tpl->ID_REEMBOLSO = $linha['id'];
				$tpl->REEMBOLSO = $linha['nome'];
				
				
				$tpl->block("BLOCO_REEMBOLSOS");
			}	
			
	$query_administradoras = $pdo->query("SELECT id, nome FROM cadastro_administradoras ORDER BY id DESC")->fetchAll();
			
			foreach($query_administradoras as $linha){
				$tpl->ID_ADMINISTRADORA = $linha['id'];
				$tpl->ADMINISTRADORA = $linha['nome'];
				
				
				$tpl->block("BLOCO_ADMINISTRADORAS");
			}	
			
	$query_operadoras = $pdo->query("SELECT id, nome FROM cadastro_operadoras ORDER BY id DESC")->fetchAll();
			
			foreach($query_operadoras as $linha){
				$tpl->ID_OPERADORA = $linha['id'];
				$tpl->OPERADORA = $linha['nome'];
				
				
				$tpl->block("BLOCO_OPERADORAS");
			}	
			
	$query_entidades = $pdo->query("SELECT id, nome FROM cadastro_entidades ORDER BY id DESC")->fetchAll();
			
			foreach($query_entidades as $linha){
				$tpl->ID_ENTIDADE = $linha['id'];
				$tpl->ENTIDADE = $linha['nome'];
				
				
				$tpl->block("BLOCO_ENTIDADES");
			}	
			
	
	$query_informacoes = $pdo->query("SELECT id, nome FROM cadastro_informacoes_adicionais ORDER BY id DESC")->fetchAll();
			
			foreach($query_informacoes as $linha){
				$tpl->ID_INFORMACOES = $linha['id'];
				$tpl->INFORMACOES = $linha['nome'];
				
				
				$tpl->block("BLOCO_INFORMACOES");
			}
					
	$tpl->TOKEN = md5(uniqid(rand(), true));
	
	$tpl->ROOT = ROOT;
		
    $tpl->DATA = date('Y');
	
	$tpl->NOME_USUARIO = $_SESSION['NameUser'];
	$tpl->MENU4 = "active";
	$tpl->MENU4_6 = "active";
    $tpl->show();

?>