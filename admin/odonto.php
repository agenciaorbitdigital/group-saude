<?php

	require 'modulos/session-login.php';

	require '../modulos/connection-db.php';

    require_once("lib/raelgc/view/Template.php");
    use raelgc\view\Template;

    $tpl = new Template("templates/seguradora-odonto.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("TOPBAR", "templates/topbar.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("SIDEBAR", "templates/sidebar.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("SCRIPTS", "templates/scripts.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("ESTILOS", "templates/estilos.html");
	
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("FOOTER", "templates/footer.html");
	
	
	//Dados Usuário
	$email_login = $_SESSION['email_login'];
	
	//Dados Usuário
	$email_login = $_SESSION['email_login'];
	
	$slug = $_GET['slug'];
	
	$estadosBrasileiros = array(
	'AC'=>'Acre',
	'AL'=>'Alagoas',
	'AP'=>'Amapá',
	'AM'=>'Amazonas',
	'BA'=>'Bahia',
	'CE'=>'Ceará',
	'DF'=>'Distrito Federal',
	'ES'=>'Espírito Santo',
	'GO'=>'Goiás',
	'MA'=>'Maranhão',
	'MT'=>'Mato Grosso',
	'MS'=>'Mato Grosso do Sul',
	'MG'=>'Minas Gerais',
	'PA'=>'Pará',
	'PB'=>'Paraíba',
	'PR'=>'Paraná',
	'PE'=>'Pernambuco',
	'PI'=>'Piauí',
	'RJ'=>'Rio de Janeiro',
	'RN'=>'Rio Grande do Norte',
	'RS'=>'Rio Grande do Sul',
	'RO'=>'Rondônia',
	'RR'=>'Roraima',
	'SC'=>'Santa Catarina',
	'SP'=>'São Paulo',
	'SE'=>'Sergipe',
	'TO'=>'Tocantins'
	);
	
	$FundoReserva = array(
	'1'=>'Com fundo de reserva',
	'2'=>'Sem fundo de reserva'
	);
	
	$Atuacao = array(
	'Regional'=>'Regional',
	'Nacional'=>'Nacional'
	);
	
	$Reembolso = array(
	'Sim'=>'Sim',
	'Não'=>'Não'
	);
	
	$Coparticipacao = array(
	'Sim'=>'Sim',
	'Não'=>'Não'
	);
	
	$Perfil = array(
	'1'=>'Familiar',
	'2'=>'Individual',
	'3'=>'PME'
	);

	$query_seguradoras_odonto = $pdo->query("SELECT id, nome, status, token, data_cadastro, telefone_clientes, telefone_vendedores, logo, estado, taxa_inscricao, min_vidas, max_vidas FROM cadastro_seguradoras_odonto WHERE slug = '$slug'")->fetchAll();
			
			foreach($query_seguradoras_odonto as $linha){
				$token = $linha['token'];
				$tpl->TOKEN = $token;
				$tpl->ID = $linha['id'];
				$tpl->TELEFONE1 = $linha['telefone_clientes'];
				$tpl->TELEFONE2 = $linha['telefone_vendedores'];
				$tpl->NOME = $linha['nome'];
				$estado_atual = $linha['estado'];
				$tpl->DATA_CADASTRO = $linha['nome'];
				$tpl->IMAGEM_DESTACADA = $linha['logo'];
				$tpl->ESTADO = $linha['estado'];
				$tpl->TAXA_INSCRICAO = $linha['taxa_inscricao'];
				$tpl->MIN_VIDAS = $linha['min_vidas'];
				$tpl->MAX_VIDAS = $linha['max_vidas'];
				
				
				if($linha['status'] == 1){
				$tpl->STATUS_EXPL = "Ativo";
				$tpl->STATUS_CHECK = "checked";
			}else{
				$tpl->STATUS_EXPL = "Inativo";	
				$tpl->STATUS_CHECK = "";
			}
			}
			
	
	foreach($estadosBrasileiros as $value => $text){

		$tpl->SIGLA = $value;
		$tpl->ESTADO = $text;
	
	// Vendo se a opção atual deve ter o atributo "selected"
		if($estado_atual == $value) $tpl->SELECTED = "selected";

		// Caso esta não seja a opção atual, limpamos o valor da variável SELECTED
		else $tpl->clear("SELECTED");

		$tpl->block("BLOCO_ESTADO");	

	}
	
	/*$query_planos = $pdo->query("SELECT * FROM cadastro_planos_odonto WHERE token_seguradora = '$token'")->fetchAll();
			

			foreach($query_planos as $ln){
				$token_plano = $ln['token'];
				$atuacao_atual = $ln['atuacao'];
				$perfil_atual = $ln['perfil'];
				$fundoreserva_atual = $ln['fundo_reserva'];
				$reembolso_atual = $ln['reembolso'];
				$coparticipacao_atual = $ln['coparticipacao'];
				$id_carencia = $ln['carencia'];
				$tpl->ID_PLANO = $ln['id'];
				$tpl->NOME_PLANO = $ln['nome'];
				$tpl->TOKEN_PLANO = $ln['token'];
				$tpl->PRECO = $ln['preco'];
				$tpl->DOCUMENTACAO = $ln['documentos'];
				$tpl->MAIS_INFORMACOES = $ln['informacoes_adicionais'];
				
				
				foreach($FundoReserva as $value => $text){

					$tpl->VALOR1 = $value;
					$tpl->TEXTO1 = $text;
				
				
				// Vendo se a opção atual deve ter o atributo "selected"
					if($fundoreserva_atual == $value) $tpl->SELECTED2 = "selected";
			
					// Caso esta não seja a opção atual, limpamos o valor da variável SELECTED
					else $tpl->clear("SELECTED2");
			
					$tpl->block("BLOCO_FUNDO");	
			
				}
				foreach($Atuacao as $value => $text){

					$tpl->VALOR2 = $value;
					$tpl->TEXTO2 = $text;
				
				
				// Vendo se a opção atual deve ter o atributo "selected"
					if($atuacao_atual == $value) $tpl->SELECTED3 = "selected";
			
					// Caso esta não seja a opção atual, limpamos o valor da variável SELECTED
					else $tpl->clear("SELECTED3");
			
					$tpl->block("BLOCO_ATUACAO");	
			
				}
				foreach($Reembolso as $value => $text){
			
					$tpl->VALOR3 = $value;
					$tpl->TEXTO3 = $text;
				
				// Vendo se a opção atual deve ter o atributo "selected"
					if($reembolso_atual == $value) $tpl->SELECTED4 = "selected";
			
					// Caso esta não seja a opção atual, limpamos o valor da variável SELECTED
					else $tpl->clear("SELECTED4");
			
					$tpl->block("BLOCO_REEMBOLSO");	
			
				}
				foreach($Coparticipacao as $value => $text){
			
					$tpl->VALOR4 = $value;
					$tpl->TEXTO4 = $text;
				
				// Vendo se a opção atual deve ter o atributo "selected"
					if($coparticipacao_atual == $value) $tpl->SELECTED5 = "selected";
			
					// Caso esta não seja a opção atual, limpamos o valor da variável SELECTED
					else $tpl->clear("SELECTED5");
			
					$tpl->block("BLOCO_COPARTICIPACAO");	
			
				}
				
				foreach($Perfil as $value => $text){
			
					$tpl->VALOR5 = $value;
					$tpl->TEXTO5 = $text;
				
				// Vendo se a opção atual deve ter o atributo "selected"
					if($perfil_atual == $value) $tpl->SELECTED6 = "selected";
			
					// Caso esta não seja a opção atual, limpamos o valor da variável SELECTED
					else $tpl->clear("SELECTED6");
			
					$tpl->block("BLOCO_PERFIL");	
			
				}
				
				$query_carencia = $pdo->query("SELECT id, nome, status FROM cadastro_carencias ORDER BY id DESC")->fetchAll();
			
				foreach($query_carencia as $row){
					$tpl->ID_CARENCIA = $row['id'];
					$tpl->NOME_CARENCIA = $row['nome'];
					
					if($id_carencia == $row['id']){
						$tpl->SELECT_CARENCIA = "selected";
					}else{
						$tpl->SELECT_CARENCIA = "";
					}
					
				$tpl->block("BLOCO_CARENCIAS_ATUAL");
				}
				
				$query_unidades_select = $pdo->query("SELECT id, nome, status FROM cadastro_rede_credenciada WHERE id IN (SELECT id_rede_credenciada FROM assoc_planos_odonto_rede_credenciada WHERE token_plano = '$token_plano')")->fetchAll();
			
					foreach($query_unidades_select as $linha){
						$id_especialidade = $linha['id'];
						$tpl->ID_UNIDADE_SELECT = $linha['id'];
						$tpl->UNIDADE_SELECT = $linha['nome'];
						
						$tpl->SELECIONADOS_UNIDADES = "selected";
						
						$tpl->block("BLOCO_UNIDADES_SELECIONADOS");
					}
	
				$query_unidades = $pdo->query("SELECT id, nome, status FROM cadastro_rede_credenciada WHERE id NOT IN (SELECT id_rede_credenciada FROM assoc_planos_odonto_rede_credenciada WHERE token_plano = '$token_plano')")->fetchAll();
					
					foreach($query_unidades as $linha){
						$tpl->ID_UNIDADE = $linha['id'];
						$tpl->UNIDADE = $linha['nome'];
		
						if($linha['status'] == 1){
							$tpl->STATUS_UNIDADE = "";
							$tpl->EXP_STATUS = "";
						}if($linha['status'] == 0){
							$tpl->STATUS_UNIDADE = "disabled";
							$tpl->EXP_STATUS = "<small>Inativo</small>";
						}
						
						$tpl->block("BLOCO_UNIDADES");
					}	
					
					$query_procedimentos_select = $pdo->query("SELECT id, nome, status FROM cadastro_procedimentos WHERE id IN (SELECT id_procedimento FROM assoc_Procedimentos_planos_odonto WHERE token_plano = '$token_plano')")->fetchAll();
			
					foreach($query_procedimentos_select as $linha){
						$id_procedimento = $linha['id'];
						$tpl->ID_PROCEDIMENTO_SELECT = $linha['id'];
						$tpl->PROCEDIMENTO_SELECT = $linha['nome'];
						
						$tpl->PROCEDIMENTOS_UNIDADES = "selected";
						
						$tpl->block("BLOCO_PROCEDIMENTOS_SELECIONADOS");
					}
	
				$query_procedimentos = $pdo->query("SELECT id, nome, status FROM cadastro_procedimentos WHERE id NOT IN (SELECT id_procedimento FROM assoc_procedimentos_planos_odonto WHERE token_plano = '$token_plano')")->fetchAll();
					
					foreach($query_procedimentos as $linha){
						$tpl->ID_PROCEDIMENTO = $linha['id'];
						$tpl->PROCEDIMENTO = $linha['nome'];
		
						if($linha['status'] == 1){
							$tpl->STATUS_PROCEDIMENTO = "";
							$tpl->EXP_STATUS_PROCEDIMENTO = "";
						}if($linha['status'] == 0){
							$tpl->STATUS_PROCEDIMENTO = "disabled";
							$tpl->EXP__STATUS_PROCEDIMENTO = "<small>Inativo</small>";
						}
						
						$tpl->block("BLOCO_PROCEDIMENTOS");
					}	
			
				$tpl->block("BLOCO_PLANOS");
			}
			
	
	$query_hospital = $pdo->query("SELECT id, nome FROM cadastro_rede_credenciada ORDER BY id DESC")->fetchAll();
			
			foreach($query_hospital as $linha){
				$tpl->ID_HOSPITAL = $linha['id'];
				$tpl->HOSPITAL = $linha['nome'];
				
				
				$tpl->block("BLOCO_HOSPITAL");
			}	
			
	$query_carencia = $pdo->query("SELECT id, nome FROM cadastro_carencias ORDER BY id DESC")->fetchAll();
			
			foreach($query_carencia as $linha){
				$tpl->ID_CARENCIA = $linha['id'];
				$tpl->CARENCIA = $linha['nome'];
				
				
				$tpl->block("BLOCO_CARENCIAS");
				
			}
	*/
	
	$query_planos_select = $pdo->query("SELECT id, nome, token FROM cadastro_planos_odonto WHERE token IN (SELECT token_plano FROM assoc_planos_odonto_seguradoras WHERE token_seguradora = '$token')")->fetchAll();
			
					foreach($query_planos_select as $linha){
						$id_plano = $linha['id'];
						$tpl->TOKEN_PLANO_SEL = $linha['token'];
						$tpl->ID_PLANO_SEL = $linha['id'];
						$tpl->NOME_PLANO_SEL = $linha['nome'];
						
						$tpl->SELECIONADOS_PLANOS = "selected";
						
						$tpl->block("BLOCO_SELECT_PLANOS_SELECIONADOS");
					}
	
				$query_planos = $pdo->query("SELECT id, nome, token FROM cadastro_planos_odonto WHERE token NOT IN (SELECT token_plano FROM assoc_planos_odonto_seguradoras WHERE token_seguradora = '$token')")->fetchAll();
					
					foreach($query_planos as $linha){
						$tpl->TOKEN_PLAN = $linha['token'];
						$tpl->ID_PLAN = $linha['id'];
						$tpl->NAME_PLAN = $linha['nome'];
		
						$tpl->block("BLOCO_SELECT_PLANOS");
					}	
					
					
	$tpl->ROOT = ROOT;
		
    $tpl->DATA = date('Y');
	
	$tpl->NOME_USUARIO = $_SESSION['NameUser'];
	$tpl->MENU8 = "active";
    $tpl->show();

?>