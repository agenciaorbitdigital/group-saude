﻿<?php session_start();
header('Content-Type: text/html; charset=utf-8');

	require '../../modulos/connection-db.php';
	
	require '../lib/slug.php';
	
	$token = $_POST['token'];
	$nome = $_POST['nome'];
	$taxa = $_POST['taxa'];
	$estado = $_POST['estado'];
	$telefone = $_POST['telefone'];
	$telefone2 = $_POST['telefone2'];
	
	$data = date("Y-m-d H:i:s");
	
	$min_vidas = $_POST['min_vidas'];
	$max_vidas = $_POST['max_vidas'];
	
	if($min_vidas > $max_vidas){
		echo "<script>alert('O número mínimo de vidas não pode ser maior que o número máximo!');window.history.go(-1);</script>";
	}else {
		
		
	if(isset($_POST['status'])){
		$status = $_POST['status'];	
	}else{
		$status = "0";	
	}
	
	$slug = slug($nome);
	
	$select = $pdo->query("SELECT id FROM cadastro_seguradoras_odonto WHERE slug like '%".$slug."%' ORDER BY id DESC")->fetchAll();
		$count = count($select); 
		

	if($count > 0){
		$num = $count + 1;
		$slug = $slug. '-' .$num;
	}else{
		$slug = slug($nome);
	}
	
	if(isset($_POST['token_plano'])) {
		$nome_plano = $_POST['plano'];
		$token_plano = $_POST['token_plano'];
		$carencia = $_POST['carencia'];
		$fundo_reserva = $_POST['fundo_reserva'];
		$reembolso = $_POST['reembolso'];
		$coparticipacao = $_POST['coparticipacao'];
		$atuacao = $_POST['atuacao'];
		$informacoes = $_POST['mais_informacoes'];
		$preco = $_POST['preco'];

		$documentos = $_POST['documentacao'];
		$perfil = $_POST['perfil'];
	
		foreach( $token_plano as $key => $n ) {
			
			$add_plano = $pdo->query("INSERT INTO cadastro_planos_odonto (token, token_seguradora, nome, carencia, fundo_reserva, reembolso, atuacao, preco, informacoes_adicionais, documentos, coparticipacao, perfil,  data_cadastro, status) VALUES ('$token_plano[$key]', '$token', '$nome_plano[$key]', '$carencia[$key]', '$fundo_reserva[$key]', '$reembolso[$key]', '$atuacao[$key]', '$preco[$key]', '$informacoes[$key]', '$documentos[$key]', '$coparticipacao[$key]', '$perfil[$key]', '$data', '$status')");
			
			if(isset($_POST['unidades'])) {
			$unidades = $_POST['unidades'];
				foreach( $unidades as $key2 => $n ) {
			
			$add_unidade = $pdo->query("INSERT INTO assoc_planos_odonto_rede_credenciada (token_plano, id_rede_credenciada, data_cadastro) VALUES ('$token_plano[$key]', '$unidades[$key2]', '$data')");
				}
			}
	
			if(isset($_POST['procedimentos'])) {
			$procedimentos = $_POST['procedimentos'];
			foreach( $procedimentos as $key2 => $n ) {
				
			$add_procedimento = $pdo->query("INSERT INTO assoc_procedimentos_planos_odonto (token_plano, id_procedimento, data_cadastro) VALUES ('$token[$key]', '$procedimentos[$key2]', '$data')");
			}
		}
		}
		
	}
	
		if(isset($_POST['planos'])) {
			$planos = $_POST['planos'];
				foreach( $planos as $key3 => $n ) {
			
			$add_plano = $pdo->query("INSERT INTO assoc_planos_odonto_seguradoras (token_plano, token_seguradora, data_cadastro) VALUES ('$planos[$key3]', '$token', '$data')");
				}
			}
				
	$valid_extensions = array('jpeg', 'jpg', 'png', 'gif', 'bmp'); // valid extensions
	$path = '../imagens/'; // upload directory

	if(isset($_FILES['image']))
	{
		$img = $_FILES['image']['name'];
		$tmp = $_FILES['image']['tmp_name'];
			
		// get uploaded file's extension
		$ext = strtolower(pathinfo($img, PATHINFO_EXTENSION));
		
		// can upload same image using rand function
		$final_image = ''.rand(1000,1000000).''.uniqid().'.'.$ext.'';
		
		// check's valid format
		if(in_array($ext, $valid_extensions)) 
		{					
			$path = $path.strtolower($final_image);	
				
			if(move_uploaded_file($tmp,$path)) 
			{
				
			$add_plano = $pdo->query("INSERT INTO cadastro_seguradoras_odonto (nome, token, logo, telefone_clientes, telefone_vendedores, taxa_inscricao, estado, data_cadastro, status, slug, min_vidas, max_vidas) VALUES ('$nome', '$token', '$final_image', '$telefone', '$telefone2', '$taxa', '$estado', '$data', '$status', '$slug', '$min_vidas', '$max_vidas')");
	
				if($add_plano){	
					echo "<script>window.location.href = '../seguradoras-odonto'</script>";
				}
			}
		}
		else 
		{
		$final_image = "";
		$add_plano = $pdo->query("INSERT INTO cadastro_seguradoras_odonto (nome, token, logo, telefone_clientes, telefone_vendedores, taxa_inscricao, estado, data_cadastro, status, slug, min_vidas, max_vidas) VALUES ('$nome', '$token', '$final_image', '$telefone', '$telefone2', '$taxa', '$estado', '$data', '$status', '$slug', '$min_vidas', '$max_vidas')");
	
				if($add_plano){	
					echo "<script>window.location.href = '../seguradoras-odonto'</script>";
				}
		}
	}else{
		
	}

	}
?>