<?php session_start();

require '../../modulos/connection-db.php';

	$query_clientes = $pdo->query("SELECT id FROM cadastro_usuarios")->fetchAll();
	$todos_clientes = count($query_clientes);
	$dados['usuarios'] = $todos_clientes;
	
	$query_ativos = $pdo->query("SELECT id FROM cadastro_entidades")->fetchAll();
	$todos_ativos = count($query_ativos);
	$dados['entidades'] = $todos_ativos;
	
	$query_inativos = $pdo->query("SELECT id FROM cadastro_seguradoras")->fetchAll();
	$todos_inativos = count($query_inativos);
	$dados['seguradoras'] = $todos_inativos;
	
	$query_inativos = $pdo->query("SELECT id FROM cadastro_corretoras")->fetchAll();
	$todos_inativos = count($query_inativos);
	$dados['corretoras'] = $todos_inativos;
	
	echo json_encode($dados);

?>