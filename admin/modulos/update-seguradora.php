﻿<?php session_start();
header('Content-Type: text/html; charset=utf-8');

	require '../../modulos/connection-db.php';
	
	require '../lib/slug.php';
	
	$token = $_POST['token'];
	$nome = $_POST['nome'];
	$taxa = $_POST['taxa'];
	$estado = $_POST['estado'];
	$telefone = $_POST['telefone'];
	$telefone2 = $_POST['telefone2'];
	
	$data = date("Y-m-d H:i:s");
	
	$min_vidas = $_POST['min_vidas'];
	$max_vidas = $_POST['max_vidas'];
	$quantidade_titulares = $_POST['quantidade_titulares'];
	
	if($min_vidas > $max_vidas){
		echo "<script>alert('O número mínimo de vidas não pode ser maior que o número máximo!');window.history.go(-1);</script>";
	}else {
		
	if(isset($_POST['status'])){
		$status = $_POST['status'];	
	}else{
		$status = "0";	
	}
	
	$slug = slug($nome);
	
	if(isset($_POST['token_plano'])) {
		$nome_plano = $_POST['plano'];
		$token_plano = $_POST['token_plano'];
		$carencia = $_POST['carencia'];
		$acomodacao = $_POST['acomodacao'];
		$reembolso = $_POST['reembolso'];
		$coparticipacao = $_POST['coparticipacao'];
		$atuacao = $_POST['atuacao'];
		$informacoes = $_POST['mais_informacoes'];
		
		$idade1 = $_POST['idade1'];
		$idade2 = $_POST['idade2'];
		$idade3 = $_POST['idade3'];
		$idade4 = $_POST['idade4'];
		$idade5 = $_POST['idade5'];
		$idade6 = $_POST['idade6'];
		$idade7 = $_POST['idade7'];
		$idade8 = $_POST['idade8'];
		$idade9 = $_POST['idade9'];
		$idade10 = $_POST['idade10'];

		$documentos = $_POST['documentacao'];
		$perfil = $_POST['perfil'];
	
		foreach( $token_plano as $key => $n ) {
			
			$add_plano = $pdo->query("INSERT INTO cadastro_planos (token, token_seguradora, nome, carencia, acomodacao, reembolso, atuacao, 0_18, 19_23, 24_28, 29_33, 34_38, 39_43, 44_48, 49_53, 54_58, acima_59, informacoes_adicionais, documentos, coparticipacao, perfil,  data_cadastro, status) VALUES ('$token_plano[$key]', '$token', '$nome_plano[$key]', '$carencia[$key]', '$acomodacao[$key]', '$reembolso[$key]', '$atuacao[$key]', '$idade1[$key]', '$idade2[$key]', '$idade3[$key]', '$idade4[$key]', '$idade5[$key]', '$idade6[$key]', '$idade7[$key]', '$idade8[$key]', '$idade9[$key]', '$idade10[$key]', '$informacoes[$key]', '$documentos[$key]', '$coparticipacao[$key]', '$perfil[$key]', '$data', '$status')");
			
			if(isset($_POST['unidades'])) {
			$unidades = $_POST['unidades'];
				foreach( $unidades as $key2 => $n ) {
			
			$add_unidade = $pdo->query("INSERT INTO assoc_planos_rede_credenciada (token_plano, id_rede_credenciada, data_cadastro) VALUES ('$token_plano[$key]', '$unidades[$key2]', '$data')");
				}
			}

			
			if(isset($_POST['idadeEx'])) {
			$idadeEx = $_POST['idadeEx'];
			$precoEx = $_POST['precoEx'];
				foreach( $idadeEx as $key3 => $n ) {
			
			$add_excessao = $pdo->query("INSERT INTO cadastro_excessao_idade (token_plano, idade, valor, data_cadastro) VALUES ('$token_plano[$key]', '$idadeEx[$key3]', '$precoEx[$key3]', '$data')");
				}
			}
		}
		}

	if(isset($_POST['exist_token_plano'])) {
		$nome_plano = $_POST['exist_plano'];
		$token_plano = $_POST['exist_token_plano'];
		$carencia = $_POST['exist_carencia'];
		$acomodacao = $_POST['exist_acomodacao'];
		$reembolso = $_POST['exist_reembolso'];
		$coparticipacao = $_POST['exist_coparticipacao'];
		$atuacao = $_POST['exist_atuacao'];
		$informacoes = $_POST['exist_mais_informacoes'];
		
		$idade1 = $_POST['exist_idade1'];
		$idade2 = $_POST['exist_idade2'];
		$idade3 = $_POST['exist_idade3'];
		$idade4 = $_POST['exist_idade4'];
		$idade5 = $_POST['exist_idade5'];
		$idade6 = $_POST['exist_idade6'];
		$idade7 = $_POST['exist_idade7'];
		$idade8 = $_POST['exist_idade8'];
		$idade9 = $_POST['exist_idade9'];
		$idade10 = $_POST['exist_idade10'];

		$documentos = $_POST['exist_documentacao'];
		$perfil = $_POST['exist_perfil'];
	
		foreach( $token_plano as $key => $n ) {
			
			$add_plano = $pdo->query("UPDATE cadastro_planos SET nome='$nome_plano[$key]', carencia='$carencia[$key]', acomodacao='$acomodacao[$key]', reembolso='$reembolso[$key]', atuacao='$atuacao[$key]', 0_18='$idade1[$key]', 19_23='$idade2[$key]', 24_28='$idade3[$key]', 29_33='$idade4[$key]', 34_38='$idade5[$key]', 39_43='$idade6[$key]', 44_48='$idade7[$key]', 49_53='$idade8[$key]', 54_58='$idade9[$key]', acima_59='$idade10[$key]', informacoes_adicionais='$informacoes[$key]', documentos='$documentos[$key]', coparticipacao='$coparticipacao[$key]', perfil='$perfil[$key]', status='$status' WHERE token='$token_plano[$key]'");

			if(isset($_POST['exist_idadeEx'])) {
					
				$idadeEx = $_POST['exist_idadeEx'];
				$precoEx = $_POST['exist_precoEx'];
				$idIdadeEX = $_POST['exist_idadeEx_id'];
				
				foreach( $idadeEx as $key3 => $n ) {
			
			$add_excessao = $pdo->query("UPDATE cadastro_excessao_idade SET idade='$idadeEx[$key3]', valor='$precoEx[$key3]' WHERE id='$idIdadeEX[$key3]'");
				}
			}
		}
		}
		
	if(isset($_POST['planos'])) {
		$planos = $_POST['planos'];
		$delete = $pdo->query("DELETE FROM assoc_planos_saude_seguradoras WHERE token_seguradora = '$token'");
		foreach( $planos as $key4 => $n ) {
			
			$add_assoc_planos = $pdo->query("INSERT INTO assoc_planos_saude_seguradoras (token_plano, token_seguradora, data_cadastro) VALUES ('$planos[$key4]', '$token', '$data')");
		}
	}else{
		$delete = $pdo->query("DELETE FROM assoc_planos_saude_seguradoras WHERE token_seguradora = '$token'");
	}	
				
	$valid_extensions = array('jpeg', 'jpg', 'png', 'gif', 'bmp'); // valid extensions
	$path = '../imagens/'; // upload directory

	if(isset($_FILES['image']))
	{
		$img = $_FILES['image']['name'];
		$tmp = $_FILES['image']['tmp_name'];
			
		// get uploaded file's extension
		$ext = strtolower(pathinfo($img, PATHINFO_EXTENSION));
		
		// can upload same image using rand function
		$final_image = ''.rand(1000,1000000).''.uniqid().'.'.$ext.'';
		
		// check's valid format
		if(in_array($ext, $valid_extensions)) 
		{					
			$path = $path.strtolower($final_image);	
				
			if(move_uploaded_file($tmp,$path)) 
			{
				
			$update_plano = $pdo->query("UPDATE cadastro_seguradoras SET nome='$nome', logo='$final_image', telefone_clientes='$telefone', telefone_vendedores='$telefone2', taxa_inscricao='$taxa', estado='$estado', status='$status', min_vidas='$min_vidas', max_vidas='$max_vidas', titulares='$quantidade_titulares' WHERE token='$token'");
	
				if($update_plano){	
					echo "<script>window.history.go(-1)</script>";	
				}
			}
		}
		else 
		{
		$update_plano = $pdo->query("UPDATE cadastro_seguradoras SET nome='$nome', telefone_clientes='$telefone', telefone_vendedores='$telefone2', taxa_inscricao='$taxa', estado='$estado', status='$status', min_vidas='$min_vidas', max_vidas='$max_vidas', titulares='$quantidade_titulares' WHERE token='$token'");
	
				if($update_plano){	
					echo "<script>window.history.go(-1)</script>";	
				}
		}
	}else{
		
	}

	}
?>