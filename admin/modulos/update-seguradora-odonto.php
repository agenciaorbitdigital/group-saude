﻿<?php session_start();
header('Content-Type: text/html; charset=utf-8');

	require '../../modulos/connection-db.php';
	
	require '../lib/slug.php';
	
	$token = $_POST['token'];
	$nome = $_POST['nome'];
	$taxa = $_POST['taxa'];
	$estado = $_POST['estado'];
	$telefone = $_POST['telefone'];
	$telefone2 = $_POST['telefone2'];
	
	$data = date("Y-m-d H:i:s");
	
	$min_vidas = $_POST['min_vidas'];
	$max_vidas = $_POST['max_vidas'];
	
	if($min_vidas > $max_vidas){
		echo "<script>alert('O número mínimo de vidas não pode ser maior que o número máximo!');window.history.go(-1);</script>";
	}else {
		
	if(isset($_POST['status'])){
		$status = $_POST['status'];	
	}else{
		$status = "0";	
	}
	
	$slug = slug($nome);
	
	if(isset($_POST['token_plano'])) {
		$nome_plano = $_POST['plano'];
		$token_plano = $_POST['token_plano'];
		$carencia = $_POST['carencia'];
		$fundo_reserva = $_POST['fundo_reserva'];
		$reembolso = $_POST['reembolso'];
		$coparticipacao = $_POST['coparticipacao'];
		$atuacao = $_POST['atuacao'];
		$informacoes = $_POST['mais_informacoes'];
		$preco = $_POST['preco'];

		$documentos = $_POST['documentacao'];
		$perfil = $_POST['perfil'];
	
		foreach( $token_plano as $key => $n ) {
			
			$add_plano = $pdo->query("INSERT INTO cadastro_planos_odonto (token, token_seguradora, nome, carencia, fundo_reserva, reembolso, atuacao, preco, informacoes_adicionais, documentos, coparticipacao, perfil,  data_cadastro, status) VALUES ('$token_plano[$key]', '$token', '$nome_plano[$key]', '$carencia[$key]', '$fundo_reserva[$key]', '$reembolso[$key]', '$atuacao[$key]', '$preco[$key]', '$informacoes[$key]', '$documentos[$key]', '$coparticipacao[$key]', '$perfil[$key]', '$data', '$status')");
			
			if(isset($_POST['unidades'])) {
			$unidades = $_POST['unidades'];
			foreach( $unidades as $key2 => $n ) {
			
			$add_unidade = $pdo->query("INSERT INTO assoc_planos_odonto_rede_credenciada (token_plano, id_rede_credenciada, data_cadastro) VALUES ('$token[$key]', '$unidades[$key2]', '$data')");
		}
	}
	
			if(isset($_POST['procedimentos'])) {
			$procedimentos = $_POST['procedimentos'];
			foreach( $procedimentos as $key2 => $n ) {
				
			$add_procedimento = $pdo->query("INSERT INTO assoc_procedimentos_planos_odonto (token_plano, id_procedimento, data_cadastro) VALUES ('$token[$key]', '$procedimentos[$key2]', '$data')");
			}
		}
		}
		
	}
	
	if(isset($_POST['exist_token_plano'])) {
		$nome_plano = $_POST['exist_plano'];
		$token_plano = $_POST['exist_token_plano'];
		$carencia = $_POST['exist_carencia'];
		$fundo_reserva = $_POST['exist_fundo_reserva'];
		$reembolso = $_POST['exist_reembolso'];
		$coparticipacao = $_POST['exist_coparticipacao'];
		$atuacao = $_POST['exist_atuacao'];
		$informacoes = $_POST['exist_mais_informacoes'];
		
		$preco = $_POST['exist_preco'];

		$documentos = $_POST['exist_documentacao'];
		$perfil = $_POST['exist_perfil'];
	
		foreach( $token_plano as $key => $n ) {
			
			$add_plano = $pdo->query("UPDATE cadastro_planos_odonto SET nome='$nome_plano[$key]', carencia='$carencia[$key]', fundo_reserva='$fundo_reserva[$key]', reembolso='$reembolso[$key]', atuacao='$atuacao[$key]', preco='$preco[$key]', informacoes_adicionais='$informacoes[$key]', documentos='$documentos[$key]', coparticipacao='$coparticipacao[$key]', perfil='$perfil[$key]', status='$status' WHERE token='$token_plano[$key]'");

		}
		}
		
	if(isset($_POST['planos'])) {
		$planos = $_POST['planos'];
		$delete = $pdo->query("DELETE FROM assoc_planos_odonto_seguradoras WHERE token_seguradora = '$token'");
		foreach( $planos as $key4 => $n ) {
			
			$add_assoc_planos = $pdo->query("INSERT INTO assoc_planos_odonto_seguradoras (token_plano, token_seguradora, data_cadastro) VALUES ('$planos[$key4]', '$token', '$data')");
		}
	}else{
		$delete = $pdo->query("DELETE FROM assoc_planos_odonto_seguradoras WHERE token_seguradora = '$token'");
	}	
		
	$valid_extensions = array('jpeg', 'jpg', 'png', 'gif', 'bmp'); // valid extensions
	$path = '../imagens/'; // upload directory

	if(isset($_FILES['image']))
	{
		$img = $_FILES['image']['name'];
		$tmp = $_FILES['image']['tmp_name'];
			
		// get uploaded file's extension
		$ext = strtolower(pathinfo($img, PATHINFO_EXTENSION));
		
		// can upload same image using rand function
		$final_image = ''.rand(1000,1000000).''.uniqid().'.'.$ext.'';
		
		// check's valid format
		if(in_array($ext, $valid_extensions)) 
		{					
			$path = $path.strtolower($final_image);	
				
			if(move_uploaded_file($tmp,$path)) 
			{
				
			$update_plano = $pdo->query("UPDATE cadastro_seguradoras_odonto SET nome='$nome', logo='$final_image', telefone_clientes='$telefone', telefone_vendedores='$telefone2', taxa_inscricao='$taxa', estado='$estado', status='$status', min_vidas='$min_vidas', max_vidas='$max_vidas' WHERE token='$token'");
	
				if($update_plano){	
					echo "<script>window.history.go(-1)</script>";	
				}
			}
		}
		else 
		{
		$update_plano = $pdo->query("UPDATE cadastro_seguradoras_odonto SET nome='$nome', telefone_clientes='$telefone', telefone_vendedores='$telefone2', taxa_inscricao='$taxa', estado='$estado', status='$status', min_vidas='$min_vidas', max_vidas='$max_vidas' WHERE token='$token'");
	
				if($update_plano){	
					echo "<script>window.history.go(-1)</script>";	
				}
		}
	}else{
		
	}

	}
?>