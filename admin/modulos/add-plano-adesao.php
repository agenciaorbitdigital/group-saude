﻿<?php session_start();
header('Content-Type: text/html; charset=utf-8');

	require '../../modulos/connection-db.php';
	
	require '../lib/slug.php';
	
	$nome = $_POST['plano'];

	$estado = $_POST['estado'];
	$telefone = $_POST['telefone'];
	$telefone2 = $_POST['telefone2'];
	$acomodacao = $_POST['acomodacao'];
	$reembolso = $_POST['reembolso'];
	$coparticipacao = $_POST['coparticipacao'];
	$atuacao = $_POST['atuacao'];
	$informacoes = $_POST['informacoes'];
	$carencia = $_POST['carencia'];
	$token = $_POST['token'];
	$idade1 = $_POST['idade1'];
	$idade2 = $_POST['idade2'];
	$idade3 = $_POST['idade3'];
	$idade4 = $_POST['idade4'];
	$idade5 = $_POST['idade5'];
	$idade6 = $_POST['idade6'];
	$idade7 = $_POST['idade7'];
	$idade8 = $_POST['idade8'];
	$idade9 = $_POST['idade9'];
	$idade10 = $_POST['idade10'];

	
	$data = date("Y-m-d H:i:s");
	
	
	if($min_vidas > $max_vidas){
		echo "<script>alert('O número mínimo de vidas não pode ser maior que o número máximo!');window.history.go(-1);</script>";
	}else {
		
		
	if(isset($_POST['status'])){
		$status = $_POST['status'];	
	}else{
		$status = "0";	
	}
	
	$slug = slug($nome);
	
	$select = $pdo->query("SELECT id FROM cadastro_planos_adesao WHERE slug like '%".$slug."%' ORDER BY id DESC")->fetchAll();
		$count = count($select); 
		

	if($count > 0){
		$num = $count + 1;
		$slug = $slug. '-' .$num;
	}else{
		$slug = slug($nome);
	}
	
	if(isset($_POST['idadeEx'])) {
		$idadeEx = $_POST['idadeEx'];
		$precoEx = $_POST['precoEx'];
		foreach( $idadeEx as $key => $n ) {
			
			$add_excessao = $pdo->query("INSERT INTO cadastro_excessao_idade (token_plano, idade, valor, data_cadastro) VALUES ('$token', '$idadeEx[$key]', '$precoEx[$key]', '$data')");
		}
	}
	
	if(isset($_POST['operadoras'])) {
		$operadoras = $_POST['operadoras'];
		foreach( $operadoras as $key => $n ) {
			
			$add_operadora = $pdo->query("INSERT INTO assoc_planos_adesao_operadoras (token_plano, id_operadora, data_cadastro) VALUES ('$token', '$operadoras[$key]', '$data')");
		}
	}
	
	if(isset($_POST['entidades'])) {
		$entidades = $_POST['entidades'];
		foreach( $entidades as $key => $n ) {
			
			$add_entidade = $pdo->query("INSERT INTO assoc_planos_adesao_entidades (token_plano, id_entidade, data_cadastro) VALUES ('$token', '$entidades[$key]', '$data')");
		}
	}
	if(isset($_POST['unidades'])) {
		$unidades = $_POST['unidades'];
		foreach( $unidades as $key => $n ) {
			
			$add_unidade = $pdo->query("INSERT INTO assoc_planos_adesao_rede_credenciada (token_plano, id_rede_credenciada, data_cadastro) VALUES ('$token', '$unidades[$key]', '$data')");
		}
	}
	
	if(isset($_POST['administradoras'])) {
		$administradoras = $_POST['administradoras'];
		foreach( $administradoras as $key => $n ) {
			
			$add_administradora = $pdo->query("INSERT INTO assoc_planos_adesao_administradoras (token_plano, id_administradora, data_cadastro) VALUES ('$token', '$administradoras[$key]', '$data')");
		}
	}
	
	
	
	$valid_extensions = array('jpeg', 'jpg', 'png', 'gif', 'bmp'); // valid extensions
	$path = '../imagens/'; // upload directory

	if(isset($_FILES['image']))
	{
		$img = $_FILES['image']['name'];
		$tmp = $_FILES['image']['tmp_name'];
			
		// get uploaded file's extension
		$ext = strtolower(pathinfo($img, PATHINFO_EXTENSION));
		
		// can upload same image using rand function
		$final_image = ''.rand(1000,1000000).''.uniqid().'.'.$ext.'';
		
		// check's valid format
		if(in_array($ext, $valid_extensions)) 
		{					
			$path = $path.strtolower($final_image);	
				
			if(move_uploaded_file($tmp,$path)) 
			{
				
			$add_plano = $pdo->query("INSERT INTO cadastro_planos_adesao (nome, token, logo, telefone_clientes, telefone_vendedores, carencia, id_informacoes, acomodacao, reembolso, atuacao, coparticipacao, estado, 0_18, 19_23, 24_28, 29_33, 34_38, 39_43, 44_48, 49_53, 54_58, acima_59, data_cadastro, status, slug) VALUES ('$nome', '$token', '$final_image', '$telefone', '$telefone2', '$carencia', '$informacoes', '$acomodacao', '$reembolso', '$atuacao', '$coparticipacao', '$estado', '$idade1', '$idade2', '$idade3', '$idade4', '$idade5', '$idade6', '$idade7', '$idade8', '$idade9', '$idade10', '$data', '$status', '$slug')");
	
				if($add_plano){	
					echo "<script>window.location.href = '../planos-adesao'</script>";	
				}
			}
		}
		else 
		{
		$final_image = "";
		$add_plano = $pdo->query("INSERT INTO cadastro_planos_adesao (nome, token, logo, telefone_clientes, telefone_vendedores, carencia, id_informacoes, acomodacao, reembolso, atuacao, coparticipacao, estado, 0_18, 19_23, 24_28, 29_33, 34_38, 39_43, 44_48, 49_53, 54_58, acima_59, data_cadastro, status, slug) VALUES ('$nome', '$token', '$final_image', '$telefone', '$telefone2', '$carencia', '$informacoes', '$acomodacao', '$reembolso', '$atuacao', '$coparticipacao', '$estado', '$idade1', '$idade2', '$idade3', '$idade4', '$idade5', '$idade6', '$idade7', '$idade8', '$idade9', '$idade10', '$data', '$status', '$slug')");
	
				if($add_plano){	
					echo "<script>window.location.href = '../planos-adesao'</script>";	
				}
		}
	}else{
		
	}

	}
?>