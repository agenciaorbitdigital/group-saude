<?php

	require 'modulos/session-login.php';

	require '../modulos/connection-db.php';

    require_once("lib/raelgc/view/Template.php");
    use raelgc\view\Template;

    $tpl = new Template("templates/usuarios.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("TOPBAR", "templates/topbar.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("SIDEBAR", "templates/sidebar.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("SCRIPTS", "templates/scripts.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("ESTILOS", "templates/estilos.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("FOOTER", "templates/footer.html");
	
	//Dados Usuário
	$email_login = $_SESSION['email_login'];
	$userName = $_SESSION['UserName'];
	

	$query_usuario = $pdo->query("SELECT id, nome, email, status FROM cadastro_usuarios ORDER BY id DESC")->fetchAll();
			
			foreach($query_usuario as $linha){
				$tpl->ID_USUARIO = $linha['id'];
				$tpl->NOME = $linha['nome'];
				$tpl->EMAIL = $linha['email'];
				if($linha['status'] == 1){
					$tpl->STATUS = "Ativo";
				}if($linha['status'] == 0){
					$tpl->STATUS = "Inativo";
				}
				
				$tpl->block("BLOCO_CLIENTES");
			}

		
    $tpl->DATA = date('Y');
	$tpl->NOME_USUARIO = $_SESSION['NameUser'];
	$tpl->ROOT = ROOT;
	$tpl->MENU6 = "active";
	$tpl->MENU6_1 = "active";
    $tpl->show();

?>