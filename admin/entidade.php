<?php

	require 'modulos/session-login.php';

	require '../modulos/connection-db.php';

    require_once("lib/raelgc/view/Template.php");
    use raelgc\view\Template;

    $tpl = new Template("templates/entidade.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("TOPBAR", "templates/topbar.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("SIDEBAR", "templates/sidebar.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("SCRIPTS", "templates/scripts.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("ESTILOS", "templates/estilos.html");
	
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("FOOTER", "templates/footer.html");
	
	
	//Dados Usuário
	$email_login = $_SESSION['email_login'];
	
	//Dados Usuário
	$email_login = $_SESSION['email_login'];
	
	$slug = $_GET['slug'];

	

	$query_seguradoras = $pdo->query("SELECT id, token, nome, status, slug, logo, data_cadastro FROM cadastro_entidades WHERE slug = '$slug'")->fetchAll();
			
			foreach($query_seguradoras as $linha){
				$tpl->ID = $linha['id'];
				$tpl->TOKEN = $linha['token'];
				$token_entidade = $linha['token'];
				$tpl->NOME = $linha['nome'];
				$tpl->IMAGEM_DESTACADA = $linha['logo'];
				$tpl->DATA_CADASTRO = $linha['data_cadastro'];
				if($linha['status'] == 1){
				$tpl->STATUS_EXPL = "Ativo";
				$tpl->STATUS_CHECK = "checked";
			}else{
				$tpl->STATUS_EXPL = "Inativo";	
				$tpl->STATUS_CHECK = "";
			}
			}
	$query_empregos_select = $pdo->query("SELECT id, nome, status FROM cadastro_empregos WHERE id IN (SELECT id_emprego FROM assoc_entidades_empregos WHERE token_entidade = '$token_entidade')")->fetchAll();
			
					foreach($query_empregos_select as $linha){
						$id_emprego = $linha['id'];
						$tpl->ID_EMPREGO_SELECT = $linha['id'];
						$tpl->EMPREGO_SELECT = $linha['nome'];
						
						$tpl->SELECIONADOS_EMPREGOS = "selected";
						
						$tpl->block("BLOCO_EMPREGOS_SELECIONADOS");
					}
	
				$query_empregos = $pdo->query("SELECT id, nome, status FROM cadastro_empregos WHERE id NOT IN (SELECT id_emprego FROM assoc_entidades_empregos WHERE token_entidade = '$token_entidade')")->fetchAll();
					
					foreach($query_empregos as $linha){
						$tpl->ID_EMPREGO = $linha['id'];
						$tpl->EMPREGO = $linha['nome'];
		
						if($linha['status'] == 1){
							$tpl->STATUS_EMPREGO = "";
							$tpl->EXP_STATUS_EMPREGO = "";
						}if($linha['status'] == 0){
							$tpl->STATUS_EMPREGO = "disabled";
							$tpl->EXP_STATUS_EMPREGO = "<small>Inativo</small>";
						}
						
						$tpl->block("BLOCO_EMPREGOS");
					}	

	$tpl->ROOT = ROOT;
		
    $tpl->DATA = date('Y');

	$tpl->NOME_USUARIO = $_SESSION['NameUser'];
	$tpl->MENU4 = "active";
    $tpl->show();

?>