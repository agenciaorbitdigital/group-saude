<?php

	require 'modulos/session-login.php';

	require '../modulos/connection-db.php';

    require_once("lib/raelgc/view/Template.php");
    use raelgc\view\Template;

    $tpl = new Template("templates/add-plano-odonto.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("TOPBAR", "templates/topbar.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("SIDEBAR", "templates/sidebar.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("SCRIPTS", "templates/scripts.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("ESTILOS", "templates/estilos.html");
	
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("FOOTER", "templates/footer.html");
	
	
	//Dados Usuário
	$email_login = $_SESSION['email_login'];
	
	$query_usuario = $pdo->query("SELECT id, nome, data_criacao FROM cadastro_usuarios WHERE email = '$email_login'")->fetchAll();
 		
		foreach($query_usuario as $linha){
			$id_usuario = $linha['id'];
			$nome = $linha['nome'];
			$data_cadastro = $linha['data_criacao'];
			
			$_SESSION['UserName'] = $linha['nome'];
		}
	$query_hospital = $pdo->query("SELECT id, nome FROM cadastro_rede_credenciada ORDER BY id DESC")->fetchAll();
			
			foreach($query_hospital as $linha){
				$tpl->ID_HOSPITAL = $linha['id'];
				$tpl->HOSPITAL = $linha['nome'];
				
				
				$tpl->block("BLOCO_HOSPITAL");
			}	
			
	$query_carencia = $pdo->query("SELECT id, nome FROM cadastro_carencias ORDER BY id DESC")->fetchAll();
			
			foreach($query_carencia as $linha){
				$tpl->ID_CARENCIA = $linha['id'];
				$tpl->CARENCIA = $linha['nome'];
				
				
				$tpl->block("BLOCO_CARENCIAS");
			}
	$query_reembolso = $pdo->query("SELECT id, nome FROM cadastro_reembolsos WHERE status = '1' ORDER BY id DESC")->fetchAll();
			
			foreach($query_reembolso as $linha){
				$tpl->ID_REEMBOLSO = $linha['id'];
				$tpl->REEMBOLSO = $linha['nome'];
				
				
				$tpl->block("BLOCO_REEMBOLSOS");
			}	
					
	$query_hospital = $pdo->query("SELECT id, nome FROM cadastro_rede_credenciada ORDER BY id DESC")->fetchAll();
			
			foreach($query_hospital as $linha){
				$tpl->ID_HOSPITAL = $linha['id'];
				$tpl->HOSPITAL = $linha['nome'];
				
				
				$tpl->block("BLOCO_HOSPITAL");
			}	
			
	$query_procedimento = $pdo->query("SELECT id, nome FROM cadastro_procedimentos ORDER BY id DESC")->fetchAll();
			
			foreach($query_procedimento as $linha){
				$tpl->ID_PROCEDIMENTO = $linha['id'];
				$tpl->PROCEDIMENTO = $linha['nome'];
				
				
				$tpl->block("BLOCO_PROCEDIMENTOS");
			}	
			
	$query_informacoes = $pdo->query("SELECT id, nome FROM cadastro_informacoes_adicionais ORDER BY id DESC")->fetchAll();
			
			foreach($query_informacoes as $linha){
				$tpl->ID_INFORMACOES = $linha['id'];
				$tpl->INFORMACOES = $linha['nome'];
				
				
				$tpl->block("BLOCO_INFORMACOES");
			}
				
	$tpl->TOKEN = md5(uniqid(rand(), true));
	
	$tpl->ROOT = ROOT;
		
    $tpl->DATA = date('Y');
	
	$tpl->NOME_USUARIO = $_SESSION['NameUser'];
	$tpl->MENU8 = "active";
	$tpl->MENU8_5 = "active";
    $tpl->show();

?>