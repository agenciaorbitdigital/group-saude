<?php

	require 'modulos/session-login.php';

	require '../modulos/connection-db.php';

    require_once("lib/raelgc/view/Template.php");
    use raelgc\view\Template;

    $tpl = new Template("templates/add-plano-saude.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("TOPBAR", "templates/topbar.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("SIDEBAR", "templates/sidebar.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("SCRIPTS", "templates/scripts.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("ESTILOS", "templates/estilos.html");
	
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("FOOTER", "templates/footer.html");
	
	
	//Dados Usuário
	$email_login = $_SESSION['email_login'];
	
	$query_usuario = $pdo->query("SELECT id, nome, data_criacao FROM cadastro_usuarios WHERE email = '$email_login'")->fetchAll();
 		
		foreach($query_usuario as $linha){
			$id_usuario = $linha['id'];
			$nome = $linha['nome'];
			$data_cadastro = $linha['data_criacao'];
			
			$_SESSION['UserName'] = $linha['nome'];
		}
	$query_hospital = $pdo->query("SELECT id, nome, ps_pronto_socorro, psi_pronto_socorro_infantil, m_maternidade, h_hospital, pa_pronto_atendimento, a_ambulatorio FROM cadastro_rede_credenciada ORDER BY id DESC")->fetchAll();
			
			foreach($query_hospital as $linha){
				$tpl->ID_HOSPITAL = $linha['id'];
				$tpl->HOSPITAL = $linha['nome'];
				$unidade = $linha['nome'];
				if($linha['a_ambulatorio'] == 1){
					$ambulatorio = "A / ";
					}else{
						$ambulatorio = "";
					}
					if($linha['h_hospital'] == 1){
					$hospital = "H / ";
					}else{
						$hospital = "";
					}
					if($linha['m_maternidade'] == 1){
					$maternidade = "M / ";
					}else{
						$maternidade = "";
					}
					if($linha['pa_pronto_atendimento'] == 1){
					$p_atendimento = "PA / ";
					}else{
						$p_atendimento = "";
					}
					if($linha['ps_pronto_socorro'] == 1){
					$p_socorro = "PS ";
					}else{
						$p_socorro = "";
					}
					
					if($linha['a_ambulatorio'] OR $linha['h_hospital'] OR $linha['m_maternidade'] OR $linha['pa_pronto_atendimento'] OR $linha['ps_pronto_socorro'] == 1){
					$tpl->HOSPITAL = $unidade ." - " .$ambulatorio . $hospital . $maternidade .$p_atendimento . $p_socorro;
					}else{
						$tpl->HOSPITAL = $unidade;	
					}
				
				$tpl->block("BLOCO_HOSPITAL");
				
			}	
			
	$query_carencia = $pdo->query("SELECT id, nome FROM cadastro_carencias WHERE status = '1' ORDER BY id DESC")->fetchAll();
			
			foreach($query_carencia as $linha){
				$tpl->ID_CARENCIA = $linha['id'];
				$tpl->CARENCIA = $linha['nome'];
				
				
				$tpl->block("BLOCO_CARENCIAS");
			}

	$query_reembolso = $pdo->query("SELECT id, nome FROM cadastro_reembolsos WHERE status = '1' ORDER BY id DESC")->fetchAll();
			
			foreach($query_reembolso as $linha){
				$tpl->ID_REEMBOLSO = $linha['id'];
				$tpl->REEMBOLSO = $linha['nome'];
				
				
				$tpl->block("BLOCO_REEMBOLSOS");
			}	
			
		$query_informacoes = $pdo->query("SELECT id, nome FROM cadastro_informacoes_adicionais ORDER BY id DESC")->fetchAll();
			
			foreach($query_informacoes as $linha){
				$tpl->ID_INFORMACOES = $linha['id'];
				$tpl->INFORMACOES = $linha['nome'];
				
				
				$tpl->block("BLOCO_INFORMACOES");
			}	
		
	$tpl->TOKEN = md5(uniqid(rand(), true));
	
	$tpl->ROOT = ROOT;
		
    $tpl->DATA = date('Y');
	
	$tpl->NOME_USUARIO = $_SESSION['NameUser'];
	$tpl->MENU2 = "active";
	$tpl->MENU2_4 = "active";
    $tpl->show();

?>