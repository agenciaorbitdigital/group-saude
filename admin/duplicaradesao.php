<?php

	require 'modulos/session-login.php';

	require '../modulos/connection-db.php';

    require_once("lib/raelgc/view/Template.php");
    use raelgc\view\Template;

    $tpl = new Template("templates/duplicar-adesao.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("TOPBAR", "templates/topbar.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("SIDEBAR", "templates/sidebar.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("SCRIPTS", "templates/scripts.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("ESTILOS", "templates/estilos.html");
	
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("FOOTER", "templates/footer.html");
	
	
	//Dados Usuário
	$email_login = $_SESSION['email_login'];
	
	//Dados Usuário
	$email_login = $_SESSION['email_login'];
	
	$slug = $_GET['slug'];
	
	$estadosBrasileiros = array(
	'AC'=>'Acre',
	'AL'=>'Alagoas',
	'AP'=>'Amapá',
	'AM'=>'Amazonas',
	'BA'=>'Bahia',
	'CE'=>'Ceará',
	'DF'=>'Distrito Federal',
	'ES'=>'Espírito Santo',
	'GO'=>'Goiás',
	'MA'=>'Maranhão',
	'MT'=>'Mato Grosso',
	'MS'=>'Mato Grosso do Sul',
	'MG'=>'Minas Gerais',
	'PA'=>'Pará',
	'PB'=>'Paraíba',
	'PR'=>'Paraná',
	'PE'=>'Pernambuco',
	'PI'=>'Piauí',
	'RJ'=>'Rio de Janeiro',
	'RN'=>'Rio Grande do Norte',
	'RS'=>'Rio Grande do Sul',
	'RO'=>'Rondônia',
	'RR'=>'Roraima',
	'SC'=>'Santa Catarina',
	'SP'=>'São Paulo',
	'SE'=>'Sergipe',
	'TO'=>'Tocantins'
	);
	
	$Acomodacao = array(
	'1'=>'Quarto coletivo ou Enfermaria',
	'2'=>'Quarto privativo ou Apartamento'
	);
	
	$Atuacao = array(
	'Regional'=>'Regional',
	'Nacional'=>'Nacional'
	);
	
	$Reembolso = array(
	'Sim'=>'Sim',
	'Não'=>'Não'
	);
	
	$Coparticipacao = array(
	'Sim'=>'Sim',
	'Não'=>'Não'
	);

	$Perfil = array(
	'1'=>'Familiar',
	'2'=>'Individual',
	'3'=>'PME'
	);
	
	$query_planos = $pdo->query("SELECT * FROM cadastro_planos_adesao WHERE slug = '$slug'")->fetchAll();
			
			foreach($query_planos as $ln){
				$token_plano = $ln['token'];
				$atuacao_atual = $ln['atuacao'];
				$estado_atual = $ln['estado'];
				$perfil_atual = $ln['perfil'];
				$acomodacao_atual = $ln['acomodacao'];
				$reembolso_atual = $ln['reembolso'];
				$coparticipacao_atual = $ln['coparticipacao'];
				$id_carencia = $ln['carencia'];
				$id_informacoes = $ln['id_informacoes'];
				$tpl->ID = $ln['id'];
				$tpl->NOME = $ln['nome'];
				$tpl->PRECO1 = $ln['0_18'];
				$tpl->PRECO2 = $ln['19_23'];
				$tpl->PRECO3 = $ln['24_28'];
				$tpl->PRECO4 = $ln['29_33'];
				$tpl->PRECO5 = $ln['34_38'];
				$tpl->PRECO6 = $ln['39_43'];
				$tpl->PRECO7 = $ln['44_48'];
				$tpl->PRECO8 = $ln['49_53'];
				$tpl->PRECO9 = $ln['54_58'];
				$tpl->PRECO10 = $ln['acima_59'];
				$tpl->IMAGEM_DESTACADA = $ln['logo'];
				$tpl->TELEFONE1 = $ln['telefone_clientes'];
				$tpl->TELEFONE2 = $ln['telefone_vendedores'];
				$tpl->DATA_CADASTRO = $ln['data_cadastro'];
				$tpl->TAXA_INSCRICAO = $ln['taxa_inscricao'];
				$tpl->MIN_VIDAS = $ln['min_vidas'];
				$tpl->MAX_VIDAS = $ln['max_vidas'];
				$tpl->TITULARES = $ln['titulares'];
				
				if($ln['status'] == 1){
					$tpl->STATUS_EXPL = "Ativo";
					$tpl->STATUS_CHECK = "checked";
				}else{
					$tpl->STATUS_EXPL = "Inativo";	
					$tpl->STATUS_CHECK = "";
				}

				
				foreach($estadosBrasileiros as $value => $text){

					$tpl->SIGLA = $value;
					$tpl->ESTADO = $text;
				
				// Vendo se a opção atual deve ter o atributo "selected"
					if($estado_atual == $value) $tpl->SELECTED = "selected";
			
					// Caso esta não seja a opção atual, limpamos o valor da variável SELECTED
					else $tpl->clear("SELECTED");
			
					$tpl->block("BLOCO_ESTADO");	
			
				}
				
				foreach($Acomodacao as $value => $text){

					$tpl->VALOR1 = $value;
					$tpl->TEXTO1 = $text;
				
				
				// Vendo se a opção atual deve ter o atributo "selected"
					if($acomodacao_atual == $value) $tpl->SELECTED2 = "selected";
			
					// Caso esta não seja a opção atual, limpamos o valor da variável SELECTED
					else $tpl->clear("SELECTED2");
			
					$tpl->block("BLOCO_ACOMODACAO");	
			
				}
				foreach($Atuacao as $value => $text){

					$tpl->VALOR2 = $value;
					$tpl->TEXTO2 = $text;
				
				
				// Vendo se a opção atual deve ter o atributo "selected"
					if($atuacao_atual == $value) $tpl->SELECTED3 = "selected";
			
					// Caso esta não seja a opção atual, limpamos o valor da variável SELECTED
					else $tpl->clear("SELECTED3");
			
					$tpl->block("BLOCO_ATUACAO");	
			
				}
				foreach($Reembolso as $value => $text){
			
					$tpl->VALOR3 = $value;
					$tpl->TEXTO3 = $text;
				
				// Vendo se a opção atual deve ter o atributo "selected"
					if($reembolso_atual == $value) $tpl->SELECTED4 = "selected";
			
					// Caso esta não seja a opção atual, limpamos o valor da variável SELECTED
					else $tpl->clear("SELECTED4");
			
					$tpl->block("BLOCO_REEMBOLSO");	
			
				}
				foreach($Coparticipacao as $value => $text){
			
					$tpl->VALOR4 = $value;
					$tpl->TEXTO4 = $text;
				
				// Vendo se a opção atual deve ter o atributo "selected"
					if($coparticipacao_atual == $value) $tpl->SELECTED5 = "selected";
			
					// Caso esta não seja a opção atual, limpamos o valor da variável SELECTED
					else $tpl->clear("SELECTED5");
			
					$tpl->block("BLOCO_COPARTICIPACAO");	
			
				}
				foreach($Perfil as $value => $text){
			
					$tpl->VALOR5 = $value;
					$tpl->TEXTO5 = $text;
				
				// Vendo se a opção atual deve ter o atributo "selected"
					if($perfil_atual == $value) $tpl->SELECTED6 = "selected";
			
					// Caso esta não seja a opção atual, limpamos o valor da variável SELECTED
					else $tpl->clear("SELECTED6");
			
					$tpl->block("BLOCO_PERFIL");	
			
				}
				
				$query_carencia = $pdo->query("SELECT id, nome, status FROM cadastro_carencias ORDER BY id DESC")->fetchAll();
			
				foreach($query_carencia as $row){
					$tpl->ID_CARENCIA = $row['id'];
					$tpl->NOME_CARENCIA = $row['nome'];
					
					if($id_carencia == $row['id']){
						$tpl->SELECT_CARENCIA = "selected";
					}else{
						$tpl->SELECT_CARENCIA = "";
					}
					
				$tpl->block("BLOCO_CARENCIAS_ATUAL");
				}
				
				$query_informacoes = $pdo->query("SELECT id, nome, status FROM cadastro_informacoes_adicionais ORDER BY id DESC")->fetchAll();
			
				foreach($query_informacoes as $row){
					$tpl->ID_INFORMACOES = $row['id'];
					$tpl->NOME_INFORMACOES = $row['nome'];
					
					if($id_informacoes == $row['id']){
						$tpl->SELECT_INFORMACOES = "selected";
					}else{
						$tpl->SELECT_INFORMACOES = "";
					}
					
				$tpl->block("BLOCO_INFORMACOES_ATUAL");
				}
				
				$query_unidades_select = $pdo->query("SELECT id, nome, status FROM cadastro_rede_credenciada WHERE id IN (SELECT id_rede_credenciada FROM assoc_planos_adesao_rede_credenciada WHERE token_plano = '$token_plano')")->fetchAll();
			
					foreach($query_unidades_select as $linha){
						$id_especialidade = $linha['id'];
						$tpl->ID_UNIDADE_SELECT = $linha['id'];
						$tpl->UNIDADE_SELECT = $linha['nome'];
						
						$tpl->SELECIONADOS_UNIDADES = "selected";
						
						$tpl->block("BLOCO_UNIDADES_SELECIONADOS");
					}
	
				$query_unidades = $pdo->query("SELECT id, nome, status FROM cadastro_rede_credenciada WHERE id NOT IN (SELECT id_rede_credenciada FROM assoc_planos_adesao_rede_credenciada WHERE token_plano = '$token_plano')")->fetchAll();
					
					foreach($query_unidades as $linha){
						$tpl->ID_UNIDADE = $linha['id'];
						$tpl->UNIDADE = $linha['nome'];
		
						if($linha['status'] == 1){
							$tpl->STATUS_UNIDADE = "";
							$tpl->EXP_STATUS = "";
						}if($linha['status'] == 0){
							$tpl->STATUS_UNIDADE = "disabled";
							$tpl->EXP_STATUS = "<small>Inativo</small>";
						}
						
						$tpl->block("BLOCO_UNIDADES");
					}
					
					$query_operadoras_select = $pdo->query("SELECT id, nome, status FROM cadastro_operadoras WHERE id IN (SELECT id_operadora FROM assoc_planos_adesao_operadoras WHERE token_plano = '$token_plano')")->fetchAll();
			
					foreach($query_operadoras_select as $linha){
						$id_operadora = $linha['id'];
						$tpl->ID_OPERADORA_SELECT = $linha['id'];
						$tpl->OPERADORA_SELECT = $linha['nome'];
						
						$tpl->SELECIONADOS_OPERADORAS = "selected";
						
						$tpl->block("BLOCO_OPERADORAS_SELECIONADOS");
					}
	
				$query_operadoras = $pdo->query("SELECT id, nome, status FROM cadastro_operadoras WHERE id NOT IN (SELECT id_operadora FROM assoc_planos_adesao_operadoras WHERE token_plano = '$token_plano')")->fetchAll();
					
					foreach($query_operadoras as $linha){
						$tpl->ID_OPERADORA = $linha['id'];
						$tpl->OPERADORA = $linha['nome'];
		
						if($linha['status'] == 1){
							$tpl->STATUS_OPERADORA = "";
							$tpl->EXP_STATUS_OPERADORA = "";
						}if($linha['status'] == 0){
							$tpl->STATUS_OPERADORA = "disabled";
							$tpl->EXP_STATUS_OPERADORA = "<small>Inativo</small>";
						}
						
						$tpl->block("BLOCO_OPERADORAS");
					}
					
					$query_entidades_select = $pdo->query("SELECT id, nome, status FROM cadastro_entidades WHERE id IN (SELECT id_entidade FROM assoc_planos_adesao_entidades WHERE token_plano = '$token_plano')")->fetchAll();
			
					foreach($query_entidades_select as $linha){
						$id_entidade = $linha['id'];
						$tpl->ID_ENTIDADE_SELECT = $linha['id'];
						$tpl->ENTIDADE_SELECT = $linha['nome'];
						
						$tpl->SELECIONADOS_ENTIDADES = "selected";
						
						$tpl->block("BLOCO_ENTIDADES_SELECIONADOS");
					}
	
				$query_entidades = $pdo->query("SELECT id, nome, status FROM cadastro_entidades WHERE id NOT IN (SELECT id_entidade FROM assoc_planos_adesao_entidades WHERE token_plano = '$token_plano')")->fetchAll();
					
					foreach($query_entidades as $linha){
						$tpl->ID_ENTIDADE = $linha['id'];
						$tpl->ENTIDADE = $linha['nome'];
		
						if($linha['status'] == 1){
							$tpl->STATUS_ENTIDADE = "";
							$tpl->EXP_STATUS_ENTIDADE = "";
						}if($linha['status'] == 0){
							$tpl->STATUS_ENTIDADE = "disabled";
							$tpl->EXP_STATUS_ENTIDADE = "<small>Inativo</small>";
						}
						
						$tpl->block("BLOCO_ENTIDADES");
					}
					
					$query_empregos_select = $pdo->query("SELECT id, nome, status FROM cadastro_empregos WHERE id IN (SELECT id_emprego FROM assoc_planos_adesao_empregos WHERE token_plano = '$token_plano')")->fetchAll();
			
					foreach($query_empregos_select as $linha){
						$id_emprego = $linha['id'];
						$tpl->ID_EMPREGO_SELECT = $linha['id'];
						$tpl->EMPREGO_SELECT = $linha['nome'];
						
						$tpl->SELECIONADOS_EMPREGOS = "selected";
						
						$tpl->block("BLOCO_EMPREGOS_SELECIONADOS");
					}
	
				$query_empregos = $pdo->query("SELECT id, nome, status FROM cadastro_empregos WHERE id NOT IN (SELECT id_emprego FROM assoc_planos_adesao_empregos WHERE token_plano = '$token_plano')")->fetchAll();
					
					foreach($query_empregos as $linha){
						$tpl->ID_EMPREGO = $linha['id'];
						$tpl->EMPREGO = $linha['nome'];
		
						if($linha['status'] == 1){
							$tpl->STATUS_EMPREGO = "";
							$tpl->EXP_STATUS_EMPREGO = "";
						}if($linha['status'] == 0){
							$tpl->STATUS_EMPREGO = "disabled";
							$tpl->EXP_STATUS_EMPREGO = "<small>Inativo</small>";
						}
						
						$tpl->block("BLOCO_EMPREGOS");
					}
					
					$query_administradoras_select = $pdo->query("SELECT id, nome, status FROM cadastro_administradoras WHERE id IN (SELECT id_administradora FROM assoc_planos_adesao_administradoras WHERE token_plano = '$token_plano')")->fetchAll();
			
					foreach($query_administradoras_select as $linha){
						$id_administradora = $linha['id'];
						$tpl->ID_ADMINISTRADORA_SELECT = $linha['id'];
						$tpl->ADMINISTRADORA_SELECT = $linha['nome'];
						
						$tpl->SELECIONADOS_ADMINISTRADORAS = "selected";
						
						$tpl->block("BLOCO_ADMINISTRADORAS_SELECIONADOS");
					}
	
				$query_administradoras = $pdo->query("SELECT id, nome, status FROM cadastro_administradoras WHERE id NOT IN (SELECT id_administradora FROM assoc_planos_adesao_administradoras WHERE token_plano = '$token_plano')")->fetchAll();
					
					foreach($query_administradoras as $linha){
						$tpl->ID_ADMINISTRADORA = $linha['id'];
						$tpl->ADMINISTRADORA = $linha['nome'];
		
						if($linha['status'] == 1){
							$tpl->STATUS_ADMINISTRADORA = "";
							$tpl->EXP_STATUS_ADMINISTRADORA = "";
						}if($linha['status'] == 0){
							$tpl->STATUS_ADMINISTRADORA = "disabled";
							$tpl->EXP_STATUS_ADMINISTRADORA = "<small>Inativo</small>";
						}
						
						$tpl->block("BLOCO_ADMINISTRADORAS");
					}	
				
				$query_exc_preco = $pdo->query("SELECT id, idade, valor FROM cadastro_excessao_idade WHERE token_plano = '$token_plano'")->fetchAll();
			
					foreach($query_exc_preco as $linha){
						$tpl->ID_IDADE_EXC = $linha['id'];
						$tpl->IDADE_EXC = $linha['idade'];
						$tpl->PRECO_EXC = $linha['valor'];
						
						
						$tpl->block("BLOCO_PRECOS_IDADE");
						
					}
			
				$tpl->block("BLOCO_PLANOS");
			}
			
	$tpl->TOKEN = md5(uniqid(rand(), true));
	
	$tpl->ROOT = ROOT;
		
    $tpl->DATA = date('Y');
	
	$tpl->NOME_USUARIO = $_SESSION['NameUser'];
	$tpl->MENU4 = "active";
    $tpl->show();

?>