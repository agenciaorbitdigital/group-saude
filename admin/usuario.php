<?php

	require 'modulos/session-login.php';

	require '../modulos/connection-db.php';

    require_once("lib/raelgc/view/Template.php");
    use raelgc\view\Template;

    $tpl = new Template("templates/usuario.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("TOPBAR", "templates/topbar.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("SIDEBAR", "templates/sidebar.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("SCRIPTS", "templates/scripts.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("ESTILOS", "templates/estilos.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("FOOTER", "templates/footer.html");
	
	
	//Dados Usuário
	$email_login = $_SESSION['email_login'];
	
	$tpl->ROOT = ROOT;
	
	$id = $_GET['id'];
	$tpl->ID = $id;
	
	$Permissao = array(
	'1'=>'Administrador',
	'2'=>'Usuário'
	);
	
	$query_usuario = $pdo->query("SELECT id, nome, email, celular, status, token, sobrenome, data_criacao, company_id, role, admin  FROM cadastro_usuarios WHERE id = $id")->fetchAll();
 		
		foreach($query_usuario as $linha){
			
			$company_id = $linha['company_id'];
			$permissao_atual = $linha['role'];
					
			$tpl->NOME = $linha['nome'];
			$tpl->SOBRENOME = $linha['sobrenome'];
			$tpl->EMAIL = $linha['email'];
			$tpl->CELULAR = $linha['celular'];
			$tpl->DATA_CADASTRO = $linha['data_criacao'];
			
			if($linha['admin'] == 1){
				$tpl->ADMIN_CHECK = "checked";
			}else{
				$tpl->ADMIN_CHECK = "";	
			}
			
			if($linha['status'] == 1){
				$tpl->STATUS_EXPL = "Ativo";
				$tpl->STATUS_CHECK = "checked";
			}else{
				$tpl->STATUS_EXPL = "Inativo";	
				$tpl->STATUS_CHECK = "";
			}
			
			foreach($Permissao as $value => $text){

					$tpl->VALOR = $value;
					$tpl->TEXTO = $text;
				
				
				// Vendo se a opção atual deve ter o atributo "selected"
					if($permissao_atual == $value) $tpl->SELECTED = "selected";
			
					// Caso esta não seja a opção atual, limpamos o valor da variável SELECTED
					else $tpl->clear("SELECTED");
			
					$tpl->block("BLOCO_PERMISSAO");	
			
				}
			
		}
		
	$query_corretoras = $pdo->query("SELECT id, nome, status FROM cadastro_corretoras ORDER BY id DESC")->fetchAll();
			
			foreach($query_corretoras as $linha){
				$tpl->ID_CORRETORA = $linha['id'];
				$tpl->CORRETORA = $linha['nome'];
				if($linha['status'] == 1){
					$tpl->STATUS_CORRETORA = "";
					$tpl->EXP_STATUS = "";
				}if($linha['status'] == 0){
					$tpl->STATUS_CORRETORA = "disabled";
					$tpl->EXP_STATUS = "<small>Inativo</small>";
				}
				
				if($linha['id'] == $company_id){
					$tpl->SELECIONADOS = "selected";
				}else{
					$tpl->SELECIONADOS = "";
				}
				
				
				$tpl->block("BLOCO_CORRETORAS");
			}
				
	$tpl->MENU6 = "active";
	$tpl->NOME_USUARIO = $_SESSION['NameUser'];
    $tpl->show();

?>