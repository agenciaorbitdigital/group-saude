<?php

	require 'modulos/session-login.php';

	require '../modulos/connection-db.php';

    require_once("lib/raelgc/view/Template.php");
    use raelgc\view\Template;

    $tpl = new Template("templates/add-seguradora.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("TOPBAR", "templates/topbar.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("SIDEBAR", "templates/sidebar.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("SCRIPTS", "templates/scripts.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("ESTILOS", "templates/estilos.html");
	
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("FOOTER", "templates/footer.html");
	
	
	//Dados Usuário
	$email_login = $_SESSION['email_login'];
	
	$query_usuario = $pdo->query("SELECT id, nome, data_criacao FROM cadastro_usuarios WHERE email = '$email_login'")->fetchAll();
 		
		foreach($query_usuario as $linha){
			$id_usuario = $linha['id'];
			$nome = $linha['nome'];
			$data_cadastro = $linha['data_criacao'];
			
			$_SESSION['UserName'] = $linha['nome'];
		}
	$query_hospital = $pdo->query("SELECT id, nome FROM cadastro_rede_credenciada ORDER BY id DESC")->fetchAll();
			
			foreach($query_hospital as $linha){
				$tpl->ID_HOSPITAL = $linha['id'];
				$tpl->HOSPITAL = $linha['nome'];
				
				
				$tpl->block("BLOCO_HOSPITAL");
			}	
			
	$query_carencia = $pdo->query("SELECT id, nome FROM cadastro_carencias ORDER BY id DESC")->fetchAll();
			
			foreach($query_carencia as $linha){
				$tpl->ID_CARENCIA = $linha['id'];
				$tpl->CARENCIA = $linha['nome'];
				
				
				$tpl->block("BLOCO_CARENCIAS");
			}	
			
	$query_planos = $pdo->query("SELECT id, token, nome FROM cadastro_planos WHERE status = 1 ORDER BY id DESC")->fetchAll();
			
			foreach($query_planos as $ln){
				$tpl->ID_PLANO = $ln['id'];
				$tpl->NOME_PLANO = $ln['nome'];
				$tpl->TOKEN_PLANO = $ln['token'];
				
				
				$tpl->block("BLOCO_PLANOS");
			}	
		
	$tpl->TOKEN = md5(uniqid(rand(), true));
	
	$tpl->ROOT = ROOT;
		
    $tpl->DATA = date('Y');
	
	$tpl->NOME_USUARIO = $_SESSION['NameUser'];
	$tpl->MENU2 = "active";
	$tpl->MENU2_2 = "active";
    $tpl->show();

?>