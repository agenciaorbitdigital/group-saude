<?php

	require 'modulos/session-login.php';

	require '../modulos/connection-db.php';

    require_once("lib/raelgc/view/Template.php");
    use raelgc\view\Template;

    $tpl = new Template("templates/duplicar-unidade.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("TOPBAR", "templates/topbar.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("SIDEBAR", "templates/sidebar.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("SCRIPTS", "templates/scripts.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("ESTILOS", "templates/estilos.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("FOOTER", "templates/footer.html");
	
	
	//Dados Usuário
	$email_login = $_SESSION['email_login'];
	
	$tpl->ROOT = ROOT;
	
	$slug = $_GET['slug'];

	$Regiao = array(
	'ABC'=>'ABC',
	'Centro'=>'Centro',
	'Litoral'=>'Litoral',
	'Zona Leste'=>'Zona Leste',
	'Zona Norte'=>'Zona Norte',
	'Zona Oeste'=>'Zona Oeste',
	'Zona Sul'=>'Zona Sul'
	);
	
	$Categoria = array(
	'Clínica' => 'clinica',
	'Hospital' => 'hospital',
	'Laboratório' => 'laboratorio'											
	);					
	
	$query_unidade = $pdo->query("SELECT * FROM cadastro_rede_credenciada WHERE slug = '$slug'")->fetchAll();
 		
		foreach($query_unidade as $linha){
			$token_rede_credenciada = $linha['token'];
			$id_rede_credenciada = $linha['id'];
			$tpl->NOME = $linha['nome'];
			$tpl->TOKEN = $linha['token'];
			$tpl->ID = $linha['id'];
			$tpl->EMAIL = $linha['email'];
			$tpl->TELEFONE = $linha['telefone'];
			$tpl->TELEFONE2 = $linha['telefone_adicional'];
			$tpl->ENDERECO = $linha['endereco'];
			$tpl->NUMERO = $linha['numero'];
			$tpl->BAIRRO = $linha['bairro'];
			$tpl->CEP = $linha['cep'];
			$tpl->CIDADE = $linha['cidade'];
			$tpl->ESTADO = $linha['estado'];
			$tpl->COMPLEMENTO = $linha['complemento'];
			$regiao_atual = $linha['regiao'];
			$categoria_atual = $linha['categoria'];
			
			$tpl->HORARIOS_ATENDIMENTO = $linha['atendimento'];
			
			if($linha['status'] == 1){

				$tpl->STATUS_CHECK = "checked";
			}else{
				$tpl->STATUS_CHECK = "";
			}
			
			if($linha['a_ambulatorio'] == 1){
				$tpl->ATENDIMENTO1_CHECK = "checked";
			}else{
				$tpl->ATENDIMENTO1_CHECK = "";
			}
			
			if($linha['h_hospital'] == 1){
				$tpl->ATENDIMENTO2_CHECK = "checked";
			}else{
				$tpl->ATENDIMENTO2_CHECK = "";
			}
			
			if($linha['m_maternidade'] == 1){
				$tpl->ATENDIMENTO3_CHECK = "checked";
			}else{
				$tpl->ATENDIMENTO3_CHECK = "";
			}
			
			if($linha['pa_pronto_atendimento'] == 1){
				$tpl->ATENDIMENTO4_CHECK = "checked";
			}else{
				$tpl->ATENDIMENTO4_CHECK = "";
			}
			
			if($linha['ps_pronto_socorro'] == 1){
				$tpl->ATENDIMENTO5_CHECK = "checked";
			}else{
				$tpl->ATENDIMENTO5_CHECK = "";
			}
			
			if($linha['psi_pronto_socorro_infantil'] == 1){
				$tpl->ATENDIMENTO6_CHECK = "checked";
			}else{
				$tpl->ATENDIMENTO6_CHECK = "";
			}
			
			foreach($Regiao as $value => $text){
			
					$tpl->VALOR3 = $value;
					$tpl->TEXTO3 = $text;
				
				// Vendo se a opção atual deve ter o atributo "selected"
					if($regiao_atual == $value) $tpl->SELECTED4 = "selected";
			
					// Caso esta não seja a opção atual, limpamos o valor da variável SELECTED
					else $tpl->clear("SELECTED4");
			
					$tpl->block("BLOCO_REGIAO");	
			
				}
			foreach($Categoria as $value2 => $text2){
			
					$tpl->VALOR4 = $value2;
					$tpl->TEXTO4 = $text2;
				
				// Vendo se a opção atual deve ter o atributo "selected"
					if($categoria_atual == $text2) $tpl->SELECTED5 = "selected";
			
					// Caso esta não seja a opção atual, limpamos o valor da variável SELECTED
					else $tpl->clear("SELECTED5");
			
					$tpl->block("BLOCO_CATEGORIA");	
			
				}
		}
		
	$query_epecialidades_select = $pdo->query("SELECT id, nome, status FROM cadastro_especialidades WHERE id IN (SELECT id_especialidade FROM assoc_rede_credenciada_especialidades WHERE token_rede_credenciada = '$token_rede_credenciada')")->fetchAll();
			
			foreach($query_epecialidades_select as $linha){
				$id_especialidade = $linha['id'];
				$tpl->ID_ESPECIALIDADE_SELECT = $linha['id'];
				$tpl->ESPECIALIDADE_SELECT = $linha['nome'];
				
				$tpl->SELECIONADOS = "selected";
				
				$tpl->block("BLOCO_ESPECIALIDADES_SELECIONADOS");
			}
	
					
	$query_especialidades = $pdo->query("SELECT id, nome, status FROM cadastro_especialidades WHERE id NOT IN (SELECT id_especialidade FROM assoc_rede_credenciada_especialidades WHERE token_rede_credenciada = '$token_rede_credenciada')")->fetchAll();
			
			foreach($query_especialidades as $linha){
				$tpl->ID_ESPECIALIDADE = $linha['id'];
				$tpl->ESPECIALIDADE = $linha['nome'];

				if($linha['status'] == 1){
					$tpl->STATUS_ESPECIALIDADE = "";
					$tpl->EXP_STATUS = "";
				}if($linha['status'] == 0){
					$tpl->STATUS_ESPECIALIDADE = "disabled";
					$tpl->EXP_STATUS = "<small>Inativo</small>";
				}
				
				$tpl->block("BLOCO_ESPECIALIDADES");
			}
				

	$tpl->TOKEN = md5(uniqid(rand(), true));
	
	$tpl->NOME_USUARIO = $_SESSION['NameUser'];
	$tpl->MENU3 = "active";
	$tpl->MENU3_2 = "active";
    $tpl->show();

?>