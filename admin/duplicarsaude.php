<?php

	require 'modulos/session-login.php';

	require '../modulos/connection-db.php';

    require_once("lib/raelgc/view/Template.php");
    use raelgc\view\Template;

    $tpl = new Template("templates/duplicar-saude.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("TOPBAR", "templates/topbar.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("SIDEBAR", "templates/sidebar.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("SCRIPTS", "templates/scripts.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("ESTILOS", "templates/estilos.html");
	
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("FOOTER", "templates/footer.html");
	
	
	//Dados Usuário
	$email_login = $_SESSION['email_login'];
	
	//Dados Usuário
	$email_login = $_SESSION['email_login'];
	
	$slug = $_GET['slug'];
	

	
	$Acomodacao = array(
	'1'=>'Quarto coletivo ou Enfermaria',
	'2'=>'Quarto privativo ou Apartamento'
	);
	
	$Atuacao = array(
	'Regional'=>'Regional',
	'Nacional'=>'Nacional'
	);
	
	$Reembolso = array(
	'Sim'=>'Sim',
	'Não'=>'Não'
	);
	
	$Coparticipacao = array(
	'Sim'=>'Sim',
	'Não'=>'Não'
	);
	
	$Perfil = array(
	'1'=>'Familiar',
	'2'=>'Individual',
	'3'=>'PME'
	);
	
	$query_seguradoras = $pdo->query("SELECT id, nome, status, token, data_cadastro, telefone_clientes, telefone_vendedores, logo, estado, taxa_inscricao, min_vidas, max_vidas, titulares FROM cadastro_seguradoras WHERE slug = '$slug'")->fetchAll();
			
			foreach($query_seguradoras as $linha){
				$token = $linha['token'];
				$tpl->ID = $linha['id'];
				$tpl->TELEFONE1 = $linha['telefone_clientes'];
				$tpl->TELEFONE2 = $linha['telefone_vendedores'];
				$tpl->NOME = $linha['nome'];
				$estado_atual = $linha['estado'];
				
				$tpl->IMAGEM_DESTACADA = $linha['logo'];
				$tpl->ESTADO = $linha['estado'];
				$tpl->TAXA_INSCRICAO = $linha['taxa_inscricao'];
				$tpl->MIN_VIDAS = $linha['min_vidas'];
				$tpl->MAX_VIDAS = $linha['max_vidas'];
				$tpl->TITULARES = $linha['titulares'];
				
			}
			

	
	$query_planos = $pdo->query("SELECT * FROM cadastro_planos WHERE slug = '$slug'")->fetchAll();
			
			$i=1;
			foreach($query_planos as $ln){
				$token_plano = $ln['token'];
				$atuacao_atual = $ln['atuacao'];
				$perfil_atual = $ln['perfil'];
				$acomodacao_atual = $ln['acomodacao'];
				$reembolso_atual = $ln['reembolso'];
				$coparticipacao_atual = $ln['coparticipacao'];
				$id_carencia = $ln['carencia'];
				$id_informacoes = $ln['id_informacoes'];
				$tpl->ID_PLANO = $ln['id'];
				$tpl->NOME_PLANO = $ln['nome'];
				$tpl->TOKEN = $ln['token'];
				$tpl->PRECO1 = $ln['0_18'];
				$tpl->PRECO2 = $ln['19_23'];
				$tpl->PRECO3 = $ln['24_28'];
				$tpl->PRECO4 = $ln['29_33'];
				$tpl->PRECO5 = $ln['34_38'];
				$tpl->PRECO6 = $ln['39_43'];
				$tpl->PRECO7 = $ln['44_48'];
				$tpl->PRECO8 = $ln['49_53'];
				$tpl->PRECO9 = $ln['54_58'];
				$tpl->PRECO10 = $ln['acima_59'];
				$tpl->DATA_CADASTRO = $ln['data_cadastro'];

				
				if($ln['status'] == 1){
				$tpl->STATUS_EXPL = "Ativo";
				$tpl->STATUS_CHECK = "checked";
			}else{
				$tpl->STATUS_EXPL = "Inativo";	
				$tpl->STATUS_CHECK = "";
			}
			
				foreach($Acomodacao as $value => $text){

					$tpl->VALOR1 = $value;
					$tpl->TEXTO1 = $text;
				
				
				// Vendo se a opção atual deve ter o atributo "selected"
					if($acomodacao_atual == $value) $tpl->SELECTED2 = "selected";
			
					// Caso esta não seja a opção atual, limpamos o valor da variável SELECTED
					else $tpl->clear("SELECTED2");
			
					$tpl->block("BLOCO_ACOMODACAO");	
			
				}
				foreach($Atuacao as $value => $text){

					$tpl->VALOR2 = $value;
					$tpl->TEXTO2 = $text;
				
				
				// Vendo se a opção atual deve ter o atributo "selected"
					if($atuacao_atual == $value) $tpl->SELECTED3 = "selected";
			
					// Caso esta não seja a opção atual, limpamos o valor da variável SELECTED
					else $tpl->clear("SELECTED3");
			
					$tpl->block("BLOCO_ATUACAO");	
			
				}
				foreach($Reembolso as $value => $text){
			
					$tpl->VALOR3 = $value;
					$tpl->TEXTO3 = $text;
				
				// Vendo se a opção atual deve ter o atributo "selected"
					if($reembolso_atual == $value) $tpl->SELECTED4 = "selected";
			
					// Caso esta não seja a opção atual, limpamos o valor da variável SELECTED
					else $tpl->clear("SELECTED4");
			
					$tpl->block("BLOCO_REEMBOLSO");	
			
				}
				foreach($Coparticipacao as $value => $text){
			
					$tpl->VALOR4 = $value;
					$tpl->TEXTO4 = $text;
				
				// Vendo se a opção atual deve ter o atributo "selected"
					if($coparticipacao_atual == $value) $tpl->SELECTED5 = "selected";
			
					// Caso esta não seja a opção atual, limpamos o valor da variável SELECTED
					else $tpl->clear("SELECTED5");
			
					$tpl->block("BLOCO_COPARTICIPACAO");	
			
				}
				foreach($Perfil as $value => $text){
			
					$tpl->VALOR5 = $value;
					$tpl->TEXTO5 = $text;
				
				// Vendo se a opção atual deve ter o atributo "selected"
					if($perfil_atual == $value) $tpl->SELECTED6 = "selected";
			
					// Caso esta não seja a opção atual, limpamos o valor da variável SELECTED
					else $tpl->clear("SELECTED6");
			
					$tpl->block("BLOCO_PERFIL");	
			
				}
				
				$query_carencia = $pdo->query("SELECT id, nome, status FROM cadastro_carencias ORDER BY id DESC")->fetchAll();
			
				foreach($query_carencia as $row){
					$tpl->ID_CARENCIA = $row['id'];
					$tpl->NOME_CARENCIA = $row['nome'];
					
					if($id_carencia == $row['id']){
						$tpl->SELECT_CARENCIA = "selected";
					}else{
						$tpl->SELECT_CARENCIA = "";
					}
					
				$tpl->block("BLOCO_CARENCIAS_ATUAL");
				}
				
				$query_informacoes = $pdo->query("SELECT id, nome, status FROM cadastro_informacoes_adicionais ORDER BY id DESC")->fetchAll();
			
				foreach($query_informacoes as $row){
					$tpl->ID_INFORMACOES = $row['id'];
					$tpl->NOME_INFORMACOES = $row['nome'];
					
					if($id_informacoes == $row['id']){
						$tpl->SELECT_INFORMACOES = "selected";
					}else{
						$tpl->SELECT_INFORMACOES = "";
					}
					
				$tpl->block("BLOCO_INFORMACOES_ATUAL");
				}
				$query_unidades_select = $pdo->query("SELECT id, nome, status, ps_pronto_socorro, psi_pronto_socorro_infantil, m_maternidade, h_hospital, pa_pronto_atendimento, a_ambulatorio FROM cadastro_rede_credenciada WHERE id IN (SELECT id_rede_credenciada FROM assoc_planos_rede_credenciada WHERE token_plano = '$token_plano')")->fetchAll();
			
					foreach($query_unidades_select as $linha){
						$id_especialidade = $linha['id'];
						$tpl->ID_UNIDADE_SELECT = $linha['id'];
					
						$unidade = $linha['nome'];
						if($linha['a_ambulatorio'] == 1){
							$ambulatorio = "A / ";
							}else{
								$ambulatorio = "";
							}
							if($linha['h_hospital'] == 1){
							$hospital = "H / ";
							}else{
								$hospital = "";
							}
							if($linha['m_maternidade'] == 1){
							$maternidade = "M / ";
							}else{
								$maternidade = "";
							}
							if($linha['pa_pronto_atendimento'] == 1){
							$p_atendimento = "PA / ";
							}else{
								$p_atendimento = "";
							}
							if($linha['ps_pronto_socorro'] == 1){
							$p_socorro = "PS ";
							}else{
								$p_socorro = "";
							}
							
							if($linha['a_ambulatorio'] OR $linha['h_hospital'] OR $linha['m_maternidade'] OR $linha['pa_pronto_atendimento'] OR $linha['ps_pronto_socorro'] == 1){
							$tpl->UNIDADE_SELECT = $unidade ." - " .$ambulatorio . $hospital . $maternidade .$p_atendimento . $p_socorro;
							}else{
								$tpl->UNIDADE_SELECT = $unidade;	
							}
					
						$tpl->SELECIONADOS_UNIDADES = "selected";
						
						$tpl->block("BLOCO_UNIDADES_SELECIONADOS");
					}
	
				$query_unidades = $pdo->query("SELECT id, nome, status, ps_pronto_socorro, psi_pronto_socorro_infantil, m_maternidade, h_hospital, pa_pronto_atendimento, a_ambulatorio FROM cadastro_rede_credenciada WHERE id NOT IN (SELECT id_rede_credenciada FROM assoc_planos_rede_credenciada WHERE token_plano = '$token_plano')")->fetchAll();
					
					foreach($query_unidades as $linha){
						$tpl->ID_UNIDADE = $linha['id'];
						$unidade = $linha['nome'];
						if($linha['a_ambulatorio'] == 1){
							$ambulatorio = "A / ";
							}else{
								$ambulatorio = "";
							}
							if($linha['h_hospital'] == 1){
							$hospital = "H / ";
							}else{
								$hospital = "";
							}
							if($linha['m_maternidade'] == 1){
							$maternidade = "M / ";
							}else{
								$maternidade = "";
							}
							if($linha['pa_pronto_atendimento'] == 1){
							$p_atendimento = "PA / ";
							}else{
								$p_atendimento = "";
							}
							if($linha['ps_pronto_socorro'] == 1){
							$p_socorro = "PS ";
							}else{
								$p_socorro = "";
							}
							
							if($linha['a_ambulatorio'] OR $linha['h_hospital'] OR $linha['m_maternidade'] OR $linha['pa_pronto_atendimento'] OR $linha['ps_pronto_socorro'] == 1){
							$tpl->UNIDADE = $unidade ." - " .$ambulatorio . $hospital . $maternidade .$p_atendimento . $p_socorro;
							}else{
								$tpl->UNIDADE = $unidade;	
							}
		
						if($linha['status'] == 1){
							$tpl->STATUS_UNIDADE = "";
							$tpl->EXP_STATUS = "";
						}if($linha['status'] == 0){
							$tpl->STATUS_UNIDADE = "disabled";
							$tpl->EXP_STATUS = "<small>Inativo</small>";
						}
						
						$tpl->block("BLOCO_UNIDADES");
					}	
				
				$query_exc_preco = $pdo->query("SELECT id, idade, valor FROM cadastro_excessao_idade WHERE token_plano = '$token_plano'")->fetchAll();
			
					foreach($query_exc_preco as $linha){
						$tpl->ID_IDADE_EXC = $linha['id'];
						$tpl->IDADE_EXC = $linha['idade'];
						$tpl->PRECO_EXC = $linha['valor'];
						
						
						$tpl->block("BLOCO_PRECOS_IDADE");
						
					}
			
			}
			
	
	$query_hospital = $pdo->query("SELECT id, nome FROM cadastro_rede_credenciada ORDER BY id DESC")->fetchAll();
			
			foreach($query_hospital as $linha){
				$tpl->ID_HOSPITAL = $linha['id'];
				$tpl->HOSPITAL = $linha['nome'];
				
				
				$tpl->block("BLOCO_HOSPITAL");
			}	
			
	$query_carencia = $pdo->query("SELECT id, nome FROM cadastro_carencias ORDER BY id DESC")->fetchAll();
			
			foreach($query_carencia as $linha){
				$tpl->ID_CARENCIA = $linha['id'];
				$tpl->CARENCIA = $linha['nome'];
				
				
				$tpl->block("BLOCO_CARENCIAS");
				
			}
	
	$tpl->TOKEN = md5(uniqid(rand(), true));
	
	$tpl->ROOT = ROOT;
		
    $tpl->DATA = date('Y');
	
	$tpl->NOME_USUARIO = $_SESSION['NameUser'];
	$tpl->MENU2 = "active";
    $tpl->show();

?>