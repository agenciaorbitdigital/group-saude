<?php

	require 'modulos/session-login.php';

	require '../modulos/connection-db.php';

    require_once("lib/raelgc/view/Template.php");
    use raelgc\view\Template;

    $tpl = new Template("templates/corretora.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("TOPBAR", "templates/topbar.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("SIDEBAR", "templates/sidebar.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("SCRIPTS", "templates/scripts.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("ESTILOS", "templates/estilos.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("FOOTER", "templates/footer.html");
	
	
	//Dados Usuário
	$email_login = $_SESSION['email_login'];
	
	$tpl->ROOT = ROOT;
	
	$slug = $_GET['slug'];
		
	$query_corretoras = $pdo->query("SELECT * FROM cadastro_corretoras WHERE slug = '$slug'")->fetchAll();
			
			foreach($query_corretoras as $linha){
				$tpl->ID = $linha['id'];
				$tpl->CORRETORA = $linha['nome'];
				$tpl->TELEFONE = $linha['telefone'];
				$tpl->ENDERECO = $linha['endereco'];
				$tpl->NUMERO = $linha['numero'];
				$tpl->BAIRRO = $linha['bairro'];
				$tpl->CEP = $linha['cep'];
				$tpl->CIDADE = $linha['cidade'];
				$tpl->ESTADO = $linha['estado'];
				$tpl->COMPLEMENTO = $linha['complemento'];
				
				$tpl->IMAGEM_DESTACADA = $linha['logo'];
				$tpl->DATA_CADASTRO = $linha['data_cadastro'];
				
				if($linha['status'] == 1){
					$tpl->STATUS_EXPL = "Ativo";
					$tpl->STATUS_CHECK = "checked";
				}else{
					$tpl->STATUS_EXPL = "Inativo";	
					$tpl->STATUS_CHECK = "";
				}

			}
			
	$tpl->NOME_USUARIO = $_SESSION['NameUser'];				
	$tpl->MENU5 = "active";
    $tpl->show();

?>