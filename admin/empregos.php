<?php

	require 'modulos/session-login.php';

	require '../modulos/connection-db.php';

    require_once("lib/raelgc/view/Template.php");
    use raelgc\view\Template;

    $tpl = new Template("templates/empregos.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("TOPBAR", "templates/topbar.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("SIDEBAR", "templates/sidebar.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("SCRIPTS", "templates/scripts.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("ESTILOS", "templates/estilos.html");
	
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("FOOTER", "templates/footer.html");
	
	
	//Dados Usuário
	$email_login = $_SESSION['email_login'];
	
	$query_usuario = $pdo->query("SELECT id, nome, data_criacao FROM cadastro_usuarios WHERE email = '$email_login'")->fetchAll();
 		
		foreach($query_usuario as $linha){
			$id_usuario = $linha['id'];
			$nome = $linha['nome'];
			$data_cadastro = $linha['data_criacao'];
			
			$_SESSION['UserName'] = $linha['nome'];
		}
		
	$query_empregoss = $pdo->query("SELECT id, nome FROM cadastro_empregos ORDER BY id DESC")->fetchAll();
			
			foreach($query_empregoss as $linha){
				$tpl->ID = $linha['id'];
				$tpl->NOME = $linha['nome'];
				
				$tpl->block("BLOCO_LISTAGEM");
			}
			
	$tpl->ROOT = ROOT;
		
    $tpl->DATA = date('Y');
	
	$tpl->NOME_USUARIO = $_SESSION['NameUser'];
	$tpl->MENU4 = "active";
	$tpl->MENU4_2 = "active";
    $tpl->show();

?>