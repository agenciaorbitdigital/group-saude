<?php

	require 'modulos/session-login.php';

	require '../modulos/connection-db.php';

    require_once("lib/raelgc/view/Template.php");
    use raelgc\view\Template;

    $tpl = new Template("templates/add-usuario.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("TOPBAR", "templates/topbar.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("SCRIPTS", "templates/scripts.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("ESTILOS", "templates/estilos.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("SIDEBAR", "templates/sidebar.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("FOOTER", "templates/footer.html");
	
	$query_corretoras = $pdo->query("SELECT id, nome, status FROM cadastro_corretoras WHERE status = '1' ORDER BY id DESC")->fetchAll();
			
			foreach($query_corretoras as $linha){
				$tpl->ID_CORRETORA = $linha['id'];
				$tpl->CORRETORA = $linha['nome'];
				if($linha['status'] == 1){
					$tpl->STATUS = "";
					$tpl->EXP_STATUS = "";
				}if($linha['status'] == 0){
					$tpl->STATUS = "disabled";
					$tpl->EXP_STATUS = "<small>Inativo</small>";
				}
				
				$tpl->block("BLOCO_CORRETORAS");
			}
			
	//Dados Usuário
	$email_login = $_SESSION['email_login'];
	$tpl->TOKEN = md5(uniqid(rand(), true));
	$tpl->NOME_USUARIO = $_SESSION['NameUser'];
	$tpl->ROOT = ROOT;
	$tpl->MENU6 = "active";
	$tpl->MENU6_2 = "active";
    $tpl->show();

?>