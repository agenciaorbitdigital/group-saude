<?php error_reporting(E_ALL & ~E_NOTICE & ~E_WARNING);
	
	require 'modulos/session-login.php';

	require 'modulos/connection-db.php';

    require_once("lib/raelgc/view/Template.php");
    use raelgc\view\Template;

    $tpl = new Template("templates/processa-simulacao-odonto.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("TOPBAR", "templates/topbar.html");

		// Adicionando mais um arquivo HTML
    $tpl->addFile("FOOTER", "templates/footer.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("SIDEBAR", "templates/sidebar.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("SCRIPTS", "templates/scripts.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("ESTILOS", "templates/estilos.html");
	
	if(!isset($_POST['seguradora'])){	
		echo "<script>alert('Não existem seguradoras cadastradas!');window.history.go(-1);</script>";	
	}else{
		$token = $_POST['seguradora'];
	
	$perfil = $_POST['perfil'];
	if($perfil == "1"){
		$tpl->PERFIL = "Familiar";
	}
	if($perfil == "2"){
		$tpl->PERFIL = "Individual";
	}
	if($perfil == "3"){
		$tpl->PERFIL = "PME";
	}
	
	}
	
	$tpl->QUANTIDADE_VIDAS = $_POST['quantidade'];
	
	if($_POST['quantidade'] > 1){
		$tpl->PLURAL_VIDAS = "s";
	}else{
		$tpl->PLURAL_VIDAS = "";	
	}
	
	$estado = $_POST['estado'];
	$estadosBrasileiros = array(
		'AC'=>'Acre','AL'=>'Alagoas','AP'=>'Amapá','AM'=>'Amazonas','BA'=>'Bahia','CE'=>'Ceará','DF'=>'Distrito Federal','ES'=>'Espírito Santo','GO'=>'Goiás','MA'=>'Maranhão','MT'=>'Mato Grosso','MS'=>'Mato Grosso do Sul','MG'=>'Minas Gerais','PA'=>'Pará','PB'=>'Paraíba','PR'=>'Paraná','PE'=>'Pernambuco','PI'=>'Piauí','RJ'=>'Rio de Janeiro','RN'=>'Rio Grande do Norte','RS'=>'Rio Grande do Sul','RO'=>'Rondônia','RR'=>'Roraima','SC'=>'Santa Catarina','SP'=>'São Paulo','SE'=>'Sergipe','TO'=>'Tocantins'
		);

		if(array_key_exists($estado, $estadosBrasileiros))
		{
		$tpl->SIGLA = $estado;
		$tpl->ESTADO = $estadosBrasileiros[$estado];
		}
		
	$query_seguradora = $pdo->query("SELECT id, nome, logo, token FROM cadastro_seguradoras_odonto WHERE token = '$token'")->fetchAll();
				
		foreach($query_seguradora as $linha){
			$tpl->SEGURADORA = $linha['nome'];
			$tpl->IMAGEM_DESTACADA = $linha['logo'];			
		}
	$query_assoc_seguradora = $pdo->query("SELECT token_plano FROM assoc_planos_odonto_seguradoras WHERE token_seguradora = '$token' ORDER BY id DESC")->fetchAll();
				
	foreach($query_assoc_seguradora as $ln){
		$token_assoc_plano = $ln['token_plano'];
		
	$query_planos = $pdo->query("SELECT id, nome, token FROM cadastro_planos_odonto WHERE token = '$token_assoc_plano'")->fetchAll();
				
		foreach($query_planos as $linha){
			$tpl->PLANOS = '<th><label><input type="checkbox" name="planos[]" value="'.$linha['id'].'" checked /> Imprimir o plano:</label><br />'.$linha['nome'].'</th>';
			
			$tpl->block("BLOCO_THEAD");				
		}
	

	if(isset($_POST['preco'])){
		$preco = $_POST['preco'];
		
		$tpl->TD_VALOR = "<td>Valor</td>";
		
		$query_preco = $pdo->query("SELECT preco FROM cadastro_planos_odonto WHERE token = '$token_assoc_plano'")->fetchAll();
				
		foreach($query_preco as $linha){
			$tpl->PRECOS = "<th>R$ ".$linha['preco']."</th>";	
			
			$tpl->block("BLOCO_PRECO");		
		}
	}

	if(isset($_POST['fundo_reserva'])){
		$fundo_reserva = $_POST['fundo_reserva'];
		
		$tpl->TD_FUNDO = "<td>Fundo de Reserva</td>";
		
		$query_fundo_reserva = $pdo->query("SELECT fundo_reserva FROM cadastro_planos_odonto WHERE token = '$token_assoc_plano'")->fetchAll();
				
		foreach($query_fundo_reserva as $linha){
			if($linha['fundo_reserva'] == "1"){
				$tpl->FUNDOS = "<th>Com Fundo de Reserva</th>";		
			}
			if($linha['fundo_reserva'] == "2"){
				$tpl->FUNDOS = "<th>Sem Fundo de Reserva</th>";	
			}
			
			
			$tpl->block("BLOCO_FUNDO");		
		}
	}
	if(isset($_POST['reembolso'])){
		$reembolso = $_POST['reembolso'];
		
		$tpl->TD_REEMBOLSO = "<td>Rembolso</td>";
		
		$query_reembolso = $pdo->query("SELECT reembolso FROM cadastro_planos_odonto WHERE token = '$token_assoc_plano'")->fetchAll();
				
		foreach($query_reembolso as $linha){
			$tpl->REEMBOLSOS = "<th>".$linha['reembolso']."</th>";	
			
			$tpl->block("BLOCO_REEMBOLSO");		
		}
	}
	
	if(isset($_POST['taxa'])){
		$taxa = $_POST['taxa'];
		
		$query_taxa = $pdo->query("SELECT taxa_inscricao FROM cadastro_seguradoras_odonto WHERE token = '$token'")->fetchAll();
				
		foreach($query_taxa as $linha){
			if($linha['taxa_inscricao'] != ""){
				$tpl->TAXA = "<h2>Taxa de Inscrição: ".$linha['taxa_inscricao']."</h2>";
			}else{
				$tpl->TAXA = "<h2>Taxa de Inscrição: Não Informado</h2>";	
			}
		}
	}
	
	if(isset($_POST['atuacao'])){
		$atuacao = $_POST['atuacao'];
		
		$tpl->TD_ABRANGENCIA = "<td>Abrangência</td>";
		
		$query_abrangencia = $pdo->query("SELECT atuacao FROM cadastro_planos_odonto WHERE token = '$token_assoc_plano'")->fetchAll();
				
		foreach($query_abrangencia as $linha){
			$tpl->ABRANGENCIAS = "<th>".$linha['atuacao']."</th>";	
			
			$tpl->block("BLOCO_ABRANGENCIA");		
		}
	}
	if(isset($_POST['coparticipacao'])){
		$coparticipacao = $_POST['coparticipacao'];	
		
		$tpl->TD_COPARTICIPACAO = "<td>Coparticipação</td>";
		
		$query_coparticipacao = $pdo->query("SELECT coparticipacao FROM cadastro_planos_odonto WHERE token = '$token_assoc_plano'")->fetchAll();
				
		foreach($query_coparticipacao as $linha){
			$tpl->COPARTICIPACOES = "<th>".$linha['coparticipacao']."</th>";	
			
			$tpl->block("BLOCO_COPARTICIPACAO");		
		}
	}
		
	if(isset($_POST['carencia'])){
		$carencia = $_POST['carencia'];
		
		$tpl->TD_CARENCIA = "<td>Carência</td>";
		
		$query_id_carencia = $pdo->query("SELECT carencia FROM cadastro_planos_odonto WHERE token = '$token_assoc_plano'")->fetchAll();
				
		foreach($query_id_carencia as $linha){
			$id_carencia = $linha['carencia'];
			
			$query_carencia = $pdo->query("SELECT slug FROM cadastro_carencias WHERE id = '$id_carencia'")->fetchAll();
				
			foreach($query_carencia as $row){
			
				$tpl->CARENCIAS = $row['slug'];	
				
				$tpl->block("BLOCO_CARENCIA");		
			}	
		}
	}
	
	if(isset($_POST['rede_credenciada'])){
		$rede_credenciada = $_POST['rede_credenciada'];
		
		$query_token_plano = $pdo->query("SELECT token, nome FROM cadastro_planos_odonto WHERE token = '$token_assoc_plano'")->fetchAll();
				
		foreach($query_token_plano as $linha){
			$id_plano = $linha['token'];
			$tpl->NOME_PLANO = $linha['nome'];
			
			$query_rede_credenciada = $pdo->query("SELECT id_rede_credenciada FROM assoc_planos_odonto_rede_credenciada WHERE token_plano = '$id_plano'")->fetchAll();
			
			
			foreach($query_rede_credenciada as $ln){
				$id_rede_credenciada = $ln['id_rede_credenciada'];
				
				$query_cidades = $pdo->query("SELECT cidade FROM cadastro_rede_credenciada WHERE id = '$id_rede_credenciada'")->fetchAll();

				
				foreach($query_cidades as $rw){
					$array[] = $rw['cidade'];
				}
				$count = count($query_cidades);	
				if($count >= 2){
					$array=array_unique($array);
				}else{
					$array = $array;
				}
			}
			
			
			
			foreach($array as $cidade){
				$tpl->CIDADES_UNIDADES = $cidade;
				$query_rede_credenciada = $pdo->query("SELECT id_rede_credenciada FROM assoc_planos_odonto_rede_credenciada WHERE token_plano = '$id_plano'")->fetchAll();
			
			
			foreach($query_rede_credenciada as $ln){
				$id_rede_credenciada = $ln['id_rede_credenciada'];
				$query_unidades = $pdo->query("SELECT  id, nome, token, a_ambulatorio, h_hospital, m_maternidade, pa_pronto_atendimento, ps_pronto_socorro FROM cadastro_rede_credenciada WHERE cidade = '$cidade' AND id = '$id_rede_credenciada'")->fetchAll();
			
				foreach($query_unidades as $row){
					$id_unidade = $row['id'];
					
					$token_rede_credenciada = $row['token'];
					$nome = $row['nome'];	

					if($row['a_ambulatorio'] == 1){
					$ambulatorio = "A / ";
					}else{
						$ambulatorio = "";
					}
					if($row['h_hospital'] == 1){
					$hospital = "H / ";
					}else{
						$hospital = "";
					}
					if($row['m_maternidade'] == 1){
					$maternidade = "M / ";
					}else{
						$maternidade = "";
					}
					if($row['pa_pronto_atendimento'] == 1){
					$p_atendimento = "PA / ";
					}else{
						$p_atendimento = "";
					}
					if($row['ps_pronto_socorro'] == 1){
					$p_socorro = "PS ";
					}else{
						$p_socorro = "";
					}
					
					if($row['a_ambulatorio'] OR $row['h_hospital'] OR $row['m_maternidade'] OR $row['pa_pronto_atendimento'] OR $row['ps_pronto_socorro'] == 1){
					$tpl->UNIDADES = $nome ." - " .$ambulatorio . $hospital . $maternidade .$p_atendimento . $p_socorro;
					}else{
						$tpl->UNIDADES = $nome;	
					}
					
					$tpl->block("BLOCO_UNIDADE");
				}
			
			}
			$tpl->block("BLOCO_REGIOES_UNIDADES");	
			}
			
			$count_unidades = count($query_rede_credenciada);
			

			if($count_unidades == 0){
				$tpl->SEM_UNIDADES = "Nenhuma unidade cadastrada!";
			}else{
				$tpl->SEM_UNIDADES = "";
			}
			
			$tpl->block("BLOCO_REDE_CREDENCIADA");			
		}
		
		
	}
	
	if(isset($_POST['procedimentos'])){
		$procedimentos = $_POST['procedimentos'];
		
		$tpl->PRINT_PROCEDIMENTOS = "1";
		
		$query_token_plano = $pdo->query("SELECT token, nome FROM cadastro_planos_odonto WHERE token = '$token_assoc_plano'")->fetchAll();
				
		foreach($query_token_plano as $linha){
			$id_plano = $linha['token'];
			$tpl->NOME_PLANO2 = $linha['nome'];
			
			$query_procedimento = $pdo->query("SELECT id_procedimento FROM assoc_procedimentos_planos_odonto WHERE token_plano = '$id_plano'")->fetchAll();
				
			foreach($query_procedimento as $ln){
				$id_procedimento = $ln['id_procedimento'];
				
				$query_procedimentos = $pdo->query("SELECT id, nome FROM cadastro_procedimentos WHERE id = '$id_procedimento'")->fetchAll();
				
				foreach($query_procedimentos as $row){
					
					$tpl->PROCEDIMENTO = $row['nome'];					
									
				}	
				
				$tpl->block("BLOCO_PROCEDIMENTO");
			
			}
			
			$count_procedimento = count($query_procedimento);
			
			if($count_procedimento == 0){
				$tpl->SEM_PROCEDIMENTOS = "Nenhum procedimento cadastrado!";
			}else{
				$tpl->SEM_PROCEDIMENTOS = "";
			}
			
			$tpl->block("BLOCO_PROCEDIMENTOS");	
		}
	}
	
	//INFORMAÇOES	
	if(isset($_POST['informacoes'])){
		$informacoes = $_POST['informacoes'];
		
		$tpl->PRINT_INFORMACOES = "1";
		
		$query_id_informacoes = $pdo->query("SELECT id_informacoes, nome FROM cadastro_planos_odonto WHERE token = '$token_assoc_plano'")->fetchAll();
		
		
		foreach($query_id_informacoes as $rw){
			
			$id_informacoes = $rw['id_informacoes'];
			
			$query_informacoes = $pdo->query("SELECT * FROM cadastro_informacoes_adicionais WHERE id = '$id_informacoes' AND status = '1'")->fetchAll();
		
		
			foreach($query_informacoes as $row){
			
					$tpl->NAME_PLANO = $rw['nome'];
				
				if($row['informacoes_adicionais'] OR $row['documentacao'] != ""){

				}else{

					$tpl->SEM_INFORMACOES = "Nenhuma informação adicional encontrada!";
				}

				$tpl->INFORMACOES = $row['informacoes_adicionais'];	
				$tpl->DOCUMENTOS = $row['documentacao'];	

				$tpl->block("BLOCO_INFORMACOES");
			}
		}
			
			
	}
	}
 //INFORMAÇOES
	
	$tpl->REFERENCIA = ucfirst(strftime("%B/ %Y"));
			
	$token_corretora = $_SESSION['TokenCorretora'];
	$query_company = $pdo->query("SELECT background, logo, nome FROM cadastro_corretoras WHERE token = '$token_corretora'")->fetchAll();
 		
		foreach($query_company as $ln){
			$nome = $ln['nome'];
			$background = $ln['background'];
			$logo = $ln['logo'];
		}
		
	$tpl->BACKGROUND = $background;
	if($logo != ""){
		$tpl->LOGO_CORRETORA = '<img src="'.ROOT.'/admin/imagens/'.$logo.'" class="img-responsive" alt="'.$nome.'" />';
	}else{
		$tpl->LOGO_CORRETORA = '<img src="'.ROOT.'/src/imagens/logo.png" class="img-responsive" alt="Simulador Group Saúde" />';	
	}
	
	$tpl->MENU8 = "active";	
	$tpl->NOME_USUARIO = $_SESSION['NameUser'];
	$tpl->ROOT = ROOT;
    $tpl->show();

?>