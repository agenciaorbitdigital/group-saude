﻿<?php session_start();
header('Content-Type: text/html; charset=utf-8');

	require 'connection-db.php';

if(isset($_GET['quantidade'])){
	
	$quantidade = $_GET['quantidade'];
	
	if(isset($_GET['cidade']) && ($_GET['perfil'])){
		$cidade = $_GET['cidade'];
		$perfil = $_GET['perfil'];
		
		if($cidade !== 0){
			$query_cidade = $pdo->query("SELECT id, nome FROM cadastro_rede_credenciada WHERE cidade = '$cidade' ORDER BY id DESC")->fetchAll();
		}else{
			$query_cidade = $pdo->query("SELECT id, nome FROM cadastro_rede_credenciada ORDER BY id DESC")->fetchAll();
		}
		
		$count = count($query_cidade);
		if($count > 0){
			foreach($query_cidade as $line){
				
				$id_rede_credenciada = $line['id'];
				$nome = $line['nome'];
			
				$query_assoc_rede = $pdo->query("SELECT token_plano FROM assoc_planos_adesao_rede_credenciada WHERE id_rede_credenciada = '$id_rede_credenciada' ORDER BY id DESC")->fetchAll();
			
				foreach($query_assoc_rede as $ln){
					$array[] = $ln['token_plano'];
				}
			}
			
			$array=array_unique($array);
			foreach($array as $tokens){
				
				$query_token_seguradora = $pdo->query("SELECT token_seguradora FROM cadastro_planos_adesao WHERE token = '$tokens' AND perfil = '$perfil' ORDER BY id DESC")->fetchAll();
		
				foreach($query_token_seguradora as $linha){
					$array2[] = $linha['token_seguradora'];
				}
			}
			
			$array2=array_unique($array2);
			foreach($array2 as $tokens2){
				
				$query_seguradoras = $pdo->query("SELECT id, nome, logo, status, slug, token, min_vidas, max_vidas FROM cadastro_seguradoras WHERE status = '1' AND token = '$tokens2' ORDER BY id DESC")->fetchAll();
		
				foreach($query_seguradoras as $linha){
	
					$nome = $linha['nome'];
					$slug = $linha['slug'];
					$logo = $linha['logo'];
					$token_seguradora = $linha['token'];
					$min_vidas = $linha['min_vidas'];
					$max_vidas = $linha['max_vidas'];
					
					if(($quantidade < $min_vidas) || ($quantidade > $max_vidas)){
						$grayscale = "grayscale";
					}else{
						$grayscale = "";
					}
					
					echo '<div class="form-group col-md-2">
							  <input type="radio" id="'.$slug.'" value="'.$token_seguradora.'" name="seguradora" />
							  <label for="'.$slug.'">
								  <img src="admin/imagens/'.$logo.'" alt="'.$nome.'" class="img-responsive '.$grayscale.'">
							  </label>
						  </div>';
				}
			}
		}else{
			echo '<div class="col-md-12"><div class="alert alert-danger">
					<span class="entypo-attention"></span>
					<strong>Ops!</strong>&nbsp;&nbsp;Nenhuma seguradora encontrada.
				  </div></div>';	
		}
	}
}else{
	
	if(isset($_GET['cidade']) && ($_GET['perfil'])){
		$cidade = $_GET['cidade'];
		$perfil = $_GET['perfil'];
		
		if($cidade !== 0){
			$query_cidade = $pdo->query("SELECT id, nome FROM cadastro_rede_credenciada WHERE cidade = '$cidade' ORDER BY id DESC")->fetchAll();
		}else{
			$query_cidade = $pdo->query("SELECT id, nome FROM cadastro_rede_credenciada ORDER BY id DESC")->fetchAll();
		}
		
		$count = count($query_cidade);
		if($count > 0){
			foreach($query_cidade as $line){
				
				$id_rede_credenciada = $line['id'];
				$nome = $line['nome'];
			
				$query_assoc_rede = $pdo->query("SELECT token_plano FROM assoc_planos_rede_credenciada WHERE id_rede_credenciada = '$id_rede_credenciada' ORDER BY id DESC")->fetchAll();
			
				foreach($query_assoc_rede as $ln){
					$array[] = $ln['token_plano'];
				}
			}
			
			$array=array_unique($array);
			foreach($array as $tokens){
				
				$query_token_seguradora = $pdo->query("SELECT token_seguradora FROM cadastro_planos WHERE token = '$tokens' AND perfil = '$perfil' ORDER BY id DESC")->fetchAll();
		
				foreach($query_token_seguradora as $linha){
					$array2[] = $linha['token_seguradora'];
				}
			}
			
			$array2=array_unique($array2);
			foreach($array2 as $tokens2){
				
				$query_seguradoras = $pdo->query("SELECT id, nome, logo, status, slug, token FROM cadastro_seguradoras WHERE status = '1' AND token = '$tokens2' ORDER BY id DESC")->fetchAll();
		
				foreach($query_seguradoras as $linha){
					$nome = $linha['nome'];
					$slug = $linha['slug'];
					$logo = $linha['logo'];
					$token_seguradora = $linha['token'];
					
					echo '<div class="form-group col-md-2">
							  <input type="radio" id="'.$slug.'" value="'.$token_seguradora.'" name="seguradora" />
							  <label for="'.$slug.'">
								  <img src="admin/imagens/'.$logo.'" alt="'.$nome.'" class="img-responsive">
							  </label>
						  </div>';
				}
			}
		}else{
			echo '0';	
		}
	}	
}

?>