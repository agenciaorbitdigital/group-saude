﻿<?php
// Inclui o arquivo class.phpmailer.php localizado na pasta class
require_once("class.phpmailer.php");
 
// Inicia a classe PHPMailer
$mail = new PHPMailer(true);
 
// Define os dados do servidor e tipo de conexão
// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
$mail->IsSMTP(); // Define que a mensagem será SMTP
 
try {
	 include "mail-config.php";
 
 	
     //Define os destinatário(s)
     //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
     $mail->AddAddress($email, $nome);
 
     //Campos abaixo são opcionais 
     //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
     //$mail->AddCC('destinarario@dominio.com.br', 'Destinatario'); // Copia
     //$mail->AddBCC('destinatario_oculto@dominio.com.br', 'Destinatario2`'); // Cópia Oculta
     //$mail->AddAttachment('images/phpmailer.gif');      // Adicionar um anexo
 
 	 
     //Define o corpo do email
     $mail->MsgHTML('
			<h1 style="text-align:center;">Olá, "'.$nome.'"</h1>
			<h2 style="text-align:center;">Recupere Sua Senha!</h2>
			<p style="text-align:center;">Clique no link abaixo para redefinir sua senha.</p>
			<br /><br />
			<a href="'.$link.'" target="_blank">"'.$link.'"</a

	 ');
	
 
     ////Caso queira colocar o conteudo de um arquivo utilize o método abaixo ao invés da mensagem no corpo do e-mail.
     //$mail->MsgHTML(file_get_contents('arquivo.html'));
 
     $mail->Send();
     echo '<script>window.location.href ="../email-enviado?token="'.$token.'""</script>';
 
    //caso apresente algum erro é apresentado abaixo com essa exceção.
    }catch (phpmailerException $e) {
      echo $e->errorMessage(); //Mensagem de erro costumizada do PHPMailer
}
?>