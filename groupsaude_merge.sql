-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: 01-Mar-2018 às 03:17
-- Versão do servidor: 5.7.19
-- PHP Version: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `groupsaude`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `assoc_entidades_empregos`
--

DROP TABLE IF EXISTS `assoc_entidades_empregos`;
CREATE TABLE IF NOT EXISTS `assoc_entidades_empregos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token_entidade` varchar(255) NOT NULL,
  `id_emprego` int(11) NOT NULL,
  `data_cadastro` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=69 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `assoc_entidades_empregos`
--

INSERT INTO `assoc_entidades_empregos` (`id`, `token_entidade`, `id_emprego`, `data_cadastro`) VALUES
(12, 'd30aff2a75f66ed482e3781de944fa31', 149, '2018-01-17 18:33:37'),
(13, 'd30aff2a75f66ed482e3781de944fa31', 150, '2018-01-17 18:33:37'),
(14, 'd30aff2a75f66ed482e3781de944fa31', 148, '2018-01-17 18:33:37'),
(15, 'd30aff2a75f66ed482e3781de944fa31', 146, '2018-01-17 18:33:37'),
(24, '77dccbf76f9d687f4555ebd09129304e', 149, '2018-02-22 20:13:06'),
(25, 'e089a874a79f8904705849d6893f06eb', 147, '2018-02-22 22:50:39'),
(26, '4fab2535e8c164f6fe5ec95717a60374', 147, '2018-02-22 23:07:52'),
(27, '491665194126e63836e6cae1a64b3e0c', 147, '2018-02-22 23:16:17'),
(28, 'd3d06f5ffd3e2fdf7a4c0dfa2762d3a5', 147, '2018-02-22 23:16:25'),
(30, 'e76bbc462550dbf2ad7f0f6fc52d738c', 147, '2018-02-22 23:16:59'),
(31, '4d084c7c0cbd32b3ee7bee5a088a010a', 147, '2018-02-22 23:17:07'),
(32, 'd18fdb8936591ffa01f1b8a31fd52958', 147, '2018-02-22 23:17:50'),
(33, 'd18fdb8936591ffa01f1b8a31fd52958', 147, '2018-02-22 23:17:50'),
(34, '0833af844460c8a677b13b885541bf26', 147, '2018-02-22 23:17:59'),
(35, 'a724a94015f271cb80bd9715892b7240', 147, '2018-02-23 20:27:23'),
(36, '89efb7fc2ff22c1bd3756e606af9d2fd', 147, '2018-02-23 20:58:02'),
(37, 'a2fb192e81c55c50b28ed42650f9976c', 147, '2018-02-23 21:08:31'),
(38, 'a2fb192e81c55c50b28ed42650f9976c', 147, '2018-02-23 21:08:31'),
(40, '5a0aec622c228c1fd4ee3ebb43819781', 151, '2018-02-23 21:09:31'),
(45, '9e3ae8906408f38a5c654714a9e44636', 147, '2018-02-28 13:42:06'),
(63, '1935e27bafb5a1d2c68524cef2b04f4d', 146, '2018-02-28 14:02:58'),
(64, '1935e27bafb5a1d2c68524cef2b04f4d', 151, '2018-02-28 14:02:58'),
(65, '1935e27bafb5a1d2c68524cef2b04f4d', 150, '2018-02-28 14:02:58'),
(66, '1935e27bafb5a1d2c68524cef2b04f4d', 148, '2018-02-28 14:02:58'),
(67, '1935e27bafb5a1d2c68524cef2b04f4d', 147, '2018-02-28 14:02:58'),
(68, '02037049cae524b2dbf7a7e9a8d49776', 150, '2018-02-28 14:02:14');

-- --------------------------------------------------------

--
-- Estrutura da tabela `cadastro_planos`
--

DROP TABLE IF EXISTS `cadastro_planos`;
CREATE TABLE IF NOT EXISTS `cadastro_planos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(255) NOT NULL,
  `nome` varchar(255) NOT NULL,
  `carencia` int(11) NOT NULL,
  `acomodacao` int(11) NOT NULL,
  `reembolso` int(11) NOT NULL,
  `atuacao` varchar(255) NOT NULL,
  `coparticipacao` varchar(255) NOT NULL,
  `perfil` varchar(100) NOT NULL,
  `0_18` varchar(50) NOT NULL,
  `19_23` varchar(50) NOT NULL,
  `24_28` varchar(50) NOT NULL,
  `29_33` varchar(50) NOT NULL,
  `34_38` varchar(50) NOT NULL,
  `39_43` varchar(50) NOT NULL,
  `44_48` varchar(50) NOT NULL,
  `49_53` varchar(50) NOT NULL,
  `54_58` varchar(50) NOT NULL,
  `acima_59` varchar(50) NOT NULL,
  `data_cadastro` datetime NOT NULL,
  `status` int(11) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `id_informacoes` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `cadastro_planos`
--

INSERT INTO `cadastro_planos` (`id`, `token`, `nome`, `carencia`, `acomodacao`, `reembolso`, `atuacao`, `coparticipacao`, `perfil`, `0_18`, `19_23`, `24_28`, `29_33`, `34_38`, `39_43`, `44_48`, `49_53`, `54_58`, `acima_59`, `data_cadastro`, `status`, `slug`, `id_informacoes`) VALUES
(18, 'a7afaf65348afb5cb82af0c512121d9c', 'Teste 3', 153, 2, 1, 'Nacional', 'Não', '2', '222,22', '', '', '', '', '', '', '', '', '', '2018-02-22 21:51:17', 1, 'teste-3', 130),
(21, '84c4b8e184baa98758e457e10d4043f6', 'Teste 4', 150, 1, 1, 'Regional', 'Sim', '3', '111,11', '', '', '', '', '', '', '', '', '', '2018-02-23 19:43:05', 1, 'teste-4', 130),
(22, 'd7bac0a033a1628e3e1fa984e80ec3e3', 'Teste 5', 150, 1, 1, 'Regional', 'Sim', '1', '111,11', '', '', '', '', '', '', '', '', '', '2018-02-23 19:43:16', 1, 'teste-5', 131),
(23, 'e530cd92e3fcc2a091475f6c84614b6c', 'teste', 151, 2, 2, 'Regional', 'Sim', '2', '22,12', '22,22', '', '', '', '', '', '', '', '', '2018-02-28 12:59:44', 1, 'teste-4', 128);

-- --------------------------------------------------------

--
-- Estrutura da tabela `cadastro_planos_adesao`
--

DROP TABLE IF EXISTS `cadastro_planos_adesao`;
CREATE TABLE IF NOT EXISTS `cadastro_planos_adesao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(255) NOT NULL,
  `nome` varchar(255) NOT NULL,
  `logo` varchar(255) NOT NULL,
  `telefone_clientes` varchar(30) NOT NULL,
  `telefone_vendedores` varchar(30) NOT NULL,
  `carencia` int(11) NOT NULL,
  `acomodacao` int(11) NOT NULL,
  `reembolso` int(11) NOT NULL,
  `atuacao` varchar(255) NOT NULL,
  `coparticipacao` varchar(255) NOT NULL,
  `estado` varchar(10) NOT NULL,
  `0_18` varchar(50) NOT NULL,
  `19_23` varchar(50) NOT NULL,
  `24_28` varchar(50) NOT NULL,
  `29_33` varchar(50) NOT NULL,
  `34_38` varchar(50) NOT NULL,
  `39_43` varchar(50) NOT NULL,
  `44_48` varchar(50) NOT NULL,
  `49_53` varchar(50) NOT NULL,
  `54_58` varchar(50) NOT NULL,
  `acima_59` varchar(50) NOT NULL,
  `data_cadastro` datetime NOT NULL,
  `status` int(11) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `min_vidas` int(30) NOT NULL,
  `max_vidas` int(30) NOT NULL,
  `titulares` int(11) NOT NULL,
  `id_informacoes` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `cadastro_planos_adesao`
--

INSERT INTO `cadastro_planos_adesao` (`id`, `token`, `nome`, `logo`, `telefone_clientes`, `telefone_vendedores`, `carencia`, `acomodacao`, `reembolso`, `atuacao`, `coparticipacao`, `estado`, `0_18`, `19_23`, `24_28`, `29_33`, `34_38`, `39_43`, `44_48`, `49_53`, `54_58`, `acima_59`, `data_cadastro`, `status`, `slug`, `min_vidas`, `max_vidas`, `titulares`, `id_informacoes`) VALUES
(15, '9e3ae8906408f38a5c654714a9e44636', 'teste', '7743735a96dacb8348a.png', '(11) 11111-1111', '(11) 11111-1111', 151, 1, 2, 'Regional', 'Sim', 'SP', '11,11', '11,11', '', '', '', '', '', '', '', '', '2018-02-28 13:37:31', 1, 'teste', 1, 1, 1, 127),
(16, 'efbacfe5c089f3688194e502bf77e0eb', 'testev', '4543015a96dd9b05eaa.png', '(11) 11111-1111', '(11) 11111-1111', 150, 1, 1, 'Nacional', 'Sim', 'TO', '11,11', '', '', '', '', '', '', '', '', '', '2018-02-28 13:49:30', 1, 'testev', 1, 1, 1, 128);

-- --------------------------------------------------------

--
-- Estrutura da tabela `cadastro_planos_odonto`
--

DROP TABLE IF EXISTS `cadastro_planos_odonto`;
CREATE TABLE IF NOT EXISTS `cadastro_planos_odonto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(255) NOT NULL,
  `nome` varchar(255) NOT NULL,
  `carencia` int(11) NOT NULL,
  `fundo_reserva` varchar(11) NOT NULL,
  `reembolso` varchar(255) NOT NULL,
  `atuacao` varchar(255) NOT NULL,
  `preco` varchar(20) NOT NULL,
  `coparticipacao` varchar(255) NOT NULL,
  `perfil` varchar(100) NOT NULL,
  `data_cadastro` datetime NOT NULL,
  `status` int(11) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `id_informacoes` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `cadastro_planos_odonto`
--

INSERT INTO `cadastro_planos_odonto` (`id`, `token`, `nome`, `carencia`, `fundo_reserva`, `reembolso`, `atuacao`, `preco`, `coparticipacao`, `perfil`, `data_cadastro`, `status`, `slug`, `id_informacoes`) VALUES
(8, '05553a558860f271024082853e61781a', 'Teste', 153, '1', '1', 'Regional', '11,11', 'Sim', '1', '2018-02-22 21:51:55', 1, 'teste', 125),
(9, 'ecbb56c9d7062981cfd7fbd3d85b0945', 'Teste 2', 153, 'Sim', 'Não', 'Nacional', '111,11', 'Sim', '2', '2018-02-22 21:52:14', 1, 'teste-2', 126),
(10, 'cb5011a46c1f69569a0ec800adceeb84', 'Teste 3', 153, 'Sim', 'Não', 'Regional', '11,11', 'Não', '3', '2018-02-22 21:52:39', 1, 'teste-3', 125);

-- --------------------------------------------------------

--
-- Estrutura da tabela `cadastro_reembolsos`
--

DROP TABLE IF EXISTS `cadastro_reembolsos`;
CREATE TABLE IF NOT EXISTS `cadastro_reembolsos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(255) NOT NULL,
  `nome` varchar(255) NOT NULL,
  `tabela_reembolso` longtext NOT NULL,
  `data_cadastro` datetime NOT NULL,
  `status` int(11) NOT NULL,
  `slug` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `cadastro_reembolsos`
--

INSERT INTO `cadastro_reembolsos` (`id`, `token`, `nome`, `tabela_reembolso`, `data_cadastro`, `status`, `slug`) VALUES
(1, '77d72f5598a23df6448ce7298eb873ab', 'Teste agora', '<table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"width:100%\">\r\n	<tbody>\r\n		<tr>\r\n			<td>\r\n			<table align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"width:900px\">\r\n				<tbody>\r\n					<tr>\r\n						<td>\r\n						<table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"width:755px\">\r\n							<tbody>\r\n								<tr>\r\n									<td>\r\n									<table border=\"0\" cellpadding=\"0\" cellspacing=\"2\" style=\"width:100%\">\r\n										<tbody>\r\n											<tr>\r\n												<td>\r\n												<table border=\"1\" cellpadding=\"1\" cellspacing=\"1\" style=\"width:754px\">\r\n													<tbody>\r\n														<tr>\r\n															<td colspan=\"2\" style=\"vertical-align:top\"><strong>REEMBOLSO</strong></td>\r\n														</tr>\r\n														<tr>\r\n															<td colspan=\"2\" style=\"vertical-align:top\"><strong>Amil 400</strong></td>\r\n														</tr>\r\n														<tr>\r\n															<td style=\"vertical-align:top\">Colesterol (HDL)wwww</td>\r\n															<td style=\"vertical-align:top\">R$ 8,00</td>\r\n														</tr>\r\n														<tr>\r\n															<td style=\"vertical-align:top\">Colesterol Total</td>\r\n															<td style=\"vertical-align:top\">R$ 4,48</td>\r\n														</tr>\r\n														<tr>\r\n															<td style=\"vertical-align:top\">Consultas M&eacute;dicas</td>\r\n															<td style=\"vertical-align:top\">R$ 70,00</td>\r\n														</tr>\r\n														<tr>\r\n															<td style=\"vertical-align:top\">Eletrocardiograma</td>\r\n															<td style=\"vertical-align:top\">R$ 14,40</td>\r\n														</tr>\r\n														<tr>\r\n															<td style=\"vertical-align:top\">Endoscopia Digestiva</td>\r\n															<td style=\"vertical-align:top\">R$ 76,80</td>\r\n														</tr>\r\n														<tr>\r\n															<td style=\"vertical-align:top\">Hemograma Completo</td>\r\n															<td style=\"vertical-align:top\">R$ 9,60</td>\r\n														</tr>\r\n														<tr>\r\n															<td style=\"vertical-align:top\">Resson&acirc;ncia Magn&eacute;tica do Cr&acirc;nio</td>\r\n															<td style=\"vertical-align:top\">R$ 677,99</td>\r\n														</tr>\r\n														<tr>\r\n															<td style=\"vertical-align:top\">RX de T&oacute;rax</td>\r\n															<td style=\"vertical-align:top\">R$ 16,70</td>\r\n														</tr>\r\n														<tr>\r\n															<td style=\"vertical-align:top\">Tomografia de Cr&acirc;nio</td>\r\n															<td style=\"vertical-align:top\">R$ 261,19</td>\r\n														</tr>\r\n														<tr>\r\n															<td style=\"vertical-align:top\">Ultrassom Obst&eacute;trico</td>\r\n															<td style=\"vertical-align:top\">R$ 44,80</td>\r\n														</tr>\r\n														<tr>\r\n															<td style=\"vertical-align:top\">Ultrassom P&eacute;lvico</td>\r\n															<td style=\"vertical-align:top\">R$ 33,60</td>\r\n														</tr>\r\n													</tbody>\r\n												</table>\r\n												</td>\r\n											</tr>\r\n										</tbody>\r\n									</table>\r\n									</td>\r\n								</tr>\r\n							</tbody>\r\n						</table>\r\n						</td>\r\n					</tr>\r\n				</tbody>\r\n			</table>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"width:100%\">\r\n	<tbody>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', '2018-02-28 12:44:53', 0, 'teste'),
(2, 'e292de0b3221b91fd74196a074499bef', 'teste', '<table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"width:100%\">\r\n	<tbody>\r\n		<tr>\r\n			<td>\r\n			<table align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"width:900px\">\r\n				<tbody>\r\n					<tr>\r\n						<td>\r\n						<table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"width:755px\">\r\n							<tbody>\r\n								<tr>\r\n									<td>\r\n									<table border=\"0\" cellpadding=\"0\" cellspacing=\"2\" style=\"width:100%\">\r\n										<tbody>\r\n											<tr>\r\n												<td>\r\n												<table border=\"1\" cellpadding=\"1\" cellspacing=\"1\" style=\"width:754px\">\r\n													<tbody>\r\n														<tr>\r\n															<td colspan=\"2\" style=\"vertical-align:top\"><strong>REEMBOLSO</strong></td>\r\n														</tr>\r\n														<tr>\r\n															<td colspan=\"2\" style=\"vertical-align:top\"><strong>Amil 400</strong></td>\r\n														</tr>\r\n														<tr>\r\n															<td style=\"vertical-align:top\">Colesterol (HDL)</td>\r\n															<td style=\"vertical-align:top\">R$ 8,00</td>\r\n														</tr>\r\n														<tr>\r\n															<td style=\"vertical-align:top\">Colesterol Total</td>\r\n															<td style=\"vertical-align:top\">R$ 4,48</td>\r\n														</tr>\r\n														<tr>\r\n															<td style=\"vertical-align:top\">Consultas M&eacute;dicas</td>\r\n															<td style=\"vertical-align:top\">R$ 70,00</td>\r\n														</tr>\r\n														<tr>\r\n															<td style=\"vertical-align:top\">Eletrocardiograma</td>\r\n															<td style=\"vertical-align:top\">R$ 14,40</td>\r\n														</tr>\r\n														<tr>\r\n															<td style=\"vertical-align:top\">Endoscopia Digestiva</td>\r\n															<td style=\"vertical-align:top\">R$ 76,80</td>\r\n														</tr>\r\n														<tr>\r\n															<td style=\"vertical-align:top\">Hemograma Completo</td>\r\n															<td style=\"vertical-align:top\">R$ 9,60</td>\r\n														</tr>\r\n														<tr>\r\n															<td style=\"vertical-align:top\">Resson&acirc;ncia Magn&eacute;tica do Cr&acirc;nio</td>\r\n															<td style=\"vertical-align:top\">R$ 677,99</td>\r\n														</tr>\r\n														<tr>\r\n															<td style=\"vertical-align:top\">RX de T&oacute;rax</td>\r\n															<td style=\"vertical-align:top\">R$ 16,70</td>\r\n														</tr>\r\n														<tr>\r\n															<td style=\"vertical-align:top\">Tomografia de Cr&acirc;nio</td>\r\n															<td style=\"vertical-align:top\">R$ 261,19</td>\r\n														</tr>\r\n														<tr>\r\n															<td style=\"vertical-align:top\">Ultrassom Obst&eacute;trico</td>\r\n															<td style=\"vertical-align:top\">R$ 44,80</td>\r\n														</tr>\r\n														<tr>\r\n															<td style=\"vertical-align:top\">Ultrassom P&eacute;lvico</td>\r\n															<td style=\"vertical-align:top\">R$ 33,60</td>\r\n														</tr>\r\n													</tbody>\r\n												</table>\r\n												</td>\r\n											</tr>\r\n										</tbody>\r\n									</table>\r\n									</td>\r\n								</tr>\r\n							</tbody>\r\n						</table>\r\n						</td>\r\n					</tr>\r\n				</tbody>\r\n			</table>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"width:100%\">\r\n	<tbody>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', '2018-02-28 12:45:04', 1, 'teste-2'),
(3, '60308f7fa645835b344f6bfe0560e677', 'teste', '<table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"width:100%\">\r\n	<tbody>\r\n		<tr>\r\n			<td>\r\n			<table align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"width:900px\">\r\n				<tbody>\r\n					<tr>\r\n						<td>\r\n						<table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"width:755px\">\r\n							<tbody>\r\n								<tr>\r\n									<td>\r\n									<table border=\"0\" cellpadding=\"0\" cellspacing=\"2\" style=\"width:100%\">\r\n										<tbody>\r\n											<tr>\r\n												<td>\r\n												<table border=\"1\" cellpadding=\"1\" cellspacing=\"1\" style=\"width:754px\">\r\n													<tbody>\r\n														<tr>\r\n															<td colspan=\"2\" style=\"vertical-align:top\"><strong>REEMBOLSO</strong></td>\r\n														</tr>\r\n														<tr>\r\n															<td colspan=\"2\" style=\"vertical-align:top\"><strong>Amil 400</strong></td>\r\n														</tr>\r\n														<tr>\r\n															<td style=\"vertical-align:top\">Colesterol (HDL)</td>\r\n															<td style=\"vertical-align:top\">R$ 8,00</td>\r\n														</tr>\r\n														<tr>\r\n															<td style=\"vertical-align:top\">Colesterol Total</td>\r\n															<td style=\"vertical-align:top\">R$ 4,48</td>\r\n														</tr>\r\n														<tr>\r\n															<td style=\"vertical-align:top\">Consultas M&eacute;dicas</td>\r\n															<td style=\"vertical-align:top\">R$ 70,00</td>\r\n														</tr>\r\n														<tr>\r\n															<td style=\"vertical-align:top\">Eletrocardiograma</td>\r\n															<td style=\"vertical-align:top\">R$ 14,40</td>\r\n														</tr>\r\n														<tr>\r\n															<td style=\"vertical-align:top\">Endoscopia Digestiva</td>\r\n															<td style=\"vertical-align:top\">R$ 76,80</td>\r\n														</tr>\r\n														<tr>\r\n															<td style=\"vertical-align:top\">Hemograma Completo</td>\r\n															<td style=\"vertical-align:top\">R$ 9,60</td>\r\n														</tr>\r\n														<tr>\r\n															<td style=\"vertical-align:top\">Resson&acirc;ncia Magn&eacute;tica do Cr&acirc;nio</td>\r\n															<td style=\"vertical-align:top\">R$ 677,99</td>\r\n														</tr>\r\n														<tr>\r\n															<td style=\"vertical-align:top\">RX de T&oacute;rax</td>\r\n															<td style=\"vertical-align:top\">R$ 16,70</td>\r\n														</tr>\r\n														<tr>\r\n															<td style=\"vertical-align:top\">Tomografia de Cr&acirc;nio</td>\r\n															<td style=\"vertical-align:top\">R$ 261,19</td>\r\n														</tr>\r\n														<tr>\r\n															<td style=\"vertical-align:top\">Ultrassom Obst&eacute;trico</td>\r\n															<td style=\"vertical-align:top\">R$ 44,80</td>\r\n														</tr>\r\n														<tr>\r\n															<td style=\"vertical-align:top\">Ultrassom P&eacute;lvico</td>\r\n															<td style=\"vertical-align:top\">R$ 33,60</td>\r\n														</tr>\r\n													</tbody>\r\n												</table>\r\n												</td>\r\n											</tr>\r\n										</tbody>\r\n									</table>\r\n									</td>\r\n								</tr>\r\n							</tbody>\r\n						</table>\r\n						</td>\r\n					</tr>\r\n				</tbody>\r\n			</table>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"width:100%\">\r\n	<tbody>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', '2018-02-28 12:45:13', 1, 'teste-3');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
